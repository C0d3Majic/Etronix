<?php

namespace App\Http\Controllers;

use App\Models\TicketsModel;
use App\Models\CommentsModel;
use App\Models\UserSessionsModel;
use App\Models\BrandsModel;
use Illuminate\Http\Request;

use Hash;
use Auth;
use Illuminate\Routing\Controller as BaseController;

class BrandsController extends BaseController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function new(Request $request)
    {
        //Data Inputs
        $name           =   $request->input('name');
        $session_id     =   $request->input('session_id');
        $session_token  =   $request->input('session_token');

        if(empty($name))
            return  response(array('status' =>  'error',    'type'  =>  'Falto el Nombre'));
        
        $user_session   =   UserSessionsModel::where('id',$session_id)->where('session_token',$session_token)->first();
        if(!$user_session)
            return  response(array('status' =>  'error',    'type'  =>  'Acceso no autorizado'));
        
        $new_brand                 =   new BrandsModel();
        $new_brand->name           =   ucwords($name);   
        $new_brand->save();

        return response(array('status'  =>  'success'));
    }

    public function edit($id = null)
    { 
        $brand   =   BrandsModel::find($id);
        if(empty($brand))
            return  response(array('status' =>  'error',    'type'  =>  'Imposible Editar este Articulo'));
        $modal  =   '<h4>Editando la Marca: ' . $brand->name . '</h4>';
        $modal  .=  '<div class="row">
                        <div class="input-field col m6 push-m3 l6 push-l3 s12">
                        <i class="material-icons prefix">adb</i>
                        <input id="name" name="name" type="text" class="validate" value="'.$brand->name.'">
                        <label for="name">Nombre de la Marca</label>
                        </div>        
                    </div>';
        return response(array('status'  =>  'success',  'data'  =>  $modal));
    }

    public function save(Request $request)
    {
        //Data Inputs
        $brand_id       =   $request->input('brand_id');
        $name           =   $request->input('name');
        $session_id     =   $request->input('session_id');
        $session_token  =   $request->input('session_token');

        if(empty($name))
            return  response(array('status' =>  'error',    'type'  =>  'Falto el nombre del Articulo'));
         
        $user_session   =   UserSessionsModel::where('id',$session_id)->where('session_token',$session_token)->first();
        if(!$user_session)
            return  response(array('status' =>  'error',    'type'  =>  'Acceso no autorizado'));
        
        $brand          =   BrandsModel::find($brand_id);
        if(!$brand)
            return response(array('status'  =>  'error',    'type'  =>  'Imposible Editar esta Marca'));

        $brand->name    =   ucwords($name);
        $brand->save();

        return response(array('status'  =>  'success'));
    }

    public function delete($id = null)
    {
        $brand   =   BrandsModel::find($id);
        $modal   =   '<h5> Deshabilitando la marca: ' . $brand->name . '</h5>';
        return response(array('status'  =>  'success',  'data' =>  $modal));
    }

    public function delete_brand(Request $request)
    {
        //Data Inputs
        $brand_id           =   $request->input('brand_id');
        $session_id         =   $request->input('session_id');
        $session_token      =   $request->input('session_token');        

        $brand   =   BrandsModel::find($brand_id);
        if(empty($brand))
            return  response(array('status' =>  'error',    'type'  =>  'Imposible Deshabilitar esta Marca'));
    
        $user_session   =   UserSessionsModel::where('id',$session_id)->where('session_token',$session_token)->first();
        if(!$user_session)
            return  response(array('status' =>  'error',    'type'  =>  'Acceso no autorizado'));
        
        $brand->is_active    =   0;
        $brand->save();

        return response(array('status'  =>  'success'));
    }
}

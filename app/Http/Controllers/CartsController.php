<?php

namespace App\Http\Controllers;

use App\Models\ThreadsModel;
use App\Models\CommentsModel;
use App\Models\ItemsModel;
use App\Models\PaymentsModel;
use App\Models\CartsModel;
use App\Models\UserSessionsModel;
use App\Models\ListsModel;
use Illuminate\Http\Request;

use Illuminate\Routing\Controller as BaseController;

class CartItem { 
    public $id; 
    public $amount; 
} 
class CartsController extends BaseController
{
    public function add_to_item(Request $request)
    {
        $item_id        =   $request->input('item_id');
        $cart_items = [];
        $flag = false;
        if ($request->session()->has('products')) {  //Si ya hay items en el carrito
            $cart_items   =   $request->session()->get('products');   //Los recuperamos
            foreach($cart_items as $item)                      //Iteramos esos items para ver si el nuevo no existe
            {
                if($item->id == $item_id)  //Si lo encontramos solo incrementamos la cantidad
                {
                    $item->amount = $item->amount + 1;
                    $flag = true;
                }
            }
        }else
            return response(array('status'  =>  'error',    'type'  =>  'Carrito Vacio'));

        if(!$flag)
            return response(array('status'  =>  'error',    'type'  =>  'Producto Invalido'));
        
        session(['products' => $cart_items]);
        
        return response(array('status'  =>  'success'));
    }

    public function substract_to_item(Request $request)
    {
        $item_id        =   $request->input('item_id');
        $cart_items = [];
        $flag = false;
        if ($request->session()->has('products')) {  //Si ya hay items en el carrito
            $cart_items   =   $request->session()->get('products');   //Los recuperamos
            foreach($cart_items as $item)                      //Iteramos esos items para ver si el nuevo no existe
            {
                if($item->id == $item_id)  //Si lo encontramos solo incrementamos la cantidad
                {
                    $item->amount = $item->amount - 1;
                    $flag = true;
                }
            }
        }else
            return response(array('status'  =>  'error',    'type'  =>  'Carrito Vacio'));

        if(!$flag)
            return response(array('status'  =>  'error',    'type'  =>  'Producto Invalido'));
        
        session(['products' => $cart_items]);
        
        return response(array('status'  =>  'success'));
    }

    public function add_item(Request $request)
    {
        $item_id        =   $request->input('item_id');
        $amount         =   $request->input('amount');
        $cart_items = [];
        $flag = false;
        if ($request->session()->has('products')) {  //Si ya hay items en el carrito
            $cart_items   =   $request->session()->get('products');   //Los recuperamos
            foreach($cart_items as $item)                      //Iteramos esos items para ver si el nuevo no existe
            {
                if($item->id == $item_id)  //Si lo encontramos solo incrementamos la cantidad
                {
                    $item->amount = $item->amount + $amount;
                    $flag = true;
                }
            }
        }
        if(!$flag){
            $new_cart_item      =   new CartItem();
            $new_cart_item->id      =   $item_id;
            $new_cart_item->amount   =   $amount;
            $cart_items[] =   $new_cart_item;
        }
        session(['products' => $cart_items]);
        
        return response(array('status'  =>  'success'));
    }

    public function delete(Request $request)
    {
        $request->session()->flush();
        return response(array('status'  =>  'success'));
    }

    public function remove_item(Request $request)
    {
        $item_id        =   $request->input('item_id');
        $cart_items = [];
        $flag = false;
        

        if ($request->session()->has('products')) {  //Si ya hay items en el carrito
            $cart_items   =   $request->session()->get('products');   //Los recuperamos
            foreach($cart_items as $elementKey => $element) {
                foreach($element as $valueKey => $value) {
                    if($valueKey == 'id' && $value == $item_id){
                        //delete this particular object from the $array
                        $flag   =   true;
                        unset($cart_items[$elementKey]);
                    } 
                }
            }
        }
        else
            return response(array('status'  =>  'error',    'type'  =>  'Carrito Vacio'));

        if(!$flag)
            return response(array('status'  =>  'error',    'type'  =>  'Producto Invalido'));
        
        session(['products' => $cart_items]);
        
        return response(array('status'  =>  'success'));
    }
    
}

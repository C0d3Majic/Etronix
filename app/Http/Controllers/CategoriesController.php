<?php

namespace App\Http\Controllers;

use App\Models\TicketsModel;
use App\Models\CommentsModel;
use App\Models\UserSessionsModel;
use App\Models\BrandsModel;
use App\Models\CategoriesModel;
use Illuminate\Http\Request;

use Hash;
use Auth;
use Illuminate\Routing\Controller as BaseController;

class CategoriesController extends BaseController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function new(Request $request)
    {
        //Data Inputs
        $name           =   $request->input('name');
        $session_id     =   $request->input('session_id');
        $session_token  =   $request->input('session_token');

        if(empty($name))
            return  response(array('status' =>  'error',    'type'  =>  'Falto el Nombre'));
        
        $user_session   =   UserSessionsModel::where('id',$session_id)->where('session_token',$session_token)->first();
        if(!$user_session)
            return  response(array('status' =>  'error',    'type'  =>  'Acceso no autorizado'));
        
        $new_category            =   new CategoriesModel();
        $new_category->name      =   ucwords($name);   
        $new_category->save();

        return response(array('status'  =>  'success'));
    }

    public function edit($id = null)
    { 
        $category   =    CategoriesModel::find($id);
        if(empty($category))
            return  response(array('status' =>  'error',    'type'  =>  'Imposible Editar este Articulo'));
        $modal  =   '<h4>Editando la Marca: ' . $category->name . '</h4>';
        $modal  .=  '<div class="row">
                        <div class="input-field col m6 push-m3 l6 push-l3 s12">
                        <i class="material-icons prefix">adb</i>
                        <input id="name" name="name" type="text" class="validate" value="'.$category->name.'">
                        <label for="name">Nombre de la Categoria</label>
                        </div>        
                    </div>';
        return response(array('status'  =>  'success',  'data'  =>  $modal));
    }

    public function save(Request $request)
    {
        //Data Inputs
        $category_id    =   $request->input('category_id');
        $name           =   $request->input('name');
        $session_id     =   $request->input('session_id');
        $session_token  =   $request->input('session_token');

        if(empty($name))
            return  response(array('status' =>  'error',    'type'  =>  'Falto el nombre del Articulo'));
         
        $user_session   =   UserSessionsModel::where('id',$session_id)->where('session_token',$session_token)->first();
        if(!$user_session)
            return  response(array('status' =>  'error',    'type'  =>  'Acceso no autorizado'));
        
        $category          =   CategoriesModel::find($category_id);
        if(!$category)
            return response(array('status'  =>  'error',    'type'  =>  'Imposible Editar esta Categoria'));

        $category->name    =   ucwords($name);
        $category->save();

        return response(array('status'  =>  'success'));
    }

    public function delete($id = null)
    {
        $category   =   CategoriesModel::find($id);
        $modal      =   '<h5> Deshabilitando la Categoria: ' . $category->name . '</h5>';
        return response(array('status'  =>  'success',  'data' =>  $modal));
    }

    public function delete_category(Request $request)
    {
        //Data Inputs
        $category_id        =   $request->input('category_id');
        $session_id         =   $request->input('session_id');
        $session_token      =   $request->input('session_token');        

        $category   =   CategoriesModel::find($category_id);
        if(empty($category))
            return  response(array('status' =>  'error',    'type'  =>  'Imposible Deshabilitar esta Categoria'));
    
        $user_session   =   UserSessionsModel::where('id',$session_id)->where('session_token',$session_token)->first();
        if(!$user_session)
            return  response(array('status' =>  'error',    'type'  =>  'Acceso no autorizado'));
        
        $category->is_active    =   0;
        $category->save();

        return response(array('status'  =>  'success'));
    }
}

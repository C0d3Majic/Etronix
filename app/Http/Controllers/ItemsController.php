<?php

namespace App\Http\Controllers;

use App\Models\TicketsModel;
use App\Models\CommentsModel;
use App\Models\UserSessionsModel;
use App\Models\ProvidersModel;
use App\Models\BrandsModel;
use App\Models\CategoriesModel;
use Illuminate\Http\Request;
use App\Models\ItemsModel;
use Hash;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Routing\Controller as BaseController;

class ItemsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function new(Request $request)
    {
        //Data Inputs
        $name           =   $request->input('name');
        $part_number    =   $request->input('part_number');
        $provider       =   $request->input('provider');
        $brand          =   $request->input('brand');
        $category       =   $request->input('category');
        $price          =   $request->input('price');
        $stock          =   $request->input('stock');
        $active         =   $request->input('active');
        $description    =   $request->input('description');
        $session_id     =   $request->input('session_id');
        $session_token  =   $request->input('session_token');

        if(empty($name))
            return  response(array('status' =>  'error',    'type'  =>  'Falto el nombre del Articulo'));
        
        if(empty($part_number))
            return  response(array('status' =>  'error',    'type'  =>  'Falto el número de parte del Articulo'));
        
        if(empty($provider))
            return  response(array('status' =>  'error',    'type'  =>  'Falto el proveedor del Articulo'));
        
        if(empty($brand))
            return  response(array('status' =>  'error',    'type'  =>  'Falto la marca del Articulo'));
        
        if(empty($category))
            return  response(array('status' =>  'error',    'type'  =>  'Falto la categoria del Articulo'));
        
        if(empty($price))
            return  response(array('status' =>  'error',    'type'  =>  'Falto el precio del Articulo'));
        
        if(empty($stock))
            return  response(array('status' =>  'error',    'type'  =>  'Falto la cantidad en almacen del Articulo'));
        
        if(empty($active))
            return  response(array('status' =>  'error',    'type'  =>  'Falto determinar si esta o no disponible el Articulo'));
        
        if(empty($description))
            return  response(array('status' =>  'error',    'type'  =>  'Falto establecer la descrición del Articulo'));
        
        $user_session   =   UserSessionsModel::where('id',$session_id)->where('session_token',$session_token)->first();
        if(!$user_session)
            return  response(array('status' =>  'error',    'type'  =>  'Acceso no autorizado'));
        
        $new_item                       =   new ItemsModel();
        $new_item->name               =   $name;
        $new_item->part_number        =   $part_number;
        $new_item->provider_id        =   $provider;
        $new_item->brand_id           =   $brand;
        $new_item->price              =   $price;
        $new_item->category_id        =   $category;
        $new_item->stock              =   $stock;
        $new_item->is_active          =   $active;
        $new_item->picture            =   '/img/default.gif';
        $new_item->description        =   $description;
        $new_item->save();

        return response(array('status'  =>  'success'));
    }

    public function delete($id = null)
    {
        $item   =   ItemsModel::find($id);
        $modal  =   '<h5> Deshabilitando el item: ' . $item->name . '</h5>';
        return response(array('status'  =>  'success',  'data' =>  $modal));
    }

    public function delete_item(Request $request)
    {
        //Data Inputs
        $item_id        =   $request->input('item_id');
        $session_id     =   $request->input('session_id');
        $session_token  =   $request->input('session_token');        

        $item   =   ItemsModel::find($item_id);
        if(empty($item))
            return  response(array('status' =>  'error',    'type'  =>  'Imposible Deshabilitar este Articulo'));
    
        $user_session   =   UserSessionsModel::where('id',$session_id)->where('session_token',$session_token)->first();
        if(!$user_session)
            return  response(array('status' =>  'error',    'type'  =>  'Acceso no autorizado'));
        
        $item->is_active    =   0;
        $item->save();

        return response(array('status'  =>  'success'));
    }

    public function edit($id = null)
    { 
        $item   =   ItemsModel::find($id);
        if(empty($item))
            return  response(array('status' =>  'error',    'type'  =>  'Imposible Editar este Articulo'));
        
        $providers  =   ProvidersModel::all();
        $categories =   CategoriesModel::all();
        $brands     =   BrandsModel::all();
        $modal  =   '<h4>Editando el Articulo: ' . $item->name . '</h4>';
        $modal  .=  '<div class="row">
        <div class="input-field col m6 l6 s12">
          <i class="material-icons prefix">adb</i>
          <input id="name" name="name" type="text" class="validate" value="'.$item->name.'">
          <label for="name">Nombre del articulo</label>
        </div>
        <div class="input-field col m6 l6 s12">
          <i class="material-icons prefix">adb</i>
          <input id="number" name="number" type="text" class="validate" value="'.$item->part_number.'">
          <label for="number">Número de parte</label>
        </div>   
      </div>
      <div class="row">
          <div class="input-field col m6 l6 s12">
              <i class="material-icons prefix">people</i>
              <select name="provider" id="provider">
                  <option value="" disabled selected>Proveedor?</option>';
                    foreach($providers as $provider)
                    {
                        if($provider->id == $item->provider_id)
                            $modal.='<option value="'.$provider->id.'" selected>'.$provider->name.'</option>';
                        else
                            $modal.='<option value="'.$provider->id.'">'.$provider->name.'</option>';
                    }
                  $modal.='</select>
                  <label for="provider">Proveedor</label>
              </select>
          </div>   
          <div class="input-field col m6 l6 s12">
              <i class="material-icons prefix">branding_watermark</i>
              <select name="brand" id="brand">
                  <option value="" disabled selected>Marca?</option>';
                    foreach($brands as $brand)
                    {
                        if($brand->id = $item->brand_id)
                            $modal.='<option value="'.$brand->id.'" selected>'.$brand->name.'</option>';
                        else
                            $modal.='<option value="'.$brand->id.'">'.$brand->name.'</option>';
                    }
                  $modal.='</select>
                  <label for="brand">Marca</label>
              </select>
          </div> 
      </div>
      <div class="row">
          <div class="input-field col m6 l6 s12">
              <i class="material-icons prefix">branding_watermark</i>
              <select name="category" id="category">
              <option value="" disabled selected>Categoria?</option>';
                foreach($categories as $category)
                {
                    if($category->id = $item->category_id)
                        $modal.='<option value="'.$category->id.'" selected>'.$category->name.'</option>';
                    else
                        $modal.='<option value="'.$category->id.'">'.$category->name.'</option>';
                }
              $modal.='</select>
              <label for="category">Categoria</label>
          </div>   
          <div class="input-field col m6 l6 s12">
              <i class="material-icons prefix">attach_money</i>
              <input id="price" name="price" type="text" class="validate" value="'.$item->price.'">
              <label for="price">Precio del articulo</label>
          </div> 
      </div>  
      <div class="row">
          <div class="input-field col m6 l6 s12">
              <i class="material-icons prefix">store</i>
              <input id="stock" name="stock" type="text" class="validate" value="'.$item->stock.'">
              <label for="stock">Cantidad en almacen articulo</label>
          </div> 
          <div class="input-field col m6 l6 s12">
              <i class="material-icons prefix">check_circle</i>
              <select name="active" id="active">
                  <option value="" disabled>Esta Disponible?</option>
                  <option value="1" selected>Si</option>
                  <option value="0">No</option>
              </select>
              <label for="active">Disponible en tienda</label>
          </div>  
      </div>
      <div class="row">
          <div class="input-field col m6 push-m3 l8 push-l2 s12">
              <i class="material-icons prefix">comment</i>
              <textarea id="description" class="materialize-textarea">'.$item->description.'</textarea>
              <label for="description">Descripción</label>
          </div> 
      </div>';

        return response(array('status'  =>  'success',  'data'  =>  $modal));
    }

    public function save(Request $request)
    {
        //Data Inputs
        $item_id        =   $request->input('item_id');
        $name           =   $request->input('name');
        $part_number    =   $request->input('part_number');
        $provider       =   $request->input('provider');
        $brand          =   $request->input('brand');
        $category       =   $request->input('category');
        $price          =   $request->input('price');
        $stock          =   $request->input('stock');
        $active         =   $request->input('active');
        $description    =   $request->input('description');
        $session_id     =   $request->input('session_id');
        $session_token  =   $request->input('session_token');

        if(empty($name))
            return  response(array('status' =>  'error',    'type'  =>  'Falto el nombre del Articulo'));
        
        if(empty($part_number))
            return  response(array('status' =>  'error',    'type'  =>  'Falto el número de parte del Articulo'));
        
        if(empty($provider))
            return  response(array('status' =>  'error',    'type'  =>  'Falto el proveedor del Articulo'));
        
        if(empty($brand))
            return  response(array('status' =>  'error',    'type'  =>  'Falto la marca del Articulo'));
        
        if(empty($category))
            return  response(array('status' =>  'error',    'type'  =>  'Falto la categoria del Articulo'));
        
        if(empty($price))
            return  response(array('status' =>  'error',    'type'  =>  'Falto el precio del Articulo'));
        
        if(empty($stock))
            return  response(array('status' =>  'error',    'type'  =>  'Falto la cantidad en almacen del Articulo'));
        
        if(empty($active))
            return  response(array('status' =>  'error',    'type'  =>  'Falto determinar si esta o no disponible el Articulo'));
        
        if(empty($description))
            return  response(array('status' =>  'error',    'type'  =>  'Falto establecer la descrición del Articulo'));
        
        $user_session   =   UserSessionsModel::where('id',$session_id)->where('session_token',$session_token)->first();
        if(!$user_session)
            return  response(array('status' =>  'error',    'type'  =>  'Acceso no autorizado'));
        
        $item                         =   ItemsModel::find($item_id);
        if(!$item)
            return response(array('status'  =>  'error',    'type'  =>  'Imposible Editar este Articulo'));

        $item->name               =   $name;
        $item->part_number        =   $part_number;
        $item->provider_id        =   $provider;
        $item->brand_id           =   $brand;
        $item->price              =   $price;
        $item->category_id        =   $category;
        $item->stock              =   $stock;
        $item->is_active          =   $active;
        $item->description        =   $description;
        $item->save();

        return response(array('status'  =>  'success'));
    }

    public function edit_image($id = null)
    {
        $item   =   ItemsModel::find($id);
        if(empty($item))
            return  response(array('status' =>  'error',    'type'  =>  'Imposible Editar este Articulo'));
        
        $modal  =   '<h5>Editando la imagen del Articulo: ' . $item->name . '</h5>';
        $modal  .=  '<form id="form_upload" action="'.url('save/item/image').'" method="POST" enctype="multipart/form-data">
                        <div class="row">
                            <div class="input-field file-field col m6 l6 s12" style="margin-top:10%">
                                <div class="btn">
                                    <span>Archivo</span>
                                    <input name="picture" id="picture" accept="image/*" onchange="loadFile(event)" type="file">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text">
                                </div>
                            </div>
                            <div class="input-field col m6 l6 s12">
                                <div class="card">
                                    <div class="card-image">
                                        <img id="output" src="'.$item->picture.'">
                                    </div>
                                </div>
                            </div>   
                        </div> 
                        <input hidden id="item_id" name="item_id" type="text" value="'.$item->id.'">                                         
                    </form>';
        $footer =   '<div class="row">
        <div class="col s6">
            <a id="btnCargar" type="submit" class="modal-action modal-close waves-effect waves-light btn">Aceptar</a>     
        </div>
        <div class="col s6">
          <a id="btnCancelarCargar" style="background:red" class="modal-action modal-close waves-effect waves-light btn">Cancelar</a>
        </div>
      </div>';
        return response(array('status'  =>  'success',  'data'  =>  $modal, 'footer'    =>  $footer));
    }

    public function save_image(Request $request)
    {
        $item_id    =   $request->input('item_id');
        $item       =   ItemsModel::find($item_id);
        $this->validate($request,[
            '0' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if(!$request->hasFile('0')) 
            return response(array('status' => 'error', 'type' => 'Archivo no encontrado'));
        
        $file = $request->file('0');
        if(!$file->isValid()) {
        return response(array('status' => 'error', 'type' => 'Archivo invalido'));
        }

        $path = 'img/items/'. $item_id.'/';
        $file->move($path, 'splash.'.$file->getClientOriginalExtension());
          //return response()->json(compact('path'));
        $item->picture  =   '/img/items/'. $item_id.'/splash.'.$file->getClientOriginalExtension();
        $item->save();
        return response(array('status' => 'success'));
    }
}

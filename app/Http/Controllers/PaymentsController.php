<?php

namespace App\Http\Controllers;

use App\Models\ThreadsModel;
use App\Models\CommentsModel;
use App\Models\UserSessionsModel;
use Illuminate\Http\Request;
use App\Models\PaymentsModel;

use Illuminate\Routing\Controller as BaseController;

class PaymentsController extends BaseController
{
	public function __construct()
    {
        $this->middleware('auth');
    }

	public function search(Request $request)
	{
		//Data Inputs
		$payment_id	=	$request->input('id');
		$session_id     =   $request->input('session_id');
        $session_token  =   $request->input('session_token');

        if(empty($payment_id))
            return  response(array('status' =>  'error',    'type'  =>  'Falto id de la transacción'));
        
        $user_session   =   UserSessionsModel::where('id',$session_id)->where('session_token',$session_token)->first();
        if(!$user_session)
            return  response(array('status' =>  'error',    'type'  =>  'Acceso no autorizado'));

		$payment	=	PaymentsModel::where('user_id',$user_session->user_id)->with('id',$payment_id)->first();
		if(!$payment)
		return response(array('status' =>  'error',    'type'  =>  'No existe un pago con ese id'));

		return response(array('status'	=>	'success',	'payment'	=>	$payment));
	}
}

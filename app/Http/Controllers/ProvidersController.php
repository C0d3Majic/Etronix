<?php

namespace App\Http\Controllers;

use App\Models\TicketsModel;
use App\Models\CommentsModel;
use App\Models\UserSessionsModel;
use App\Models\BrandsModel;
use App\Models\ProvidersModel;
use Illuminate\Http\Request;

use Hash;
use Auth;
use Illuminate\Routing\Controller as BaseController;

class ProvidersController extends BaseController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function new(Request $request)
    {
        //Data Inputs
        $name           =   $request->input('name');
        $address        =   $request->input('address');
        $phone          =   $request->input('phone');
        $email          =   $request->input('email');
        $session_id     =   $request->input('session_id');
        $session_token  =   $request->input('session_token');

        if(empty($name))
            return  response(array('status' =>  'error',    'type'  =>  'Falto el Nombre'));
        if(empty($address))
            return  response(array('status' =>  'error',    'type'  =>  'Falto la dirección'));
        if(empty($phone))
            return  response(array('status' =>  'error',    'type'  =>  'Falto el Teléfono'));
        if(empty($email))
            return  response(array('status' =>  'error',    'type'  =>  'Falto el Email'));

        $user_session   =   UserSessionsModel::where('id',$session_id)->where('session_token',$session_token)->first();
        if(!$user_session)
            return  response(array('status' =>  'error',    'type'  =>  'Acceso no autorizado'));
        
        $new_provider           =   new ProvidersModel();
        $new_provider->name     =   ucwords(mb_strtolower($name));
        $new_provider->address  =   ucwords(mb_strtolower($address));  
        $new_provider->phone    =   $phone;
        $new_provider->email    =   ucwords(mb_strtolower($email));   
        $new_provider->save();

        return response(array('status'  =>  'success'));
    }

    public function edit($id = null)
    { 
        $provider   =   ProvidersModel::find($id);
        if(empty($provider))
            return  response(array('status' =>  'error',    'type'  =>  'Imposible Editar este Proveedor'));
        $modal  =   '<h4>Editando el Proveedor: ' . $provider->name . '</h4>';
        $modal  .=  '<div class="row">
                        <div class="input-field col m6 l6 s12">
                            <i class="material-icons prefix">people</i>
                            <input id="name" name="name" type="text" class="validate" value="'.$provider->name.'">
                            <label for="name">Nombre</label>
                        </div>
                        <div class="input-field col m6 l6 s12">
                            <i class="material-icons prefix">home</i>
                            <input id="address" name="address" type="text" class="validate" value="'.$provider->address.'">
                            <label for="address">Dirección</label>
                        </div>
                        </div>
                        <div class="row">
                        <div class="input-field col m6 l6 s12">
                            <i class="material-icons prefix">phone</i>
                            <input id="phone" name="phone" type="text" class="validate" value="'.$provider->phone.'">
                            <label for="phone">Teléfono</label>
                        </div>
                        <div class="input-field col m6 l6 s12">
                            <i class="material-icons prefix">mail</i>
                            <input id="email" name="email" type="text" class="validate" value="'.$provider->email.'">
                            <label for="email">Correo electrónico</label>
                        </div>
                    </div>';
        return response(array('status'  =>  'success',  'data'  =>  $modal));
    }

    public function save(Request $request)
    {
        //Data Inputs
        $name           =   $request->input('name');
        $address        =   $request->input('address');
        $phone          =   $request->input('phone');
        $email          =   $request->input('email');
        $session_id     =   $request->input('session_id');
        $session_token  =   $request->input('session_token');
        $provider_id    =   $request->input('provider_id');
        
        if(empty($name))
            return  response(array('status' =>  'error',    'type'  =>  'Falto el Nombre'));
        if(empty($address))
            return  response(array('status' =>  'error',    'type'  =>  'Falto la dirección'));
        if(empty($phone))
            return  response(array('status' =>  'error',    'type'  =>  'Falto el Teléfono'));
        if(empty($email))
            return  response(array('status' =>  'error',    'type'  =>  'Falto el Email'));

        $user_session   =   UserSessionsModel::where('id',$session_id)->where('session_token',$session_token)->first();
        if(!$user_session)
            return  response(array('status' =>  'error',    'type'  =>  'Acceso no autorizado'));

        $provider     =  ProvidersModel::find($provider_id);
        if(strcasecmp($provider->email,$email) != 0)
        {   
            $email_already_exists    =   ProvidersModel::where('email',$email)->first();
            if($email_already_exists)
                return  response(array('status' =>  'error',    'type'  =>  'Este Correo ya esta asignado a otro Proveedor'));
        }
        
        $provider->name     =   ucwords(mb_strtolower($name));
        $provider->address  =   ucwords(mb_strtolower($address));  
        $provider->phone    =   $phone;
        $provider->email    =   ucwords(mb_strtolower($email));   
        $provider->save();

        return response(array('status'  =>  'success'));
    }

    public function delete($id = null)
    {
        $provider   =   ProvidersModel::find($id);
        $modal      =   '<h5> Deshabilitando el proveedor: ' . $provider->name . '</h5>';
        return response(array('status'  =>  'success',  'data' =>  $modal));
    }

    public function delete_provider(Request $request)
    {
        //Data Inputs
        $provider_id            =   $request->input('provider_id');
        $session_id             =   $request->input('session_id');
        $session_token          =   $request->input('session_token');        

        $provider   =   ProvidersModel::find($provider_id);
        if(empty($provider))
            return  response(array('status' =>  'error',    'type'  =>  'Imposible Deshabilitar este Proveedor'));
    
        $user_session   =   UserSessionsModel::where('id',$session_id)->where('session_token',$session_token)->first();
        if(!$user_session)
            return  response(array('status' =>  'error',    'type'  =>  'Acceso no autorizado'));
        
        $provider->is_active    =   0;
        $provider->save();

        return response(array('status'  =>  'success'));
    }
}

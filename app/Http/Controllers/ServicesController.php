<?php

namespace App\Http\Controllers;

use App\Models\ThreadsModel;
use App\Models\CommentsModel;
use App\Models\ServicesModel;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Routing\Controller as BaseController;

class ServicesController extends BaseController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function send_answer (Request $request)
    {
        $email      =   $request->input('email');
        $name       =   $request->input('name');
        $content    =   $request->input('content');
        $service_id =   $request->input('service_id');

        if(empty($email))
            return  response(array('status'  =>  'error',    'type'  =>  'Falta email'));
        if(empty($name))
            return  response(array('status'  =>  'error',    'type'  =>  'Falta Nombre'));
        if(empty($content))
            return  response(array('status' =>  'error',    'type'  =>  'Falta el Contenido de la Respuesta'));

        $service    =   ServicesModel::find($service_id);
        if(!$service)
            return  response(array('status' =>  'error',    'type'  =>  'Servicio Invalido'));

        return response(array('status'  =>  'success'));
    }

    public function cancel (Request $request)
    {
        $service_id =   $request->input('service_id');
        $content    =   $request->input('content');
        
        if(empty($content))
            return  response(array('status' =>  'error',    'type'  =>  'Falta el Contenido de la Respuesta'));
            
        $service    =   ServicesModel::find($service_id);
        if(!$service)
            return  response(array('status' =>  'error',    'type'  =>  'Servicio Invalido'));
        
        return response(array('status'  =>  'success'));
    }

    public function new(Request $request)
    {
        $description    =   $request->input('description');
        $quantity       =   $request->input('quantity');
        if(empty($description))
            return response(array('status'  =>  'error',    'type'  =>  'Falta la Descripción'));
        if(empty($quantity))
            return response(array('status'  =>  'error',    'type'  =>  'Falta la Cantidad'));
        

        /*$this->validate($request,[
            '0' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);*/
        if(!$request->hasFile('0')) 
            return response(array('status' => 'error', 'type' => 'Archivo no encontrado'));
        
        $file = $request->file('0');
        if(!$file->isValid()) {
            return response(array('status' => 'error', 'type' => 'Archivo invalido'));
        }

        $path = 'files/users/'. Auth::user()->id. '/'.'service/';
        $file->move($path, $file->getClientOriginalName());
          //return response()->json(compact('path'));
        $new_service    =   new ServicesModel();
        $new_service->owner_id          =   Auth::user()->id;
        $new_service->description       =   $description;
        $new_service->quantity          =   $quantity;
        $new_service->attached_file     =   '/files/users/'. Auth::user()->id.'/service/'.$file->getClientOriginalName();
        $new_service->save();
        return response(array('status' => 'success'));
    }
}

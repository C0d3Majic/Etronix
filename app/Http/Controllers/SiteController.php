<?php

namespace App\Http\Controllers;

use App\Models\ThreadsModel;
use App\Models\CommentsModel;
use App\Models\UsersModel;
use App\Models\UserTypesModel;
use App\Models\UserSessionsModel;
use App\Models\ItemsModel;
use App\Models\BrandsModel;
use App\Models\CategoriesModel;
use App\Models\ProvidersModel;
use App\Models\ServicesModel;
use App\Services\Mailer;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Auth;

class CartItem { 
    public $id; 
    public $amount; 
} 
class SiteController extends BaseController
{
    public function show_view_login()
    {
        if(!Auth::check())
        {
            return view('login');
        }
        if(Auth::user()->user_type == 1)        
            return redirect('dashboard');
        else
            return redirect('profile');
    }

    public function show_view_privacy()
    {
        return view('privacy');
    }

    public function show_view_conditions()
    {
        return view('conditions');
    }

    public function send_welcome()
    {
        Mailer::sendWelcome('salvador_jimenez@outlook.es','asdasdq2asdasdasdasd');
    }

    public function show_view_dashboard()
    {
        if(!Auth::check())
            return redirect('darkness');

        if(Auth::user()->user_type == 1)
        {
            $user_session   =   UserSessionsModel::where('user_id',Auth::user()->id)->whereNotNull('session_token')->first();
            return view('dashboard')->with('username',Auth::user()->name)->with('active_users',1)->with('today_sales',3)->with('pending_sales',2)->with('session_id',$user_session->id)->with('session_token',$user_session->session_token);
        }
        
        return redirect('error');
    }

    public function show_view_users()
    {
        if(!Auth::check())
            return redirect('darkness');

        if(Auth::user()->user_type == 1)
        {
            $user_types     =   UserTypesModel::all();
            $users          =   UsersModel::where('is_active',1)->get();
            $user_session   =   UserSessionsModel::where('user_id',Auth::user()->id)->whereNotNull('session_token')->first();
            return view('users')->with('users',$users)->with('user_types',$user_types)->with('username',Auth::user()->name)->with('session_id',$user_session->id)->with('session_token',$user_session->session_token);
        }
        return redirect('error');
    }

    public function show_view_error()
    {
        return view('error');
    }

    public function show_view_reports()
    {
        if(!Auth::check())
            return redirect('darkness');

        if(Auth::user()->user_type == 1)
        {
            $user_types     =   UserTypesModel::all();
            $users          =   UsersModel::where('is_active',1)->get();
            $user_session   =   UserSessionsModel::where('user_id',Auth::user()->id)->whereNotNull('session_token')->first();
            return view('reports')->with('users',$users)->with('user_types',$user_types)->with('username',Auth::user()->name)->with('session_id',$user_session->id)->with('session_token',$user_session->session_token);
        }
        return redirect('error');
    }

    public function show_view_perfil()
    {
        if(!Auth::check())
            return redirect('/');

        $user_session   =   UserSessionsModel::where('user_id',Auth::user()->id)->whereNotNull('session_token')->first();
        return view('perfil')->with('username',Auth::user()->name)->with('session_id',$user_session->id)->with('session_token',$user_session->session_token);
        
        return redirect('error');
    }

    public function show_view_payments()
    {
        if(!Auth::check())
            return redirect('/');

        $user_session   =   UserSessionsModel::where('user_id',Auth::user()->id)->whereNotNull('session_token')->first();
        return view('payments')->with('username',Auth::user()->name)->with('session_id',$user_session->id)->with('session_token',$user_session->session_token);
        
        return redirect('error');
    }

    public function show_view_requests()
    {
        return view('requests');
    }

    public function show_view_landing()
    {
        $threads    =   ThreadsModel::where('is_active',1)->get();
        $categories =   CategoriesModel::all();
        $brands     =   BrandsModel::all();
        foreach($threads as $thread){
            $createDate = new \DateTime($thread->updated_at);
            $strip = $createDate->format('Y-m-d');
            $thread->date = $strip;
            $thread->comments   =   CommentsModel::where('thread_id',$thread->id)->count();
        }
        return view('landing')->with('threads',$threads)->with('brands',$brands)->with('categories',$categories);
    }

    public function show_view_items()
    {
        if(!Auth::check())
            return redirect('darkness');

        if(Auth::user()->user_type == 1)
        {
            $items          =   ItemsModel::where('is_active',1)->get();
            $brands         =   BrandsModel::all();
            $providers      =   ProvidersModel::all();
            $categories     =   CategoriesModel::all();
            $user_session   =   UserSessionsModel::where('user_id',Auth::user()->id)->whereNotNull('session_token')->first();
            return view('items')->with('providers',$providers)->with('categories',$categories)->with('items',$items)->with('brands',$brands)->with('username',Auth::user()->name)->with('session_id',$user_session->id)->with('session_token',$user_session->session_token);
        }else
            return redirect('error');
    }

    public function show_view_store()
    {
        $items = ItemsModel::where('is_active',1)->get();
        $categories =   CategoriesModel::all();
        $array_of_ids = [];
        //$user_session   =   UserSessionsModel::where('user_id',Auth::user()->id)->whereNotNull('session_token')->first();
        foreach($items as $item)
            $array_of_ids[] = $item->id;
        return view('store')->with('array_of_ids',$array_of_ids)->with('categories',$categories)->with('items',$items)->with('active',1)->with('category',1)->with('page',1);
    }

    public function show_view_providers()
    {
        if(!Auth::check())
            return redirect('darkness');

        if(Auth::user()->user_type == 1)
        {
            $providers  =   ProvidersModel::where('is_active',1)->get();
            $user_session   =   UserSessionsModel::where('user_id',Auth::user()->id)->whereNotNull('session_token')->first();
            return view('providers')->with('username',Auth::user()->name)->with('providers',$providers)->with('session_id',$user_session->id)->with('session_token',$user_session->session_token);
        }
        return redirect('error');
    }

    public function show_view_brands()
    {
        if(!Auth::check())
            return redirect('darkness');
        
        if(Auth::user()->user_type  == 1)
        {
            $brands =   BrandsModel::where('is_active',1)->get();
            $user_session   =   UserSessionsModel::where('user_id',Auth::user()->id)->whereNotNull('session_token')->first();
            return view('brands')->with('username',Auth::user()->name)->with('brands',$brands)->with('session_id',$user_session->id)->with('session_token',$user_session->session_token);
        
        }
        return redirect('error');
    }

    public function show_view_cart(Request $request)
    {
        $items        =     [];
        $x=0;
        if ($request->session()->has('products')) {
            $cart_items   =   session('products');   //Los recuperamos           
            foreach($cart_items as $item)                      //Iteramos esos items para ver si el nuevo no existe
            {
                $items[$x]              =   ItemsModel::find($item->id);            
                $items[$x]->amount      =   $item->amount;
                $x++;
            }
        }
        $subtotal  = 0;
        foreach($items as $item)
        {
            $subtotal += $item->price * $item->amount;
        }
        $iva = $subtotal * 0.16;
        $total  =   $subtotal + $iva;
        $iva = number_format($iva, 2, '.', '');
        $total = number_format($total, 2, '.', '');
        $subtotal = number_format($subtotal, 2, '.', '');
        $void = false;
        if($x == 0)
            $void = true;
        return view('cart')->with('void',$void)->with('items',$items)->with('total',$total)->with('iva',$iva)->with('subtotal',$subtotal);
    }

    public function show_view_categories()
    {
        if(!Auth::check())
            return redirect('darkness');
        
        if(Auth::user()->user_type  == 1)
        {
            $categories     =   CategoriesModel::where('is_active',1)->get();
            $user_session   =   UserSessionsModel::where('user_id',Auth::user()->id)->whereNotNull('session_token')->first();
            return view('categories')->with('username',Auth::user()->name)->with('categories',$categories)->with('session_id',$user_session->id)->with('session_token',$user_session->session_token);
        }
        return redirect('error');
    }

    public function show_view_services()
    {
        if(!Auth::check())
            return redirect('darkness');
    
        if(Auth::user()->user_type  == 1)
        {
            $services       =   ServicesModel::all();
            $user_session   =   UserSessionsModel::where('user_id',Auth::user()->id)->whereNotNull('session_token')->first();
            return view('services')->with('username',Auth::user()->name)->with('services',$services)->with('session_id',$user_session->id)->with('session_token',$user_session->session_token);
        }
        return redirect('error');
    }

    public function show_view_welcome()
    {
        return view('welcome');
    }
}
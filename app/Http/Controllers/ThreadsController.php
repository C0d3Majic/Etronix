<?php

namespace App\Http\Controllers;

use App\Models\ThreadsModel;
use App\Models\CommentsModel;

use Illuminate\Routing\Controller as BaseController;

class ThreadsController extends BaseController
{
    public function thread($id = null)
    {
    	$thread 	=	ThreadsModel::find($id);
		$createDate = new \DateTime($thread->updated_at);
		$strip = $createDate->format('Y-m-d');
		$thread->date = $strip;
		$comments_in_thread	=	CommentsModel::where('thread_id',$thread->id)->orderBy('created_at','desc')->get();
    	return view('view_thread')->with('thread',$thread)->with('comments',$comments_in_thread);
	}
	
	
}

<?php

namespace App\Http\Controllers;

use App\Models\TicketsModel;
use App\Models\CommentsModel;
use App\Models\UserSessionsModel;
use Illuminate\Http\Request;

use Hash;
use Auth;
use Illuminate\Routing\Controller as BaseController;

class TicketsController extends BaseController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function new(Request $request)
    {
        //Data Inputs
        $description    =   $request->input('description');
        $session_id     =   $request->input('session_id');
        $session_token  =   $request->input('session_token');

        if(empty($description))
            return  response(array('status' =>  'error',    'type'  =>  'Falto el contenido del Ticket'));
        
        $user_session   =   UserSessionsModel::where('id',$session_id)->where('session_token',$session_token)->first();
        if(!$user_session)
            return  response(array('status' =>  'error',    'type'  =>  'Acceso no autorizado'));
        
        $new_ticket                 =   new TicketsModel();
        $new_ticket->owner_id       =   $user_session->user_id;
        $new_ticket->description    =   $description;
        $new_ticket->save();

        return response(array('status'  =>  'success'));
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\ThreadsModel;
use App\Models\CommentsModel;
use App\Models\UserTypesModel;
use App\Models\UsersModel;
use App\Models\UserSessionsModel;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Hash;
use Auth;
class UsersController extends BaseController
{
    public function login(Request $request)
    {
        //Input Data
        $email      =   $request->input('email');
        $password   =   $request->input('password');

        $user   =   UsersModel::where('email',$email)->first();
        if(!$user)
            return response(array('status'  =>  'error',    'type'  =>  'credenciales incorrectas'));

        if(!Hash::check($password,$user->password))            
            return response(array('status'  =>  'error',    'type'  =>  'credenciales incorrectas'));
        
        $exists_old_user_session    =   UserSessionsModel::where('user_id',$user->id)->whereNotNull('session_token')->first();
        if($exists_old_user_session)
        {
            $exists_old_user_session->session_token =   null;
            $exists_old_user_session->save();
        
        }
        $new_user_session   =   new UserSessionsModel();
        $new_user_session->user_id  =   $user->id;
        $new_user_session->session_token    =   sha1(time());
        $new_user_session->save();

        Auth::login($user);
        return response(array('status'  =>  'success'));
    }

    public function logout(Request $request)
    {
        //Input Data
        $session_id     =   $request->input('session_id');
        $session_token  =   $request->input('session_token');
        $user_session   =   UserSessionsModel::find($session_id);
        $user_session->session_token    =   null;
        $user_session->save();
        Auth::logout();
        return response(array('status'  =>  'success'));
    }

    public function new(Request $request)
    {
        if(!Auth::check())
            return redirect('login');
        
        //Data Inputs
        $name           =   $request->input('name');
        $email          =   $request->input('email');
        $password       =   $request->input('password');
        $type           =   $request->input('type');
        $session_id     =   $request->input('session_id');
        $session_token  =   $request->input('session_token');

        if(empty($name))
            return  response(array('status' =>  'error',    'type'  =>  'Falto el Nombre del Usuario'));
        if(empty($email))
            return  response(array('status' =>  'error',    'type'  =>  'Falto el Correo del Usuario'));
        if(strlen($password)<6)
            return  response(array('status' =>  'error',    'type'  =>  'El password debe tener longitud mayor o igual a 6'));
        if(empty($type))
            return  response(array('status' =>  'error',    'type'  =>  'Falto el tipo de usuario'));
      
        $user_session   =   UserSessionsModel::where('id',$session_id)->where('session_token',$session_token)->first();
        if(!$user_session)
            return  response(array('status' =>  'error',    'type'  =>  'Acceso no autorizado'));
        
        $email_already_exists    =   UsersModel::where('email',$email)->first();
        if($email_already_exists)
            return  response(array('status' =>  'error',    'type'  =>  'Este Correo ya esta asignado a otro Usuario'));
        
        $new_user               =   new UsersModel();
        $new_user->name         =   ucwords(mb_strtolower ($name));
        $new_user->email        =   mb_strtolower($email);
        $new_user->password     =   Hash::make($password);
        $new_user->user_type    =   $type;
        $new_user->avatar       =   '/img/alien.png'; //Default avatar

        $new_user->save();

        return response(array('status'  =>  'success'));
    }

    public function delete($id = null)
    {
        $user               =   UsersModel::find($id);
        $modal      =   '<h5>Eliminando al Usuario : <b>' . $user->name .  '</b></h5>';
        return response(array('status'  =>  'success',  'data'  =>  $modal));
    }

    public function delete_user(Request $request)
    {
        //Data Inputs
        $user_id        =   $request->input('user_id');
        $session_id     =   $request->input('session_id');
        $session_token  =   $request->input('session_token');

        $user_session   =   UserSessionsModel::where('id',$session_id)->where('session_token',$session_token)->first();
        if(!$user_session)
            return  response(array('status' =>  'error',    'type'  =>  'Acceso no autorizado'));

        if(empty($user_id))
            return response(array('status'  =>  'error',    'type'  =>  'Imposible eliminar este usuario'));
        
        $user_to_be_deleted   =   UsersModel::find($user_id);
        //$are_there_prizes_linked_to_this_user =   PrizesModel::where('mistery',$user_to_be_deleted->id)->first();
        //if($are_there_prizes_linked_to_this_user)
        //    return response(array('status'  =>  'error',    'type'  =>  'Imposible eliminar este Usuario, existen Premios vinculados a el'));
        
        $user_to_be_deleted->is_active  =   0;
        $user_to_be_deleted->save();
        
        return response(array('status'  =>  'success'));
    }

    public function edit($id = null)
    {
        $user               =   UsersModel::find($id);
        $user_types         =   UserTypesModel::all();
        $modal              =   '<h5>Editando al Usuario: <b>' . $user->name . '</b></h5>';
        $modal              .=  '<div class="row">
                                    <div class="input-field col m6 l6 s12">
                                        <i class="material-icons prefix">people</i>
                                        <input id="name" name="name" type="text" class="validate" value="'.$user->name.'">
                                        <label for="name">Nombre del usuario</label>
                                    </div>  
                                    <div class="input-field col m6 l6 s12">
                                        <i class="material-icons prefix">mail</i>
                                        <input id="email" name="email" type="email" class="validate" value="'.$user->email.'">
                                        <label for="email">Correo electrónico</label>
                                    </div>   
                                </div>  
                                <div class="row">
                                    <div class="input-field col m6 l6 s12">
                                        <i class="material-icons prefix">people</i>
                                        <input id="password" name="password" type="password" class="validate" value="">
                                        <label for="password">Contraseña</label>
                                    </div>  
                                    <div class="input-field col m6 l6 s12">
                                        <i class="material-icons prefix">accessibility</i>
                                        <select name="type" id="type">
                                        <option value="">Tipo de usuario?</option>';
                                        foreach($user_types as $user_type)
                                        {
                                            if($user->user_type == $user_type->id)
                                                $modal.='<option value="'.$user_type->id.'" selected>'.$user_type->name.'</option>';
                                            else
                                                $modal.='<option value="'.$user_type->id.'">'.$user_type->name.'</option>';
                                        }
                                        $modal.='</select>
                                        <label for="type">Ubicación</label>
                                    </div>   
                                </div>';
        return response(array('status'  =>  'success',  'data'  =>  $modal));
    }

    public function save(Request $request)
    {
        if(!Auth::check())
            return redirect('login');
        
        //Data Inputs
        $name           =   $request->input('name');
        $email          =   $request->input('email');
        $password       =   $request->input('password');
        $type           =   $request->input('type');
        $user_id        =   $request->input('user_id');
        $session_id     =   $request->input('session_id');
        $session_token  =   $request->input('session_token');

        if(empty($name))
            return  response(array('status' =>  'error',    'type'  =>  'Falto el Nombre del Usuario'));
        if(empty($email))
            return  response(array('status' =>  'error',    'type'  =>  'Falto el Correo del Usuario'));
        if(strlen($password)<6 && !empty($password))
            return  response(array('status' =>  'error',    'type'  =>  'El password debe tener longitud mayor o igual a 6'));
        if(empty($type))
            return  response(array('status' =>  'error',    'type'  =>  'Falto el tipo de usuario'));
      
        $user_session   =   UserSessionsModel::where('id',$session_id)->where('session_token',$session_token)->first();
        if(!$user_session)
            return  response(array('status' =>  'error',    'type'  =>  'Acceso no autorizado'));
        
        $user   =   UsersModel::find($user_id);
        if(strcasecmp($user->email,$email) != 0)
        {   
            $email_already_exists    =   UsersModel::where('email',$email)->first();
            if($email_already_exists)
                return  response(array('status' =>  'error',    'type'  =>  'Este Correo ya esta asignado a otro Usuario'));
        }

        $user->name         =   ucwords(mb_strtolower ($name));
        $user->email        =   mb_strtolower($email);
        if(!empty($password))
            $user->password     =   Hash::make($password);
        $user->user_type    =   $type;

        $user->save();

        return response(array('status'  =>  'success'));
    }

    public function save_profile(Request $request)
    {
        if(!Auth::check())
            return redirect('login');
        
        //Data Inputs
        $name           =   $request->input('name');
        $email          =   $request->input('email');
        $password       =   $request->input('password');
        $session_id     =   $request->input('session_id');
        $session_token  =   $request->input('session_token');

        if(empty($name))
            return  response(array('status' =>  'error',    'type'  =>  'Falto el Nombre del Usuario'));
        if(empty($email))
            return  response(array('status' =>  'error',    'type'  =>  'Falto el Correo del Usuario'));
        if(strlen($password)<6 && !empty($password))
            return  response(array('status' =>  'error',    'type'  =>  'El password debe tener longitud mayor o igual a 6'));
    
        $user_session   =   UserSessionsModel::where('id',$session_id)->where('session_token',$session_token)->first();
        if(!$user_session)
            return  response(array('status' =>  'error',    'type'  =>  'Acceso no autorizado'));
        
        $user   =   UsersModel::find($user_session->user_id);
        if(strcasecmp($user->email,$email) != 0)
        {   
            $email_already_exists    =   UsersModel::where('email',$email)->first();
            if($email_already_exists)
                return  response(array('status' =>  'error',    'type'  =>  'Este Correo ya esta asignado a otro Usuario'));
        }

        $user->name         =   ucwords(mb_strtolower ($name));
        $user->email        =   mb_strtolower($email);
        if(!empty($password))
            $user->password     =   Hash::make($password);

        $user->save();
        return response(array('status'  =>  'success'));
    }
}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BrandsModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'brands';

    /*public function owner()
    {
        return $this->belongsTo('App\Models\UsersModel');
    }*/
}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemsModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'items';

    public function provider()
    {
        return $this->belongsTo('App\Models\ProvidersModel');
    }

    public function brand()
    {
        return $this->belongsTo('App\Models\BrandsModel');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\CategoriesModel');
    }
}
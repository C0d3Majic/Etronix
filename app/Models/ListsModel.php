<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ListsModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'lists';

    /*public function owner()
    {
        return $this->belongsTo('App\Models\UsersModel');
    }*/
}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentsModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'payments';

    /*public function owner()
    {
        return $this->belongsTo('App\Models\UsersModel');
    }*/
}
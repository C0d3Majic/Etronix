<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ThreadsModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'threads';

    public function owner()
    {
        return $this->belongsTo('App\Models\UsersModel');
    }
}
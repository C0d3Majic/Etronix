<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketsModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tickets';

    public function owner()
    {
        return $this->belongsTo('App\Models\UsersModel');
    }
}
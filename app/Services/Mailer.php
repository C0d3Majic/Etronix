<?php namespace App\Services;

class Mailer
{
  public static function sendRequestDetails($overtime_request, $user, $coordinador, $gerente)
  {
    $exploded_passengers = explode(',',$overtime_request->passengers);
    $exploded_routes = explode(',',$overtime_request->routes);
    $for       =  $user.','.$coordinador.','.$gerente.',tania_4312@outlook.com';
    $subject   =  'Solicitud de Tiempo Extra';
    $message   =  '<html><body>';
    $message  .= '<table style="background-color:#FFF; border:4px solid black; color:black"><tr><td align="center"><img src="http://checkin.barontechlabs.com/img/logo.png" ></td></tr>';
    $message  .=  '<tr><td> La presente solicitud es para la fecha <b>'.$overtime_request->date_of_service . '</b></td></tr>';
    $message  .=  '<tr><td>La planta para la cual es el servicio es <b>' . $overtime_request->plant_name . '</b></td></tr>';
    $message  .=  '<tr><td>Para el turno <b>'. $overtime_request->shift. '</b></td></tr>';
    $message  .=  '<tr><td>En la linea <b>' . $overtime_request->line . '</b></td></tr>';
    $message  .=  '<tr><td>Coordinador responsable <b>' . $coordinador . '</b></td></tr>';
    $message  .=  '<tr><td>Gerente responsable <b>' . $gerente . '</b></td></tr>';
    $message  .=  '<tr><td></td></td>';
    $cont = 0;
    foreach($exploded_passengers as $passengers)
    {
      if($passengers)
      {
        if($exploded_routes[$cont] != 'r17')
        {
            $costo_viaje = 340.00;
            $costo_ideal = $costo_viaje / 40;
        }
        else
        {
          if($overtime_request->va_al_valle)
          {
            $costo_viaje = 660.00;
            $costo_ideal = $costo_viaje / 40;
          }
          else
          {
              $costo_viaje = 420.00;
              $costo_ideal = $costo_viaje / 40;
          }
        }
        $costo_generado = ($costo_viaje/$passengers)-$costo_ideal;
        $message  .=  '<tr><td>-----------------------Informacion de la ruta <b>' . ucfirst($exploded_routes[$cont]) .'</b>-----------------</td></tr>';
        $message  .=  '<tr><td>El costo ideal por persona de la ruta <b>' . ucfirst($exploded_routes[$cont]) . '</b> es <b>' . $costo_ideal . '</b></td></tr>';
        $message  .=  '<tr><td>La cantidad de pasajeros en esta ruta fue <b> '. $passengers . '</b></td></tr>';
        $message  .=  '<tr><td>Costo por persona de esta ruta fue <b>' .($costo_viaje/$passengers) .'</b></td></tr>';
        $message  .=  '<tr><td>Gasto extra por persona de <b>' . $costo_generado . '</b></td></tr>';
        $message  .=  '<tr><td></td></tr>';
        $cont = $cont + 1;
      }
    }
    $message  .= '</table></body></html>';
    $headers   =  'From: contacto@barontechlabs.com' . "\r\n" .
                  'Reply-To: contacto@barontechlabs.com' . "\r\n" .
                  'MIME-Version: 1.0' . "\r\n" .
                  'Content-Type: text/html; charset=ISO-8859-1' . "\r\n";

    mail($for, $subject, $message, $headers);
    return 'success';
  }

  public static function sendForgot($user = null, $remember_token = null)
 {
   $to        = $user->email;
   $title    = 'Recuperar password';
   $message = '
   <html>
     <head>
       <title>Mr.Pago - Template</title>
       <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
       <meta name="viewport" content="width=device-width, initial-scale=1.0" />
       <style type="text/css">
         * {
           -ms-text-size-adjust:100%;
           -webkit-text-size-adjust:none;
           -webkit-text-resize:100%;
         }
         a{
           outline:none;
           color:#40aceb;
           text-decoration:underline;
         }
         a:hover{text-decoration:none !important;}
         .nav a:hover{text-decoration:underline !important;}
         .title a:hover{text-decoration:underline !important;}
         .title-2 a:hover{text-decoration:underline !important;}
         .btn:hover{opacity:0.8;}
         .btn a:hover{text-decoration:none !important;}
         .btn{
           -webkit-transition:all 0.3s ease;
           -moz-transition:all 0.3s ease;
           -ms-transition:all 0.3s ease;
           transition:all 0.3s ease;
         }
         table td {border-collapse: collapse !important;}
         .ExternalClass, .ExternalClass a, .ExternalClass span, .ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div{line-height:inherit;}
         @media only screen and (max-width:500px) {
           table[class="flexible"]{width:100% !important;}
           table[class="center"]{
             float:none !important;
             margin:0 auto !important;
           }
           *[class="hide"]{
             display:none !important;
             width:0 !important;
             height:0 !important;
             padding:0 !important;
             font-size:0 !important;
             line-height:0 !important;
           }
           td[class="img-flex"] img{
             width:100% !important;
             height:auto !important;
           }
           td[class="aligncenter"]{text-align:center !important;}
           th[class="flex"]{
             display:block !important;
             width:100% !important;
           }
           td[class="wrapper"]{padding:0 !important;}
           td[class="holder"]{padding:30px 15px 20px !important;}
           td[class="nav"]{
             padding:20px 0 0 !important;
             text-align:center !important;
           }
           td[class="h-auto"]{height:auto !important;}
           td[class="description"]{padding:30px 20px !important;}
           td[class="i-120"] img{
             width:120px !important;
             height:auto !important;
           }
           td[class="footer"]{padding:5px 20px 20px !important;}
           td[class="footer"] td[class="aligncenter"]{
             line-height:25px !important;
             padding:20px 0 0 !important;
           }
           tr[class="table-holder"]{
             display:table !important;
             width:100% !important;
           }
           th[class="thead"]{display:table-header-group !important; width:100% !important;}
           th[class="tfoot"]{display:table-footer-group !important; width:100% !important;}
         }
       </style>
     </head>
     <body style="margin:0; padding:0;" bgcolor="#eaeced">
       <table style="min-width:320px;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#eaeced">
         <!-- fix for gmail -->
         <tr>
           <td class="hide">
             <table width="600" cellpadding="0" cellspacing="0" style="width:600px !important;">
               <tr>
                 <td style="min-width:600px; font-size:0; line-height:0;">&nbsp;</td>
               </tr>
             </table>
           </td>
         </tr>
         <tr>
           <td class="wrapper" style="padding:0 10px;">
             <!-- module 1 -->
             <table data-module="module-1" data-thumb="http://mrpago.mx/thumbnails/01.png" width="100%" cellpadding="0" cellspacing="0">
               <tr>
                 <td data-bgcolor="bg-module" bgcolor="#eaeced">
                   <table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
                     <tr>
                       <td style="padding:29px 0 30px;">
                         <table width="100%" cellpadding="0" cellspacing="0">
                           <tr>
                             <th class="flex" width="113" align="left" style="padding:0;">
                               <table class="center" cellpadding="0" cellspacing="0">
                                 <tr>
                                   <td style="line-height:0;">
                                     
                                   </td>
                                 </tr>
                               </table>
                             </th>
                             <th class="flex" align="left" style="padding:0;">
                               <table width="100%" cellpadding="0" cellspacing="0">
                                 <tr>
                                   <td data-color="text" data-size="size navigation" data-min="10" data-max="22" data-link-style="text-decoration:none; color:#888;" class="nav" align="right" style="font:bold 13px/15px Arial, Helvetica, sans-serif; color:#888;">
                                     
                                   </td>
                                 </tr>
                               </table>
                             </th>
                           </tr>
                         </table>
                       </td>
                     </tr>
                   </table>
                 </td>
               </tr>
             </table>
             <!-- module 2 -->
             <table data-module="module-2" data-thumb="http://mrpago.mx/thumbnails/02.png" width="100%" cellpadding="0" cellspacing="0">
               <tr>
                 <td data-bgcolor="bg-module" bgcolor="#eaeced">
                   <table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
                     <tr>
                       <td class="img-flex"><img src="http://mrpago.mx/images/img-01.png" style="vertical-align:top;" width="600" height="250" alt="" /></td>
                     </tr>
                     <tr>
                       <td data-bgcolor="bg-block" class="holder" style="padding:58px 60px 52px;" bgcolor="#f9f9f9">
                         <table width="100%" cellpadding="0" cellspacing="0">
                           <tr>
                             <td data-color="title" data-size="size title" data-min="25" data-max="45" data-link-color="link title color" data-link-style="text-decoration:none; color:#292c34;" class="title" align="center" style="font:35px/38px Arial, Helvetica, sans-serif; color:#292c34; padding:0 0 24px;">
                               Hola!, observamos que estas teniendo problemas con tu cuenta
                             </td>
                           </tr>
                           <tr>
                             <td data-color="text" data-size="size text" data-min="10" data-max="26" data-link-color="link text color" data-link-style="font-weight:bold; text-decoration:underline; color:#40aceb;" align="center" style="font:bold 16px/25px Arial, Helvetica, sans-serif; color:#888; padding:0 0 23px;">
                             Ingresa a la siguiente liga para recuperar tu password : <a href="http://mrpago.mx/recover/'.$remember_token.'">Recuperar</a>
                             </td>
                           </tr>
                         </table>
                       </td>
                     </tr>
                     <tr><td height="28"></td></tr>
                   </table>
                 </td>
               </tr>
             </table>
             
             <!-- module 7 -->
             <table data-module="module-7" data-thumb="http://mrpago.mx/thumbnails/07.png" width="100%" cellpadding="0" cellspacing="0">
               <tr> 
                 <td data-bgcolor="bg-module" bgcolor="#eaeced">
                   <table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
                     <tr>
                       <td class="footer" style="padding:0 0 10px;">
                         <table width="100%" cellpadding="0" cellspacing="0">
                           <tr class="table-holder">
                             <th class="tfoot" width="400" align="left" style="vertical-align:top; padding:0;">
                               <table width="100%" cellpadding="0" cellspacing="0">
                                 <tr>
                                   <td data-color="text" data-link-color="link text color" data-link-style="text-decoration:underline; color:#797c82;" class="aligncenter" style="font:12px/16px Arial, Helvetica, sans-serif; color:#797c82; padding:0 0 10px;">
                                     Etronixs.com.mx, 2018. &nbsp; Derechos Reservados. 
                                   </td>
                                 </tr>
                               </table>
                             </th>
                             <th class="thead" width="200" align="left" style="vertical-align:top; padding:0;">
                               <table class="center" align="right" cellpadding="0" cellspacing="0">
                                 <tr>
                                   <td class="btn" valign="top" style="line-height:0; padding:3px 0 0;">
                                     <a target="_blank" style="text-decoration:none;" href="#"><img src="http://mrpago.mx/img/ico-facebook.png" border="0" style="font:12px/15px Arial, Helvetica, sans-serif; color:#797c82;" align="left" vspace="0" hspace="0" width="6" height="13" alt="fb" /></a>
                                   </td>
                                   <td width="20"></td>
                                   <td class="btn" valign="top" style="line-height:0; padding:3px 0 0;">
                                     <a target="_blank" style="text-decoration:none;" href="#"><img src="http://mrpago.mx/img/ico-twitter.png" border="0" style="font:12px/15px Arial, Helvetica, sans-serif; color:#797c82;" align="left" vspace="0" hspace="0" width="13" height="11" alt="tw" /></a>
                                   </td>
                                   <td width="19"></td>
                                   <td class="btn" valign="top" style="line-height:0; padding:3px 0 0;">
                                     <a target="_blank" style="text-decoration:none;" href="#"><img src="http://mrpago.mx/img/ico-google-plus.png" border="0" style="font:12px/15px Arial, Helvetica, sans-serif; color:#797c82;" align="left" vspace="0" hspace="0" width="19" height="15" alt="g+" /></a>
                                   </td>
                                   <td width="20"></td>
                                   <td class="btn" valign="top" style="line-height:0; padding:3px 0 0;">
                                     <a target="_blank" style="text-decoration:none;" href="#"><img src="http://mrpago.mx/img/ico-linkedin.png" border="0" style="font:12px/15px Arial, Helvetica, sans-serif; color:#797c82;" align="left" vspace="0" hspace="0" width="13" height="11" alt="in" /></a>
                                   </td>
                                 </tr>
                               </table>
                             </th>
                           </tr>
                         </table>
                       </td>
                     </tr>
                   </table>
                 </td>
               </tr>
             </table>
           </td>
         </tr>
         <!-- fix for gmail -->
         <tr>
           <td style="line-height:0;"><div style="display:none; white-space:nowrap; font:15px/1px courier;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div></td>
         </tr>
       </table>
     </body>
   </html>';
   $headers  = 'From: contacto@mrpago.mx' . "\r\n";
   $headers .= 'Reply-To: contacto@mrpago.mx' . "\r\n";
   $headers .= "MIME-Version: 1.0\r\n";
   $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
   mail($to, $title, $message, $headers, '-fcontacto@mrpago.mx');
   return 'success';
 }


    public static function sendWelcome($user = null, $remember_token = null)
    {
        $to        = $user;
        $title    = 'Bienvenido a Etronix';
        $message = '
        <html>
            <head>
            <title>Etronix</title>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0" />
            <style type="text/css">
                * {
                -ms-text-size-adjust:100%;
                -webkit-text-size-adjust:none;
                -webkit-text-resize:100%;
                }
                a{
                outline:none;
                color:#40aceb;
                text-decoration:underline;
                }
                a:hover{text-decoration:none !important;}
                .nav a:hover{text-decoration:underline !important;}
                .title a:hover{text-decoration:underline !important;}
                .title-2 a:hover{text-decoration:underline !important;}
                .btn:hover{opacity:0.8;}
                .btn a:hover{text-decoration:none !important;}
                .btn{
                -webkit-transition:all 0.3s ease;
                -moz-transition:all 0.3s ease;
                -ms-transition:all 0.3s ease;
                transition:all 0.3s ease;
                }
                table td {border-collapse: collapse !important;}
                .ExternalClass, .ExternalClass a, .ExternalClass span, .ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div{line-height:inherit;}
                @media only screen and (max-width:500px) {
                table[class="flexible"]{width:100% !important;}
                table[class="center"]{
                    float:none !important;
                    margin:0 auto !important;
                }
                *[class="hide"]{
                    display:none !important;
                    width:0 !important;
                    height:0 !important;
                    padding:0 !important;
                    font-size:0 !important;
                    line-height:0 !important;
                }
                td[class="img-flex"] img{
                    width:100% !important;
                    height:auto !important;
                }
                td[class="aligncenter"]{text-align:center !important;}
                th[class="flex"]{
                    display:block !important;
                    width:100% !important;
                }
                td[class="wrapper"]{padding:0 !important;}
                td[class="holder"]{padding:30px 15px 20px !important;}
                td[class="nav"]{
                    padding:20px 0 0 !important;
                    text-align:center !important;
                }
                td[class="h-auto"]{height:auto !important;}
                td[class="description"]{padding:30px 20px !important;}
                td[class="i-120"] img{
                    width:120px !important;
                    height:auto !important;
                }
                td[class="footer"]{padding:5px 20px 20px !important;}
                td[class="footer"] td[class="aligncenter"]{
                    line-height:25px !important;
                    padding:20px 0 0 !important;
                }
                tr[class="table-holder"]{
                    display:table !important;
                    width:100% !important;
                }
                th[class="thead"]{display:table-header-group !important; width:100% !important;}
                th[class="tfoot"]{display:table-footer-group !important; width:100% !important;}
                }
            </style>
            </head>
            <body style="margin:0; padding:0;" bgcolor="#eaeced">
            <table style="min-width:320px;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#eaeced">
                <!-- fix for gmail -->
                <tr>
                <td class="hide">
                    <table width="600" cellpadding="0" cellspacing="0" style="width:600px !important;">
                    <tr>
                        <td style="min-width:600px; font-size:0; line-height:0;">&nbsp;</td>
                    </tr>
                    </table>
                </td>
                </tr>
                <tr>
                <td class="wrapper" style="padding:0 10px;">
                    <!-- module 1 -->
                    <table data-module="module-1" data-thumb="http://www.etronixs.com.mx/img/logo.jpg" width="30%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td data-bgcolor="bg-module" bgcolor="#eaeced">
                        <table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
                            <tr>
                            <td style="padding:29px 0 30px;">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th class="flex" width="113" align="left" style="padding:0;">
                                    <table class="center" cellpadding="0" cellspacing="0">
                                        <tr>
                                        <td style="line-height:0;">
                                            
                                        </td>
                                        </tr>
                                    </table>
                                    </th>
                                    <th class="flex" align="left" style="padding:0;">
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                        <td data-color="text" data-size="size navigation" data-min="10" data-max="22" data-link-style="text-decoration:none; color:#888;" class="nav" align="right" style="font:bold 13px/15px Arial, Helvetica, sans-serif; color:#888;">
                                            
                                        </td>
                                        </tr>
                                    </table>
                                    </th>
                                </tr>
                                </table>
                            </td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    </table>
                    <!-- module 2 -->
                    <table data-module="module-2" data-thumb="http://mrpago.mx/thumbnails/02.png" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td data-bgcolor="bg-module" bgcolor="#eaeced">
                        <table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
                            <tr>
                            <td class="img-flex"><img src="http://www.etronixs.com.mx/img/logo.jpg" style="vertical-align:top;" width="600" height="auto" alt="" /></td>
                            </tr>
                            <tr>
                            <td data-bgcolor="bg-block" class="holder" style="padding:58px 60px 52px;" bgcolor="#f9f9f9">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td data-color="title" data-size="size title" data-min="25" data-max="45" data-link-color="link title color" data-link-style="text-decoration:none; color:#292c34;" class="title" align="center" style="font:35px/38px Arial, Helvetica, sans-serif; color:#292c34; padding:0 0 24px;">
                                        Hola Bienvenido a Etronixs, tu cuenta fue creada satisfactoriamente.
                                    </td>
                                </tr>                                
                                </table>
                            </td>
                            </tr>
                            <tr><td height="28"></td></tr>
                        </table>
                        </td>
                    </tr>
                    </table>
                    
                    <!-- module 7 -->
                    <table data-module="module-7" data-thumb="http://mrpago.mx/thumbnails/07.png" width="100%" cellpadding="0" cellspacing="0">
                    <tr> 
                        <td data-bgcolor="bg-module" bgcolor="#eaeced">
                        <table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
                            <tr>
                            <td class="footer" style="padding:0 0 10px;">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                <tr class="table-holder">
                                    <th class="tfoot" width="400" align="left" style="vertical-align:top; padding:0;">
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                        <td data-color="text" data-link-color="link text color" data-link-style="text-decoration:underline; color:#797c82;" class="aligncenter" style="font:12px/16px Arial, Helvetica, sans-serif; color:#797c82; padding:0 0 10px;">
                                            Etronixs.com.mx, 2018. &nbsp; Derechos Reservados. 
                                        </td>
                                        </tr>
                                    </table>
                                    </th>
                                    <th class="thead" width="200" align="left" style="vertical-align:top; padding:0;">
                                    <table class="center" align="right" cellpadding="0" cellspacing="0">
                                        <tr>
                                        <td class="btn" valign="top" style="line-height:0; padding:3px 0 0;">
                                            <a target="_blank" style="text-decoration:none;" href="#"><img src="http://www.etronixs.com.mx/img/ico-facebook.png" border="0" style="font:12px/15px Arial, Helvetica, sans-serif; color:#797c82;" align="left" vspace="0" hspace="0" width="6" height="13" alt="fb" /></a>
                                        </td>
                                        <td width="20"></td>
                                        <td class="btn" valign="top" style="line-height:0; padding:3px 0 0;">
                                            <a target="_blank" style="text-decoration:none;" href="#"><img src="http://www.etronixs.com.mx/img/ico-twitter.png" border="0" style="font:12px/15px Arial, Helvetica, sans-serif; color:#797c82;" align="left" vspace="0" hspace="0" width="13" height="11" alt="tw" /></a>
                                        </td>
                                        <td width="19"></td>
                                        <td class="btn" valign="top" style="line-height:0; padding:3px 0 0;">
                                            <a target="_blank" style="text-decoration:none;" href="#"><img src="http://www.etronixs.com.mx/img/ico-google-plus.png" border="0" style="font:12px/15px Arial, Helvetica, sans-serif; color:#797c82;" align="left" vspace="0" hspace="0" width="19" height="15" alt="g+" /></a>
                                        </td>
                                        <td width="20"></td>
                                        <td class="btn" valign="top" style="line-height:0; padding:3px 0 0;">
                                            <a target="_blank" style="text-decoration:none;" href="#"><img src="http://www.etronixs.com.mx/img/ico-linkedin.png" border="0" style="font:12px/15px Arial, Helvetica, sans-serif; color:#797c82;" align="left" vspace="0" hspace="0" width="13" height="11" alt="in" /></a>
                                        </td>
                                        </tr>
                                    </table>
                                    </th>
                                </tr>
                                </table>
                            </td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    </table>
                </td>
                </tr>
                <!-- fix for gmail -->
                <tr>
                <td style="line-height:0;"><div style="display:none; white-space:nowrap; font:15px/1px courier;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div></td>
                </tr>
            </table>
            </body>
        </html>';
        $headers  = 'From: codemajic@etronixs.com.mx' . "\r\n";
        $headers .= 'Reply-To: codemajic@etronixs.com.mx' . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        mail($to, $title, $message, $headers, '-fcodemajic@etronixs.com.mx');
        return 'success';
    }
}
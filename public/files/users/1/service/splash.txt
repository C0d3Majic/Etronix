UPDATE wp_postmeta AS s
LEFT JOIN wp_postmeta AS r ON s.post_id = r.post_id
JOIN `wp_term_relationships` ON s.post_id = wp_term_relationships.object_id 
SET s.meta_value = ROUND( r.meta_value * .40, 2) 
WHERE s.meta_key = '_sale_price'
AND r.meta_key = '_regular_price'
AND wp_term_relationships.term_taxonomy_id = 20 

SELECT s.post_id, s.meta_value 
FROM wp_postmeta AS s 
LEFT JOIN wp_postmeta AS r ON s.post_id = r.post_id 
JOIN `wp_term_relationships` ON s.post_id = wp_term_relationships.object_id 
JOIN `wp_posts` ON s.post_id = wp_posts.post_parent
WHERE s.meta_key = '_sale_price' 
AND r.meta_key = '_regular_price' 
AND wp_term_relationships.term_taxonomy_id = 20 


UPDATE wp_postmeta AS s 
LEFT JOIN wp_postmeta AS r ON s.post_id = r.post_id 
JOIN `wp_term_relationships` ON s.post_id = wp_term_relationships.object_id 
JOIN `wp_posts` ON s.post_id = wp_posts.post_parent 
SET s.meta_value = ROUND( r.meta_value * .40, 2) 
WHERE s.meta_key = '_sale_price' 
AND r.meta_key = '_regular_price' 
AND wp_term_relationships.term_taxonomy_id = 20


SELECT s.post_id, s.meta_value, s.meta_key, r.meta_value, r.meta_key 
FROM wp_postmeta AS s 
LEFT JOIN wp_postmeta AS r ON s.post_id = r.post_id 
JOIN `wp_term_relationships` ON s.post_id = wp_term_relationships.object_id
JOIN `wp_posts` ON s.post_id = wp_posts.post_parent 
WHERE s.meta_key = '_sale_price' 
AND r.meta_key = '_regular_price' 
AND wp_term_relationships.term_taxonomy_id = 158 
GROUP BY s.post_id

SELECT s.post_id, s.meta_key, s.meta_value, r.meta_key, r.meta_value
FROM wp_postmeta AS s 
LEFT JOIN wp_postmeta AS r ON s.post_id = r.post_id 
JOIN `wp_term_relationships` ON s.post_id = wp_term_relationships.object_id 
JOIN `wp_posts` ON s.post_id = wp_posts.post_parent 
WHERE s.meta_key = '_sale_price' 
AND r.meta_key = '_regular_price' 
AND wp_term_relationships.term_taxonomy_id = 20 
GROUP BY s.post_id


Seleccionar todos los ids de los productos con variacion 
y posteriormente seleccionar todos los postmeta asociados a eso.

SELECT  x.ID, y.ID as post_parent, r.meta_key, r.meta_value, s.meta_key, s.meta_value
FROM wp_term_relationships
JOIN wp_posts as y ON y.ID = wp_term_relationships.object_id
JOIN wp_posts as x ON y.ID = x.post_parent
JOIN wp_postmeta as r ON r.post_id = x.ID
JOIN wp_postmeta as s ON s.post_id = x.ID
WHERE wp_term_relationships.term_taxonomy_id = 158
AND r.meta_key = '_regular_price' 
AND s.meta_key = '_sale_price'
GROUP BY x.ID

SELECT  hijo.ID, padre.ID as post_parent, r.meta_key, r.meta_value, s.meta_key, s.meta_value 
FROM wp_postmeta AS s
JOIN wp_term_relationships ON wp_term_relationships.object_id = s.post_id
JOIN wp_posts as padre ON padre.ID = wp_term_relationships.object_id
JOIN wp_posts as hijo ON padre.ID = hijo.post_parent
JOIN wp_postmeta as r ON r.post_id = hijo.ID
WHERE wp_term_relationships.term_taxonomy_id = 158
AND r.meta_key = '_regular_price' 
AND s.meta_key = '_sale_price'

SELECT  hijo.ID, padre.ID as post_parent
FROM wp_postmeta AS s
JOIN wp_term_relationships ON wp_term_relationships.object_id = s.post_id
JOIN wp_posts as padre ON padre.ID = wp_term_relationships.object_id
JOIN wp_posts as hijo ON padre.ID = hijo.post_parent
WHERE wp_term_relationships.term_taxonomy_id = 158
GROUP BY hijo.ID



UPDATE wp_postmeta AS s 
JOIN wp_term_relationships ON wp_term_relationships.object_id = s.post_id
JOIN wp_posts as padre ON padre.ID = wp_term_relationships.object_id
JOIN wp_posts as hijo ON padre.ID = hijo.post_parent
JOIN wp_postmeta as r ON r.post_id = hijo.ID
SET r.meta_value = ROUND(s.meta_value * .40, 2) 
WHERE wp_term_relationships.term_taxonomy_id = 158
AND s.meta_key = '_regular_price' 
AND r.meta_key = '_sale_price'
AND s.post_id	= 8190


UPDATE wp_postmeta AS s 
JOIN wp_term_relationships ON wp_term_relationships.object_id = s.post_id
JOIN wp_posts as padre ON padre.ID = wp_term_relationships.object_id
JOIN wp_posts as hijo ON padre.ID = hijo.post_parent
JOIN wp_postmeta as r ON r.post_id = hijo.ID
SET r.meta_value = ROUND(s.meta_value * .40, 2) 
WHERE wp_term_relationships.term_taxonomy_id = 158
AND s.meta_key = '_regular_price' 
AND r.meta_key = '_sale_price'
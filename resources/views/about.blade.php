@extends("partials.layouts.landing_layout")

@section("content")
<!--
  Content Section Start
-->
<main>
  <div class="container">
    <br>
    <div class="row">
      <div class="col s12 l6">
       <h5 class="header" style="text-align:center; color:#003c4c">Visión</h5>
          <p style="text-align:center; color:#003c4c">
            Nuestra visión es proporcionar al público en general una plataforma web amigable para pagos electrónicos con tecnología de punta, métricas integradas y autogestión.
          </p>
         </div>
         <div class="col s12 l6 center">
           <img class="responsive-img img-circle" src="/img/vision.jpg">
         </div>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col s12 l6 center">
           <img class="responsive-img img-circle" src="/img/mision.jpg" style="width:240px; height:auto;">
        </div>
       <div class="col s12 l6 center">
          <h5 class="header" style="text-align:center; color:#003c4c">Misión</h5>
          <p style="text-align:center; color:#003c4c">
            Brindar a los innovadores en electrónica las herramientas para hacer florecer su ingenio a través de pedidos confiables y seguros.
          </p>
       </div>
             <!--div class="card-action">
               <a href="#">This is a link</a>
             </div-->
    </div>



  </div>
</main>
      <!-- END OF MAIN -->
<!--
  Content Section End
-->
@endsection


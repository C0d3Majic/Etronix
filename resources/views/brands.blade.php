@extends("partials.layouts.brands_layout")

@section("content")
<!--
  Content Section Start
-->
<main>
  <div class="row">
    <div class="col s12">
      <div class="card blue-white darken-1">
        <div class="card-content black-text">
          <span style="text-align:center" class="card-title"><b>Tabla de Marcas</b></span>
          <div class="row">
            <div class="col s12">
                <table id="users" class="highlight responsive-table">
                  <thead>
                    <tr>
                        <th>Nombre</th>
                        <th style="text-align:right">Acciones</th>
                    </tr>
                  </thead>
                  <tbody>                 
                    @foreach($brands as $brand)
                      <tr>
                        <td>{{$brand->name}}</td>
                        <td style="text-align:right">
                            <a class="editBrand small material-icons tooltipped" data-brand_id="{{$brand->id}}" data-position="bottom" data-delay="50" data-tooltip="Editar"><i style="cursor: pointer;" class="small material-icons">mode_edit</i></a>
                            <a class="deleteBrand small material-icons tooltipped" data-brand_id="{{$brand->id}}" data-position="bottom" data-delay="50" data-tooltip="Eliminar"><i style="cursor: pointer;" class="small material-icons">delete</i></a>
                        </td>  
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col s4 push-s5">
      <a id="btnAgregar" style="background-color:#0D2948" data-target="modal3" class="waves-effect waves-light btn modal-trigger">Agregar Marca</a>
    </div>
  </div>
</main>

<!-- Modal Structure -->
<div id="modal1" class="modal">
  <div class="modal-content">
  </div>
  <div class="modal-footer">
    <div class="row">
      <div class="col s6">
        <a href="#!" id="btnGuardar" class="modal-action modal-close waves-effect waves-light btn">Guardar</a>
      </div>
      <div class="col s6">
        <a href="#!" id="btnCancelarEditar" style="background:red" class="modal-action modal-close waves-effect waves-light btn">Cancelar</a>
      </div>
    </div>
  </div>
</div>

<!-- Modal Structure -->
<div id="modal5" class="modal">
  <div class="modal-content">
  </div>
  <div class="modal-footer">
  </div>
</div>

<!-- Modal Structure -->
<div id="modal2" class="modal">
  <div class="modal-content">
    <h4>Eliminar usuario</h4>
    <p>Estas seguro de querer eliminar a este usuario?</p>
  </div>
  <div class="modal-footer">
    <div class="row">
      <div class="col s6">
        <a href="#!" id="btnEliminar" class="modal-action modal-close waves-effect waves-light btn">Aceptar</a>
      </div>
      <div class="col s6">
        <a href="#!" id="btnCancelarEliminar" style="background:red" class="modal-action modal-close waves-effect waves-light btn">Cancelar</a>
      </div>
    </div>
  </div>
</div>

<!-- Modal Structure --> 
<div id="modal3" class="modal">
  <div class="modal-content">
    <h4 class="center">Llena los datos para la nueva Marca</h4>
    <div class="row">
      <div class="input-field col m6 push-m3 l6 push-l3 s12">
        <i class="material-icons prefix">adb</i>
        <input id="new_name" name="new_name" type="text" class="validate">
        <label for="new_name">Nombre de la marca</label>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <div class="row">
      <div class="col s6 center">
        <a id="btnGuardarNuevo" class="modal-action modal-close waves-effect waves-light btn">Guardar</a>
      </div>
      <div class="col s6 center">
        <a id="btnCancelar" style="background:red" class="modal-action modal-close waves-effect waves-light btn">Cancelar</a>
      </div>
    </div>
  </div>
</div>
<!--
  Content Section End
-->
@endsection
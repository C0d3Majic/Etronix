<!DOCTYPE html>
  <html>
    <head>
      <!--Import Google Icon Font-->
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="/css/materialize.min.css"  media="screen,projection"/>

      <link rel="stylesheet" href="/css/stylesheet.css" type="text/css" charset="utf-8" />
      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.15/datatables.min.css"/>

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <title>Etronix</title>
      <style type="text/css">
      /*body {
        display: flex;
        min-height: 100vh;
        flex-direction: column;
      }

      main {
        flex: 1 0 auto;
      }*/
      nav a.active{
        background: rgba(255,255,255,0.4);
      }
      #sidenav-overlay{
        z-index: 995
      }
      /*ul > li > a {padding:0 59px !important;}*/

      body{
        /*background-color: rgba(128,128,128,1);*/
        background-color: black;
      }

      body {
        display: flex;
        min-height: 100vh;
        flex-direction: column;
      }

      main {
        flex: 1 0 auto;
      }

      .tag {
         float: left;
         position: absolute;
         left: 0px;
         top: 0px;
         z-index: 1000;
         background-color: #92AD40;
         padding: 5px;
         color: #FFFFFF;
         font-weight: bold;
      }

      .indicators{
        visibility: hidden;
      }

      </style>
    </head>
    <body class="blue-grey lighten-3">
      <main>
        <!-- Dropdown Structure -->
        <ul id="dropdown1" class="dropdown-content">
          <li><a id="btnLogout">Cerrar Sesion</a></li>
          <li class="divider"></li>
        </ul>
        <nav style="background-color:#0D2948" class="navbar-fixed">
          <div class="nav-wrapper">
            <a href="{{url('/')}}" class="brand-logo center"><img class="responsive-img" src="/img/logo.jpg" style="width:auto; height:64px;"></a>
            <ul class="right hide-on-med-and-down">
              <li><a style="color:#FFF" href="{{url('perfil')}}">Mi Perfil</a></li>
              <li><a style="color:#FFF" href="{{url('cards')}}">Mis Tarjetas</a></li>
              <li><a style="color:#FFF" href="{{url('payments')}}">Mis Servicios</a></li>
              <!-- Dropdown Trigger -->
              <li><a style="color:#FFF" class="dropdown-button" data-beloworigin="true" href="#!" data-activates="dropdown1">{{$username}}<i class="material-icons right">arrow_drop_down</i></a></li>
            </ul>
            <ul id="slide-out" class="side-nav" style="background-color: white">
              <li><a style="color:black" href="{{url('cards')}}">Mis Tarjetas</a></li>
              <li><a style="color:black" href="{{url('payments')}}">Mis Servicios</a></li>
              <li><a style="color:black" id="btnLogout2"><i style="color:#000" class="material-icons left">person </i>Cerrar Sesion</a></li>
            </ul>
            <a href="#" data-activates="slide-out" class="button-collapse"><i style="color:black" class="material-icons">menu</i></a>
          </div>
        </nav>

        <container>
          <div class="row">
            <div class="col s12">
              <!-- welcome Card -->
              <div id="welcome" class="card white">
                <div class="card-content black-text">
                  <span class="card-title"><center><h4><strong>Estas son las tarjetas que tienes registradas </strong></h4>{{$username}}</center></span>
                </div>
                <div class="row">
                  <div class="col s12">
                      <table id="cards" class="highlight centered responsive-table">
                        <thead>
                          <tr>
                              <th>Propietario de la tarjeta</th>
                              <th>Número</th>
                              <th>Mes de expiracion</th>
                              <th>Año de expiracion</th>
                              <th>Acciones</th>
                          </tr>
                        </thead>

                        <tbody>
                              <tr>
                                <td>Daniel Jarquin</td>
                                <td>4231 4234 5345 6456</td>
                                <td>02</td>
                                <td>26</td>
                                <td>
                                    <a class="editCard small material-icons tooltipped btn-floating" data-card_id="1" data-position="bottom" data-delay="50" data-tooltip="Editar"><i class="small material-icons">mode_edit</i></a>
                                    <a class="deleteCard small material-icons tooltipped btn-floating" data-card_id="1" data-position="bottom" data-delay="50" data-tooltip="Eliminar"><i class="small material-icons">delete</i></a></td>
                                </td>
                              </tr>
                        </tbody>
                      </table>
                </div>
              </div>
          </div>

          <div class="row">
            <div class="col s12" style="text-align:center">
              <button href="#modal3" class="btn waves-effect waves-light" id="btnAgregar" name="agregar"> Agregar Tarjeta </button>
            </div>
          </div>
        </container>
      </main>
      <footer class="page-footer" style="background-color:#0D2948">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">Etronix</h5>
          <p class="grey-text text-lighten-4">We are a company with a strong will to deliver the tecnology products you need to made this world awesome.</p>


        </div>
        <div class="col l4 offset-l2 s12">
            <h5 class="white-text">Stay in touch</h5>
            <ul>
                <li><a style="color:white" href="#!" target="_blank"><i class="mdi mdi-facebook-box"></i></a></li>
                <li><a style="color:white" href="#!"><i class="mdi mdi-twitter-box"></i></a></li>
                <li><a style="color:white" href="#!"><i class="mdi mdi-gmail"></i>contact@etronix.com</a></li>
            </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
        <div class="container center">
            © Etronix all rights reserved 2018
        </div>
    </div>
</footer>


      <!-- Modal Structure -->
      <div id="modal1" class="modal">
        <div class="modal-content">
        </div>
        <div class="modal-footer">
          <div class="row">
            <div class="col s6">
              <a href="#!" id="btnGuardar" class="modal-action modal-close waves-effect waves-light btn">Guardar</a>
            </div>
            <div class="col s6">
              <a href="#!" id="btnCancelarModal1" style="background:red" class="modal-action modal-close waves-effect waves-light btn">Cancelar</a>
            </div>
          </div>
        </div>
      </div>

      <!-- Modal Structure -->
      <div id="modal2" class="modal">
        <div class="modal-content">
          <h4>Eliminar usuario</h4>
          <p>Estas seguro de querer eliminar esta tarjeta?</p>
        </div>
        <div class="modal-footer">
          <div class="row">
            <div class="col s6">
              <a href="#!" id="btnEliminar" class="modal-action modal-close waves-effect waves-light btn">Aceptar</a>
            </div>
            <div class="col s6">
              <a href="#!" id="btnCancelarModal2" style="background:red" class="modal-action modal-close waves-effect waves-light btn">Cancelar</a>
            </div>
          </div>
        </div>
      </div>

      <!-- Modal Structure -->
      <div id="modal3" class="modal">
        <div class="modal-content">
          <h5>Agregando nueva tarjeta</h5>
          <div class="row">
            <div class="input-field col s12 m6 l6">
              <i class="material-icons prefix">account_circle</i>
              <input id="holder_name" data-openpay-card="holder_name" type="text" class="validate">
              <label for="holder_name">Nombre del titular</label>
            </div>
            <div class="input-field col s12 m6 l6">
              <i class="material-icons prefix">credit_card</i>
              <input id="card_number" readonly data-openpay-card="card_number" type="number"  oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="16" class="validate">
              <label for="card_number">Número de tarjeta</label>
            </div>
          </div>
          <div class="row center">
            <div class="col s12">
              <h6><b>Fecha de expiración</b></h6>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s6">
             <input id="mes" readonly placeholder="Mes" data-openpay-card="expiration_month" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength = "2" min="1" max="12" id="fecha_mes" type="number" class="validate">
             <label for="fecha_mes">Mes</label>
            </div>
            <div class="input-field col s6">
             <input id="año" readonly placeholder="Año" data-openpay-card="expiration_year" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength = "2" min="01" max="99" id="fecha_año" type="number" class="validate">
             <label for="fecha_año">Año</label>
            </div>
          </div>
       </div>
        <div class="modal-footer">
          <div class="row">
            <div class="col s6">
              <a href="#!" id="btnGuardarNuevo" class="modal-action modal-close waves-effect waves-light btn">Guardar</a>
            </div>
            <div class="col s6">
              <a href="#!" style="background:red" class="modal-action modal-close waves-effect waves-light btn">Cancelar</a>
            </div>
          </div>
        </div>
      </div>

      <!-- ================================================
      Scripts
      ================================================ -->

      <!-- jQuery Library-->
      <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="/js/jquery-3.1.1.js"></script>
      <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.15/datatables.min.js"></script>

      <!--materialize js-->
      <script type="text/javascript" src="/js/materialize.min.js"></script>
      <script type="text/javascript">
        $(document).ready(function(){
          var card_id = '';
          $('.button-collapse').sideNav({
               menuWidth: 300, // Default is 300
               edge: 'left', // Choose the horizontal origin
               closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
               draggable: true // Choose whether you can drag to open on touch screens
             }
          );
          $('#cards').DataTable({
              responsive: true
          }); //Datatable instantiation

          $('.modal').modal(
            {
              dismissible: false
            }
          );

          $('body').on('click', '.editCard', function(){
            card_id = $(this).data('card_id');
            var jsonObject = {
                session_id: "{{$session_id}}",
                session_token : "{{$session_token}}",
                card_id : card_id
            }
            $.ajax({
              // The URL for the request. variable set above
              url: "{{url('user/edit/card')}}",
              // The data to send (will be converted to a query string). variable set above
              data: jsonObject,
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              // Whether this is a POST or GET request
              type: "POST",
              // The type of data we expect back. can be json, html, text, etc...
              dataType : "json",
              // Code to run if the request succeeds;
              // the response is passed to the function
              success: function( msg ) {
                if(msg.status == 'error'){
                  var $toastContent = $('<span>'+ msg.type +'</span>');
                  Materialize.toast($toastContent, 2000, 'rounded red darken-3');
                }else if(msg.status == 'success'){
                  $('.modal-content').html(msg.modal);
                  $('select').material_select();
          				$('#modal1').modal('open');
                }
              },
              error: function(){
                //window.location.reload();
                var $toastContent = $('<span>Hubo un error en el servidor</span>');
                Materialize.toast($toastContent, 2000, 'rounded red darken-3');
              }
            });
      		});

          $('body').on('click', '.deleteCard', function(){
            card_id = $(this).data('card_id');
            var jsonObject = {
                session_id: "{{$session_id}}",
                session_token : "{{$session_token}}",
                card_id : card_id
            }
            $.ajax({
              // The URL for the request. variable set above
              url: "{{url('user/delete/card')}}",
              // The data to send (will be converted to a query string). variable set above
              data: jsonObject,
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              // Whether this is a POST or GET request
              type: "POST",
              // The type of data we expect back. can be json, html, text, etc...
              dataType : "json",
              // Code to run if the request succeeds;
              // the response is passed to the function
              success: function( msg ) {
                if(msg.status == 'error'){
                  var $toastContent = $('<span>'+ msg.type +'</span>');
                  Materialize.toast($toastContent, 2000, 'rounded red darken-3');
                }else if(msg.status == 'success'){
                  $('.modal-content').html(msg.modal);
                  $('select').material_select();
          				$('#modal2').modal('open');
                }
              },
              error: function(){
                //window.location.reload();
                var $toastContent = $('<span>Hubo un error en el servidor</span>');
                Materialize.toast($toastContent, 2000, 'rounded red darken-3');
              }
            });
      		});

          $('#btnLogout2').click(function(){
            // Start $.ajax() method
            var jsonObject = {
                session_id: "{{$session_id}}",
                session_token : "{{$session_token}}"
            }
            $.ajax({
              // The URL for the request. variable set above
              url: "{{url('user/logout')}}",
              // The data to send (will be converted to a query string). variable set above
              data: jsonObject,
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              // Whether this is a POST or GET request
              type: "POST",
              // The type of data we expect back. can be json, html, text, etc...
              dataType : "json",
              // Code to run if the request succeeds;
              // the response is passed to the function
              success: function( msg ) {
                if(msg.status == 'error'){
                  var $toastContent = $('<span>'+ msg.type +'</span>');
                  Materialize.toast($toastContent, 2000, 'rounded red darken-3');
                }else if(msg.status == 'success'){
                  window.open("{{URL::to('/')}}","_self");
                }
              },
              error: function(){
                //window.location.reload();
                var $toastContent = $('<span>Hubo un error en el servidor</span>');
                Materialize.toast($toastContent, 2000, 'rounded red darken-3');
              }
            });
        	});

          $('#btnCancelarModal1').click(function(){
            Materialize.toast('<span>Edicion Cancelada</span>', 1000, 'rounded cyan darken-3');
            setTimeout(function(){window.location.reload();}, 1000);
          });

          $('#btnCancelarModal2').click(function(){
            Materialize.toast('<span>Eliminacion Cancelada</span>', 1000, 'rounded cyan darken-3');
            setTimeout(function(){window.location.reload();}, 1000);
          });

          $('#btnAgregar').click(function(){
            $('#modal3').modal('open');
          });

          $("#holder_name").change(function(){
            $("#card_number").attr("readonly", false);
          });

          $("#card_number").change(function(){
            $("#mes").attr("readonly", false);
          });

          $("#mes").change(function(){
            $("#año").attr("readonly", false);
          });

          $("#mes").focusout(function(){
            var value = $('#mes').val();
            if(value.length==1)
              value = "0" + value;
            $('#mes').val(value);
          })

          $("#año").change(function(){
            var value = $('#año').val();
            if(value.length==1)
              value = "0" + value;
            $('#año').val(value);
          })

          $('#btnGuardarNuevo').click(function(){
            // Start $.ajax() method
            var holder_name = $("#holder_name").val();
            var card_number = $("#card_number").val();
            var mes = $("#mes").val();
            var año = $("#año").val();
            var jsonObject = {
                titular : holder_name,
                tarjeta_numero : card_number,
                mes : mes,
                año : año,
                session_id: "{{$session_id}}",
                session_token : "{{$session_token}}"
            }
            console.log(jsonObject);
            $.ajax({
              // The URL for the request. variable set above
              url: "{{url('user/add/card')}}",
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              // The data to send (will be converted to a query string). variable set above
              data: jsonObject,
              // Whether this is a POST or GET request
              type: "POST",
              // The type of data we expect back. can be json, html, text, etc...
              dataType : "json",
              // Code to run if the request succeeds;
              // the response is passed to the function
              success: function( msg ) {
                if(msg.status == 'error'){
                  var $toastContent = $('<span>'+ msg.type +'</span>');
                  Materialize.toast($toastContent, 2000, 'rounded red darken-3');
                }else if(msg.status == 'success'){
                  Materialize.toast('<span>Tarjeta Agregada Exitosamente</span>', 2000, 'rounded green darken-3');
                  setTimeout(function(){window.location.reload();}, 2000);
                }
              },
              error: function(){
                //window.location.reload();
                var $toastContent = $('<span>Hubo un error en el servidor</span>');
                Materialize.toast($toastContent, 2000, 'rounded red darken-3');
              }
            });
          });

          $('#btnEliminar').click(function(){
            // Start $.ajax() method
            var jsonObject = {
                session_id: "{{$session_id}}",
                session_token : "{{$session_token}}",
                card_id : card_id
            }
            console.log(jsonObject);
            $.ajax({
              // The URL for the request. variable set above
              url: "{{url('user/card/delete')}}",
              // The data to send (will be converted to a query string). variable set above
              data: jsonObject,
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              // Whether this is a POST or GET request
              type: "POST",
              // The type of data we expect back. can be json, html, text, etc...
              dataType : "json",
              // Code to run if the request succeeds;
              // the response is passed to the function
              success: function( msg ) {
                if(msg.status == 'error'){
                  var $toastContent = $('<span>'+ msg.type +'</span>');
                  Materialize.toast($toastContent, 2000, 'rounded red darken-3');
                }else if(msg.status == 'success'){
                  Materialize.toast('<span>Tarjeta Eliminada Exitosamente</span>', 2000, 'rounded green darken-3');
                  window.location.reload();
                }
              },
              error: function(){
                //window.location.reload();
                var $toastContent = $('<span>Hubo un error en el servidor</span>');
                Materialize.toast($toastContent, 2000, 'rounded red darken-3');
              }
            });
        	});

          $('#btnGuardar').click(function(){
            // Start $.ajax() method
            var holder_name = $("#holder_name").val();
            var card_number = $("#card_number").val();
            var mes = $("#mes").val();
            var año = $("#año").val();
            var jsonObject = {
                card_id : card_id,
                titular : holder_name,
                tarjeta_numero : card_number,
                mes : mes,
                año : año,
                session_id: "{{$session_id}}",
                session_token : "{{$session_token}}"
            }
            console.log(jsonObject);
            $.ajax({
              // The URL for the request. variable set above
              url: "{{url('user/edit/card/save')}}",
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              // The data to send (will be converted to a query string). variable set above
              data: jsonObject,
              // Whether this is a POST or GET request
              type: "POST",
              // The type of data we expect back. can be json, html, text, etc...
              dataType : "json",
              // Code to run if the request succeeds;
              // the response is passed to the function
              success: function( msg ) {
                if(msg.status == 'error'){
                  var $toastContent = $('<span>'+ msg.type +'</span>');
                  Materialize.toast($toastContent, 2000, 'rounded red darken-3');
                }else if(msg.status == 'success'){
                  Materialize.toast('<span>Tarjeta Editada Exitosamente</span>', 2000, 'rounded green darken-3');
                  setTimeout(function(){window.location.reload();}, 2000);
                }
              },
              error: function(){
                //window.location.reload();
                var $toastContent = $('<span>Hubo un error en el servidor</span>');
                Materialize.toast($toastContent, 2000, 'rounded red darken-3');
              }
            });
          });

        	$('#btnLogout').click(function(){
            // Start $.ajax() method
            var jsonObject = {
                session_id: "{{$session_id}}",
                session_token : "{{$session_token}}"
            }
            $.ajax({
              // The URL for the request. variable set above
              url: "{{url('user/logout')}}",
              // The data to send (will be converted to a query string). variable set above
              data: jsonObject,
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              // Whether this is a POST or GET request
              type: "POST",
              // The type of data we expect back. can be json, html, text, etc...
              dataType : "json",
              // Code to run if the request succeeds;
              // the response is passed to the function
              success: function( msg ) {
                if(msg.status == 'error'){
                  var $toastContent = $('<span>'+ msg.type +'</span>');
                  Materialize.toast($toastContent, 2000, 'rounded red darken-3');
                }else if(msg.status == 'success'){
                  window.open("{{URL::to('/')}}","_self");
                }
              },
              error: function(){
                //window.location.reload();
                var $toastContent = $('<span>Hubo un error en el servidor</span>');
                Materialize.toast($toastContent, 2000, 'rounded red darken-3');
              }
            });
        	});
          $('select').material_select();
          $(".dropdown-button").dropdown();
        });
      </script>
   </body>
  </html>

@extends("partials.layouts.cart_layout")

@section("content")
<!--
  Content Section Start
-->

<main>
  @if(!count($items))
    <div class="valign-wrapper">
        <div class="valign" style="width:100%; top:50%">
            <center>   
                <div class="row">
                    <div class="col s12">
                        <div class="card white">
                            <div class="card-content black-text">
                            <span class="card-title">Tu Carrito Esta Vacio!</span>
                            </div>
                        </div>
                    </div>
                </div>
            </center>
        </div>
    </div>
  @else
  <div class="row">
    <div class="col s12">
      <div class="card blue-white darken-1">
        <div class="card-content black-text">
          <span style="text-align:center" class="card-title"><b>Tu Carrito</b></span>
          <div class="row">
            <div class="col s12">
                <table id="users" class="highlight responsive-table">
                  <thead>
                    <tr>
                        <th>Número de Parte</th>
                        <th>Marca</th>
                        <th>Nombre</th>
                        <th>Precio</th>
                        <th>Cantidad</th>
                        <th style="text-align:right">Acciones</th>
                    </tr>
                  </thead>
                  <tbody>                 
                    @foreach($items as $item)
                      <tr>
                        <td>{{$item->part_number}}</td>
                        <td>{{$item->brand->name}}</td>
                        <td>{{$item->name}}</td>
                        <td>{{$item->price}}</td>
                        <td>{{$item->amount}}</td>
                        <td style="text-align:right">
                            <a class="addItem small material-icons tooltipped" data-item_id="{{$item->id}}" data-position="bottom" data-delay="50" data-tooltip="Incrementar"><i class="small material-icons" style="color:#0D2948; cursor: pointer;">add_box</i></a>
                            <a class="removeItem small material-icons tooltipped" data-item_id="{{$item->id}}" data-position="bottom" data-delay="50" data-tooltip="Disminuir"><i class="small material-icons" style="color:#0D2948; cursor: pointer;">indeterminate_check_box</i></a>
                            <a class="deleteItem small material-icons tooltipped" data-item_id="{{$item->id}}" data-position="bottom" data-delay="50" data-tooltip="Eliminar"><i class="small material-icons" style="color:#0D2948; cursor: pointer;">delete</i></a>
                        </td>  
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col s12">
        <div class="card blue-white darken-1">
          <div class="card-content black-text">
            <span style="text-align:center" class="card-title"><b>El resumen de tu orden</b></span>
            <div class="row">
              <div class="col s12">
                <span style="text-align:left;font-size:18px"><b>Subtotal: </b>${{$subtotal}}</span></br>
                <span style="text-align:left;font-size:18px"><b>Iva: </b>${{$iva}}</span></br>
                <span id="total" style="text-align:left;font-size:18px"><b>Total: </b>${{$total}}</span></br>
              </div>
              <div class="col s12 center">
                <div id="paypal-button" style="padding-top:25px"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col s12 center">
      <a id="btnCancelar" style="background-color:#0D2948" data-target="modal3" class="waves-effect waves-light btn modal-trigger">Vaciar Carrito</a>
    </div>
  </div>
  @endif
</main>

<!--
  Content Section End
-->
@endsection
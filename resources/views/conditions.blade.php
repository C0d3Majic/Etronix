@extends("partials.layouts.landing_layout")

@section("content")
<div class="container">
    <h4>AVISO DE PRIVACIDAD</h4>
    <p style="text-align:left">
        <b>IDENTIDAD Y DOMICILIO DEL RESPONSABLE</b></br> 
        Daniel Arturo Jarquín Moreno, (en lo sucesivo mencionado indistintamente como “eTronixs”, “Robobeginners”, 
        “nuestro o “nosotros”) se preocupa por la confidencialidad y seguridad de los datos personales de sus clientes 
        y tiene el compromiso de proteger su privacidad y cumplir con la legislación aplicable a la protección de datos 
        personales en posesión de los particulares. Ëtronixs es el responsable de recabar sus datos personales bajo su 
        consentimiento y nuestro domicilio es el ubicado en Circuito Golondrinas No. 8050 1ª del Fracc. Paseos del alba 
        en Ciudad Juárez, Chihuahua, México C.P. 32696. Nuestros datos de contacto se encuentran en www.etronixs.com.mx . 
        La persona responsable de  los datos personales es Genaro Arturo Jarquín Fernández y lo puedes encontrar en la 
        misma dirección o en el correo electrónico ventas@etronixs.com.mx 
        </br> 
        <b>DESCRIPCIÓN DEL AVISO </b></br>  
        Los términos que se utilicen en el presente aviso de privacidad (en lo sucesivo referido como “Aviso”) tendrán 
        la definición que a los mismos le asigna la Ley Federal de Protección de Datos Personales en Posesión de Particulares, 
        publicada en el Diario Oficial de la Federación el 26 de Enero de 2017 (en lo sucesivo referida como la “Ley”), 
        con excepción expresa de aquellos que aquí se definen. Este Aviso especifica el tipo de datos que eTronixs recolecta, 
        los fines para los cuales lleva a cabo el tratamiento de los mismos, así como los derechos con que cuenta el titular 
        al respecto.
        </br> 
        <b>DATOS PERSONALES QUE RECABA DECOMPRAS</b></br>  
        Los datos personales que recaba eTronixs son: (i) nombres, apellidos, domicilios, teléfonos, correos electrónicos, 
        fechas de nacimiento, sexo, información de formas de pago (Tarjetas de crédito, débito o bancarias, medios digitales 
        o virtuales); (ii) toda aquella información obtenida a través de otras fuentes que están permitidas por la ley. 
        Toda información personal que identifica al usuario, revelada de manera directa o por cualquier medio de contacto y/o 
        foro público de conexión en línea, podrá ser recopilada y tratada por eTronixs.</br>
        La recolección de datos que realiza eTronixs es de buena fé y por tal motivo presume que los mismos son verídicos, 
        correctos, completos e identifican al titular que los suministra y/o provee, por lo que es responsabilidad del titular 
        que los datos que éste le proporcione a eTronixs cumplan con tales características y se actualicen en la medida que 
        se requiera. De igual forma, eTronixs se reserva el derecho de ejercer las acciones que considere pertinentes en caso 
        de falsedad de datos.
        Sus datos personales serán tratados con base a en los principios de licitud, consentimiento, información, calidad, 
        finalidad, lealtad, proporcionalidad y responsabilidad en términos de la Legislación.
        </br> 
        <b>¿CON QUÉ FINALIDAD RECABA eTronixs LOS DATOS PERSONALES?</b></br>  
        Los datos personales podrán ser tratados y utilizados por eTronixs y/o nuestras subsidiarias y/o afiliadas y/o 
        terceros, nacionales y/o extranjeros para llevar a cabo algunas o todas las actividades relacionadas con el 
        cumplimiento de las obligaciones que deriven de la relación comercial que se originen y/o deriven de la prestación 
        de los Servicios; con el propósito de informar a los usuarios del lanzamiento o cambios de nuevos productos, 
        servicios, promociones y/u ofertas de nosotros y/o de terceros, realizar estudios sobre hábitos de consumo y de 
        mercado, así como para cualquier otra actividad tendiente a promover, mantener, mejorar y evaluar los Servicios.
        </br> 
        <b>LIMITACIÓN DE USO Y DIVULGACIÓN DE INFORMACIÓN.</b></br>
        En nuestro programa de notificación de promociones, ofertas y servicios a través de correo electrónico, 
        sólo eTronixs tiene acceso a la información recabada. Este tipo de publicidad se realiza mediante avisos 
        y mensajes promocionales de correo electrónico, los cuales sólo serán enviados a usted y a aquellos contactos 
        registrados para tal propósito, esta indicación podrá usted modificarla en cualquier momento enviando un correo 
        a clientes@eTronixs.com.mx . En los correos electrónicos enviados, pueden incluirse ocasionalmente ofertas de 
        terceras partes que sean nuestros socios comerciales.     
        </br> 
        <b>LOS DATOS PERSONALES PUEDEN SER TRANSFERIDOS O REMITIDOS.</b></br>  
        eTronixs podrá transferir los datos personales que haya recibido y/o recolectado y/o llegue a recibir y/o 
        recolectar de sus usuarios a sus subsidiarias y/o afiliadas ya sean nacionales y/o extranjeros, y/o cualquier 
        autoridad competente que así lo solicite para llevar a cabo las finalidades descritas en el párrafo que antecede. 
        La transferencia de los datos personales del usuario se encuentra limitada a aquellos actos, hechos y/o 
        procedimientos que eTronixs requiera implementar a efecto de estar en posibilidad de cumplir con sus obligaciones 
        contractuales, regulatorias y/o comerciales en el curso ordinario de sus operaciones.
        </br> 
        <b>DATOS  PERSONALES SENSIBLES</b></br>  
        eTronixs NO solicita datos personales sensibles en ninguna sección. (Origen racial o étnico, estado de salud 
        presente y futuro, información genética, creencias religiosas, filosóficas y morales, afiliación sindical, 
        opiniones políticas, preferencia sexual)
        </br> 
        <b>MEDIOS PARA EJERCER LOS DERECHOS DE ACCESO, RECTIFICACIÓN, CANCELACIÓN U OPOSICIÓN Y REVOCAR EL CONSENTIMIENTO OTORGADO.</b></br> 
        eTronixs cuenta con los recursos técnicos, materiales y humanos necesarios para asegurar que los datos 
        personales del usuario serán tratados en estricto apego a la Ley. En cualquier momento, el Usuario 
        tendrá el derecho a solicitar a eTronixs el ejercicio de los derechos que le confiere la Ley (derechos ARCO) o bien, 
        revocar el consentimiento que haya otorgado a eTronixs, para el tratamiento de sus datos personales, mediante el envío 
        de la solicitud correspondiente por escrito al siguiente correo electrónico clientes@etronixs.com.mx . 
        Queda entendido que para que eTronixs pueda atender correcta y oportunamente cualquiera de las solicitudes a las 
        que se refiere esta sección, la solicitud del titular deberá contener como mínimo: (i) Nombre completo del titular, 
        (ii) Número de contacto, (iii) descripción breve del objeto de la solicitud, y (iv) razones que motivan su solicitud. 
        Nuestra respuesta a su solicitud será enviada dentro del plazo permitido por la Ley (20 días) al correo electrónico 
        del remitente de la misma. El usuario reconoce y acepta que una de las finalidades del tratamiento de sus datos 
        personales es cumplir con sus obligaciones comerciales que mantiene y/o mantendrá con eTronixs, por lo que no podrá 
        cancelar y/u oponerse al tratamiento de datos personales que puedan y/o pudieran afectar y/o restringir el cumplimiento 
        de las mismas.
        </br> 
        <b>MODIFICACIONES AL AVISO DE PRIVACIDAD</b></br> 
        eTronixs expresamente se reserva el derecho, bajo su exclusiva discreción, de cambiar, modificar, agregar o 
        eliminar partes del presente Aviso en cualquier momento. En tal caso, eTronixs publicará en el sitio web 
        www.etronixs.com.mx las modificaciones que se le practiquen al Aviso e indicará en la parte superior de la 
        página electrónica de dicho sitio la fecha de última versión del Aviso. En la medida que el usuario lo solicite, 
        en los términos antes mencionados, la cancelación y/u oposición de sus datos personales y continúe accediendo y/o 
        utilizando, parcial o totalmente, los Servicios, implicará que ha aceptado y consentido tales cambios y/o modificaciones.
        Al proporcionarle sus datos personales a eTronixs, el usuario expresamente reconoce, entiende y acepta el presente Aviso, 
        según el mismo pueda ser modificado o ajustado de tiempo en tiempo, y otorga su consentimiento para que eTronixs proceda 
        con el procesamiento de sus datos personales de la forma que se explica en el presente. Si el usuario no acepta este Aviso, 
        podrá ejercer los derechos que le confiere la Ley, según se describe anteriormente
        Se advierte que en nuestros productos y/o servicios pueden encontrarse enlaces a sitios web, links, aplicaciones 
        que utilicen las plataformas de eTronixs y servicios de otras empresas que cuentan con sus propias políticas o avisos 
        de privacidad, las cuales le sugerimos se consulten con la finalidad de tener conocimiento de las mismas.
        eTronixs se compromete a asegurar la privacidad de la información personal obtenida a través de sus servicios en línea.
        </br> 
        <b>PROTECCIÓN</b></br>  
        La seguridad y la confidencialidad de los datos que los usuarios proporcionen al contratar un servicio o comprar 
        un producto en línea estarán protegidos por un servidor seguro bajo el protocolo Secure Socket Layer (SSL), de tal 
        forma que los datos enviados se transmitirán encriptados para asegurar su resguardo. Al momento de contratar un 
        servicio en línea, se pedirán datos bancarios para los cuales se les direcciona a una página Web (www.paypal.com.mx).</br>
        Para verificar que se encuentra en un entorno protegido asegúrese de que aparezca una S en la barra de navegación. 
        Ejemplo: httpS://. sin embargo, y a pesar de contar cada día con herramientas más seguras, la protección de los 
        datos enviados a través de Internet no se puede garantizar al 100%; por lo que una vez recibidos, se hará todo lo 
        posible por salvaguardar la información.
        </br> 
        <b>COOKIES</b></br>  
        Las cookies son pequeñas piezas de información que son enviadas por el sitio web al navegador.
        Se almacenan en el disco duro de los equipos y utilizan para determinar las preferencias de los usuarios que 
        acceden al portal electrónico (www.etronixs.com.mx ), así como para rastrear determinados comportamientos o actividades.
        Las cookies permiten reconocer a los usuarios, detectar el ancho de banda que han seleccionado, identificar la 
        información más destacada, calcular el tamaño de la audiencia y medir algunos parámetros de tráfico.
        En el caso de empleo de cookies, el botón de "ayuda" que se encuentra en la barra de herramientas de la mayoría 
        de los navegadores, le dirá cómo evitar aceptar nuevos cookies, cómo hacer que el navegador le notifique cuando 
        recibe un nuevo cookie o cómo deshabilitar todos los cookies.
        </br>
        <b>CONFIDENCIALIDAD</b></br>
        Cuando el usuario se encuentre en el sitio web (www.etronixs.com.mx ), compartirá su información con 
        Daniel Arturo Jarquin Moreno. Daniel Arturo Jarquin Moreno no tiene obligación alguna de mantener en 
        confidencial cualquier otra información que el usuario proporcione por medio de boletines y pláticas en línea 
        (chats), así como a través de las cookies, lo anterior con base en los términos establecidos en el artículo 
        109 de la Ley Federal del Derecho de Autor y del artículo 76 bis, fracción I, de la Ley Federal de Protección 
        al Consumidor. Sólo se podrá difundir la información en casos especiales, como identificar, localizar 
        o realizar acciones legales contra aquellas personas que infrinjan las condiciones de servicio del sitio 
        web (www.etronixs.com.mx ), causen daños a, o interfieran en, los derechos de Daniel Arturo Jarquin Moreno, 
        las propiedades de otros usuarios del portal o de cualquier persona que pudiese resultar perjudicada 
        por dichas acciones.
        </br>
        <b>CONFIDENCIALIDAD (MENORES DE EDAD)</b></br>
        Se recomienda a los padres y tutores que asesoren a los menores de edad al momento de navegar por Internet 
        y que aprueben el envío de cualquier tipo de información personal.
        </br>
        <b>ACEPTACIÓN DE TERMINOS</b></br>
        Esta declaración de Confidencialidad / Privacidad está sujeta a los términos y condiciones del sitio web 
        de www.etronixs.com.mx , lo cual constituye un acuerdo legal entre el usuario y eTronixs.
        Si el usuario utiliza los servicios de eTronixs, significa que ha leído, entendido y acordado los términos 
        antes expuestos.
        </br>
        <b>AUTORIDAD</b></br>
        Si el usuario considera que han sido vulnerados sus derechos respecto de la protección de datos personales, 
        tiene el derecho de acudir a la autoridad correspondiente para defender su ejercicio. La autoridad es el 
        Instituto Federal de Acceso a la Información y Protección de Datos (IFAI), su sitio web es: www.ifai.mx.
    </p>
</div>
<!--
  Content Section End
-->
@endsection
@extends("partials.layouts.landing_layout")

@section("content")
<!--
  Content Section Start
-->
    <main>
      <div class="container">
        <div class="row">
          <div class="card-panel grey lighten-5 z-depth-1" style="height:400px">
            <h5 class="header" style="text-align:center"> Escribe tus dudas o sugerencias</h5>
            <form class="col s12" id="contact-form" method="POST" action="{{url('contact/mail')}}">
              <div class="row" style="margin-bottom: 0px;">
                <div class="input-field col s6" style="margin-top: 0px;">
                  <input id="first_name" name="first_name" type="text" class="validate" required>
                  <label for="first_name">Nombre</label>
                </div>
                <div class="input-field col s6" style="margin-top: 0px;">
                  <input id="last_name" name="last_name" type="text" class="validate">
                  <label for="last_name">Apellidos</label>
                </div>
              </div>
              <div class="row" style="margin-bottom: 0px;">
                <div class="input-field col s6" style="margin-top: 0px;">
                  <input id="email" type="email" name="email" class="validate" required>
                  <label for="first_name">Correo</label>
                </div>
                <div class="input-field col s6" style="margin-top: 0px;">
                  <input id="subject" type="text" name="subject" class="validate" required>
                  <label for="subject">Asunto</label>
                </div>
              </div>
              <div class="row" style="margin-bottom: 0px;">
                <div class="input-field col s12" style="margin-top: 0px;">
                  <textarea id="message" name="message" class="materialize-textarea" required></textarea>
                  <label for="textarea1">Mensaje</label>
                </div>
              </div>
              <div class="row" style="text-align:center">
                <button class="btn-floating btn-large red tooltipped" data-position="bottom" data-delay="50" data-tooltip="Enviar" id="send_message" name="send"><i class="material-icons">send</i></a>
              </div>
            </form>
          </div>
        </div>
      </div>

    </main>
<!--
  Content Section End
-->
@endsection

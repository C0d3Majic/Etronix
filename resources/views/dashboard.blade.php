@extends("partials.layouts.dashboard_layout")

@section("content")
<!--
  Content Section Start
-->
<main>
  <!-- welcome Card -->
  <div id="welcome" class="card blue-grey">
    <div class="card-content white-text">
      <span class="card-title"><h4><strong><center>Bienvenido al BackEnd Administrativo Etronix</strong></center></h5></span>
    </div>
    <div class="row">
      <div class="col s12">
        <p style="color:white; text-align:justify;" class="flow-text">
          En esta seccion podras ver una breve reseña de las cosas importantes que estan pendientes de revisar e informacion general acerca de usuarios o eventos del sistema.
          </br>En la barra de navegacion superior tendras accesos a las secciones de agregar usuarios y ver los reportes.
        </p>
      </div>
    </div>
    <div class="row">
      <div class="col s12 m4 l4">
        <div class="card-panel blue-grey lighten-2" style="text-align: center; vertical-align: middle;">
          <i class="material-icons" style="font-size: 9rem; display: block;">perm_identity</i>
            <h5 class="white-text">Usuarios Activos  {<b> {{$active_users}} </b>}</h5>
        </div>
      </div>
      <div class="col s12 m4 l4">
        <div class="card-panel blue-grey lighten-2" style="text-align: center; vertical-align: middle;">
          <i class="material-icons" style="font-size: 9rem; display: block;">shopping_basket</i>
            <h5 class="white-text">Ventas del día {<b> {{$today_sales}} </b>}</h5>
        </div>
      </div>
      <div class="col s12 m4 l4">
        <div class="card-panel blue-grey lighten-2" style="text-align: center; vertical-align: middle;">
          <i class="material-icons" style="font-size: 9rem; display: block;">shopping_cart</i>
            <h5 class="white-text">Ventas pendientes {<b> {{$pending_sales}} </b>}</h5>
        </div>
      </div>
    </div>
  </div>
</main>
<!--
  Content Section End
-->
@endsection
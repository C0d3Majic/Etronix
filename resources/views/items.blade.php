@extends("partials.layouts.items_layout")

@section("content")
<!--
  Content Section Start
-->
<main>
  <div class="row">
    <div class="col s12">
      <div class="card blue-white darken-1">
        <div class="card-content black-text">
          <span style="text-align:center" class="card-title"><b>Tabla del Inventario</b></span>
          <div class="row">
            <div class="col s12">
                <table id="users" class="highlight responsive-table">
                  <thead>
                    <tr>
                        <th>Número de Parte</th>
                        <th>Proveedor</th>
                        <th>Marca</th>
                        <th>Categoria</th>
                        <th>Nombre</th>
                        <th>Precio</th>
                        <th>Cantidad en Inventario</th>
                        <th style="text-align:right">Acciones</th>
                    </tr>
                  </thead>
                  <tbody>                 
                    @foreach($items as $item)
                      <tr>
                        <td>{{$item->part_number}}</td>
                        <td>{{$item->provider->name}}</td>
                        <td>{{$item->brand->name}}</td>
                        <td>{{$item->category->name}}</td>
                        <td>{{$item->name}}</td>
                        <td>{{$item->price}}</td>
                        <td>{{$item->stock}}</td>
                        <td style="text-align:right">
                            <a class="imageItem small material-icons tooltipped" data-item_id="{{$item->id}}" data-position="bottom" data-delay="50" data-tooltip="Imagen"><i class="small material-icons">insert_photo</i></a>
                            <a class="editItem small material-icons tooltipped" data-item_id="{{$item->id}}" data-position="bottom" data-delay="50" data-tooltip="Editar"><i class="small material-icons">mode_edit</i></a>
                            <a class="deleteItem small material-icons tooltipped" data-item_id="{{$item->id}}" data-position="bottom" data-delay="50" data-tooltip="Eliminar"><i class="small material-icons">delete</i></a>
                        </td>  
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col s4 push-s5">
      <a id="btnAgregar" style="background-color:#0D2948" data-target="modal3" class="waves-effect waves-light btn modal-trigger">Agregar Articulo</a>
    </div>
  </div>
</main>

<!-- Modal Structure -->
<div id="modal1" class="modal">
  <div class="modal-content">
  </div>
  <div class="modal-footer">
    <div class="row">
      <div class="col s6">
        <a href="#!" id="btnGuardar" class="modal-action modal-close waves-effect waves-light btn">Guardar</a>
      </div>
      <div class="col s6">
        <a href="#!" id="btnCancelarEditar" style="background:red" class="modal-action modal-close waves-effect waves-light btn">Cancelar</a>
      </div>
    </div>
  </div>
</div>

<!-- Modal Structure -->
<div id="modal5" class="modal">
  <div class="modal-content">
  </div>
  <div class="modal-footer">
  </div>
</div>

<!-- Modal Structure -->
<div id="modal2" class="modal">
  <div class="modal-content">
    <h4>Eliminar usuario</h4>
    <p>Estas seguro de querer eliminar a este usuario?</p>
  </div>
  <div class="modal-footer">
    <div class="row">
      <div class="col s6">
        <a href="#!" id="btnEliminar" class="modal-action modal-close waves-effect waves-light btn">Aceptar</a>
      </div>
      <div class="col s6">
        <a href="#!" id="btnCancelarEliminar" style="background:red" class="modal-action modal-close waves-effect waves-light btn">Cancelar</a>
      </div>
    </div>
  </div>
</div>

<!-- Modal Structure --> 
<div id="modal3" class="modal">
  <div class="modal-content">
    <h4 class="center">Llena los datos para el nuevo Articulo</h4>
    <div class="row">
      <div class="input-field col m6 l6 s12">
        <i class="material-icons prefix">adb</i>
        <input id="new_name" name="new_name" type="text" class="validate">
        <label for="new_name">Nombre del articulo</label>
      </div>
      <div class="input-field col m6 l6 s12">
        <i class="material-icons prefix">adb</i>
        <input id="new_number" name="new_number" type="text" class="validate">
        <label for="new_number">Número de parte</label>
      </div>   
    </div>
    <div class="row">
        <div class="input-field col m6 l6 s12">
            <i class="material-icons prefix">people</i>
            <select name="new_provider" id="new_provider">
                <option value="" disabled selected>Proveedor?</option>
                @foreach($providers as $provider)
                    <option value="{{$provider->id}}">{{$provider->name}}</option>
                @endforeach
                </select>
                <label for="new_provider">Proveedor</label>
            </select>
        </div>   
        <div class="input-field col m6 l6 s12">
            <i class="material-icons prefix">branding_watermark</i>
            <select name="new_brand" id="new_brand">
                <option value="" disabled selected>Marca?</option>
                @foreach($brands as $brand)
                    <option value="{{$brand->id}}">{{$brand->name}}</option>
                @endforeach
                </select>
                <label for="new_brand">Marca</label>
            </select>
        </div> 
    </div>
    <div class="row">
        <div class="input-field col m6 l6 s12">
            <i class="material-icons prefix">branding_watermark</i>
            <select name="new_category" id="new_category">
            <option value="" disabled selected>Categoria?</option>
            @foreach($categories as $category)
                <option value="{{$category->id}}">{{$category->name}}</option>
            @endforeach
            </select>
            <label for="new_type">Categoria</label>
        </div>   
        <div class="input-field col m6 l6 s12">
            <i class="material-icons prefix">attach_money</i>
            <input id="new_price" name="new_price" type="text" class="validate">
            <label for="new_price">Precio del articulo</label>
        </div> 
    </div>  
    <div class="row">
        <div class="input-field col m6 l6 s12">
            <i class="material-icons prefix">store</i>
            <input id="new_stock" name="new_stock" type="text" class="validate">
            <label for="new_stock">Cantidad en almacen articulo</label>
        </div> 
        <div class="input-field col m6 l6 s12">
            <i class="material-icons prefix">check_circle</i>
            <select name="new_active" id="new_active">
                <option value="" disabled selected>Esta Disponible?</option>
                <option value="1">Si</option>
                <option value="0">No</option>
            </select>
            <label for="new_active">Disponible en tienda</label>
        </div>  
    </div>
    <div class="row">
        <div class="input-field col m8 push-m8 l8 push-l2 s12">
            <i class="material-icons prefix">comment</i>
            <textarea id="new_description" class="materialize-textarea"></textarea>
            <label for="new_description">Descripción</label>
        </div> 
    </div>
  </div>
  <div class="modal-footer">
    <div class="row">
      <div class="col s6 center">
        <a id="btnGuardarNuevo" class="modal-action modal-close waves-effect waves-light btn">Guardar</a>
      </div>
      <div class="col s6 center">
        <a id="btnCancelar" style="background:red" class="modal-action modal-close waves-effect waves-light btn">Cancelar</a>
      </div>
    </div>
  </div>
</div>
<!--
  Content Section End
-->
@endsection
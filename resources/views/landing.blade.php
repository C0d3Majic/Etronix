@extends("partials.layouts.landing_layout")

@section("content")
<!--
  Content Section Start
-->


<div>
    <div id="welcome" style="padding-top: 0px; " class="block section scrollspy">
        <div class="row">
            <div class="col s12">
                <div class="center-align">
                    <h1 style="color: #FFF"><img class="responsive-img" style="width:300px" src="img/logo.png"/></h1>
                </div>
            </div>
        </div>        
    </div>
</div>

<div class="container">
    <!--   Icon Section   -->
    <div class="row">
        <div class="col s12 l10">
            @foreach($threads as $thread)
            <div class="row">
                <div class="col s12 l10">
                    <a href="{{url('thread')}}/{{$thread->id}}">
                        <h5 style="color:#0D2948">{{$thread->title}} </h5>
                        <img class="responsive-img" src="{{$thread->splash_image}}">
                    </a><p style="color:#0D2948">{{$thread->initial_content}}</p>
                    
                </div>
            </div>
            
            <div class="row">
                <div class="col s12">
                    <ul style="display: flex; flex-direction: row;justify-content: flex-start; list-style: none;  white-space: wrap; margin-top:-30px">
                        <li style="font-weight:bold">Author: <span style="color:#0D2948;font-weight:normal">Daniel Jarquin</span></li>
                        <li style="text-align:center;margin-left:10px;font-weight:bold">Comentarios: <span style="color:#0D2948;font-weight:normal; ">{{$thread->comments}}</span></li>
                        <li style="margin-left:10px;font-weight:bold; white-space: wrap;">Fecha: <span style="color:#0D2948;font-weight:normal">{{$thread->date}}</span></li>
                    </ul>
                </div>
            </div>
            @endforeach            
        </div>
        <div class="col s12 l2">
            <div class="row">
                <div class="col s12 l12">
                    <h5 style="color:#0D2948; text-align:center">Categorias</h5>
                    <div id="list8">
                        <ul>
                        @foreach($categories as $category)
                            <li><a href="" style="color:white">{{$category->name}}</a></li>
                        @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12 l12">
                    <h5 style="color:#0D2948; text-align:center">Marcas</h5>
                    <div id="list8">
                        <ul>
                        @foreach($brands as $brand)
                            <li><a href="#!" style="color:white">{{$brand->name}}</a></li>
                        @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12 l12">
                    <h5 style="color:#0D2948" class="center">Lo más buscado</h5>
                    <ul class="center">
                        <li class="collection-item avatar center">
                            <div class="item">
                                <img style="width:50%" src="https://www.prometec.net/wp-content/uploads/2016/01/Leonardo-1-300x300.png" alt="" class="circle">
                                <span style="color:#0D2948; display:block">Arduino</span>
                            <div>
                        </li>
                        <li class="collection-item avatar center">
                            <div class="item">
                                <img style="width:50%" src="https://www.prometec.net/wp-content/uploads/2015/08/IMG_1220-300x300.jpg" alt="" class="circle">
                                <span style="color:#0D2948; display:block">Kit de inicio prometec</span>
                            <div>
                        </li>
                        <li class="collection-item avatar center">
                            <div class="item">
                                <img style="width:50%" src="https://www.prometec.net/wp-content/uploads/2018/02/HTB1.cQvQXXXXXcoXVXXq6xXFXXXB-520x535.jpg" alt="" class="circle">
                                <span style="color:#0D2948; display:block">Módulo de 16 relés</span>
                            <div>
                        </li>
                        <li class="collection-item avatar center">
                            <div class="item">
                                <img style="width:50%" src="https://www.prometec.net/wp-content/uploads/2017/02/HTB1AVrnHXXXXXajXFXXq6xXFXXXs-300x300.jpg" alt="" class="circle">
                                <span style="color:#0D2948; display:block">Attiny85</span>
                            <div>
                        </li>
                        <li class="collection-item avatar center">
                            <div class="item">
                                <img style="width:50%" src="https://www.prometec.net/wp-content/uploads/2015/04/2222n.jpg" alt="" class="circle">
                                <span style="color:#0D2948; display:block">Transistor NPN 2222N</span>
                            <div>
                        </li>
                        <li class="collection-item avatar center">
                            <div class="item">
                                <img style="width:50%" src="https://www.prometec.net/wp-content/uploads/2018/01/HTB1k4_KRVXXXXb6apXXq6xXFXXX3-520x535.jpg" alt="" class="circle">
                                <span style="color:#0D2948; display:block">USB Logic Analyze 24M 8CH</span>
                            <div>
                        </li>
                    </ul>
                </div>
            </div>
        </div> 
    </div>
    <ul class="pagination center">
        <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
        <li class="active"><a href="#!">1</a></li>
        <li class="waves-effect"><a href="#!">2</a></li>
        <li class="waves-effect"><a href="#!">3</a></li>
        <li class="waves-effect"><a href="#!">4</a></li>
        <li class="waves-effect"><a href="#!">5</a></li>
        <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
    </ul>      
</div>
<!--
  Content Section End
-->
@endsection

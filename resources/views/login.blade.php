@extends("partials.layouts.login_layout")

@section("content")
<!--
  Content Section Start
-->
<main>
    <div class="valign-wrapper" style="width:100%;height:100%;position: absolute;">
        <div class="valign" style="width:100%;">
            <center>   
                <div class="container">
                    <div class="z-depth-1 black row" style="display: inline-block; padding: 32px 48px 0px 48px; background-color:#0D2948">
                        <form class="col s12" class="login-form" id="login_form" method="post" action="{{url('user/login')}}">
                            {{ csrf_field() }}
                            <img class="responsive-img" style="width: 300px;" src="/img/logo.png" />
                            <h4 style="color:#fff">Acceso a Backend</h4>
                            <div class='row'>
                                <div class="input-field col s12">
                                    <input style="color:#fff" id="email" type="email" name="email" class="validate">
                                    <label for="email" class="active">Correo electrónico</label>
                                </div>
                            </div>
                            <div class='row'>
                                <div class="input-field col s12">
                                    <input style="color:#fff" id="password" type="password" name="password" class="validate">
                                    <label for="password" class="active">Contraseña</label>
                                </div>
                                <label style='float: right;'>
                                    <a href='{{url("recuperar")}}'><b>Olvidaste la contraseña?</b></a>
                                </label>
                            </div>
                            <center>
                            <div class='row'>
                                <a id="buttonLogin" style="background-color:#0D2948" class='col s12 btn btn-large waves-effect'>Iniciar Sesión</a>
                            </div>
                            </center>
                        </form>
                    </div>
                </div><!--a href="#!">Create account</a-->
            </center>
        </div>
    </div>
</main>
<!--
  Content Section End
-->
@endsection
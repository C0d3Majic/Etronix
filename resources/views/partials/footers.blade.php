<footer class="page-footer" style="background-color:#0D2948">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">Etronix</h5>
          <p class="grey-text text-lighten-4">Somos una empresa con una gran voluntad de ofrecer los productos tecnológicos que necesita para que hacer del mundo un lugar increíble.</p>


        </div>
        <div class="col l4 offset-l2 s12">
            <h5 class="white-text">Mantente en contacto</h5>
            <ul>
                <li><a style="color:white" href="https://www.facebook.com/E-Tronixs-1528718070756402/" target="_blank"><i class="mdi mdi-facebook-box"></i>E-Tronixs</a></li>
                <li><a style="color:white" href="#!"><i class="mdi mdi-twitter-box"></i></a></li>
                <li><a style="color:white" href="{{url('contact')}}" target="_blank"><i class="mdi mdi-gmail"></i>contact@etronix.com</a></li>
            </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
        <div class="container center">
            <a href="{{url('privacy')}}">Aviso de Privacidad</a> © Etronix Todos los Derechos Reservados 2018 <a href="{{url('conditions')}}">Condiciones de Uso</a>
        </div>
    </div>
</footer>
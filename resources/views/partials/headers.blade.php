<!-- CORE CSS-->
<!--Import Google Icon Font-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="/css/materialdesignicons.min.css" media="all" rel="stylesheet" type="text/css" />
<!-- Include stylesheet -->
<link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
<link href="/css/style.min.css" type="text/css" rel="stylesheet" media="screen,projection">
<link type="text/css" rel="stylesheet" href="/css/materialize.min.css"  media="screen,projection"/>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.15/datatables.min.css"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>Etronixs</title>
<style>
nav a.active{
    background: rgba(255,255,255,0.4);
  }
  #sidenav-overlay{
    z-index: 995
  }
  body {
    display: flex;
    min-height: 100vh;
    flex-direction: column;
  }

  main {
    flex: 1 0 auto;
  }

  .tag {
     float: left;
     position: absolute;
     left: 0px;
     top: 0px;
     z-index: 1000;
     background-color: #92AD40;
     padding: 5px;
     color: #000;
     font-weight: bold;
  }

  .tabs.tabs-transparent .indicator {
      background-color: #07b;
  }

  .img-circle {
      border-radius: 50%;
  }

  .indicators{
    visibility: hidden;
  }

</style>
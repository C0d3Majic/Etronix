<script>
    $(document).ready(function(){
        if(!"{{$void}}")
        {
            paypal.Button.render({
            
            env: 'sandbox', // Or 'sandbox'

            client: {
                sandbox:    'AVnSix5HveBBf6f3fH3Pp2Ym6Z1f6U_X_C3xy8GIXvuPcclriuEdu4Am3PLFueBitAugAzbCStsjdVdJ',
                //production: 'xxxxxxxxx'
            },

            commit: true, // Show a 'Pay Now' button

            payment: function(data, actions) {
                return actions.payment.create({
                    payment: {
                        transactions: [
                            {
                                amount: { total: "{{$total}}", currency: 'USD' }
                            }
                        ]
                    }
                });
            },

            onAuthorize: function(data, actions) {
                return actions.payment.execute().then(function(payment) {

                    // The payment is complete!
                    M.toast({html: 'Venta completada Exitosamente', classes: 'rounded green', displayLength:2000, completeCallback: function(){window.location.href = "{{url('store')}}";}}); 
                });
            },
            onCancel: function(data, actions) {
                M.toast({html: "Cancelaste el pago :'(", classes: 'rounded red', displayLength:2000});
            },
            onError: function(err) {
                M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
            }

            }, '#paypal-button');
        }

        $('#btnCancelar').click( function(){
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('delete/cart')}}",
                // The data to send (will be converted to a query string). variable set above
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // Whether this is a POST or GET request
                type: "POST",
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function( msg ) {
                    if(msg.status == 'error'){
                        var $toastContent = $('<span>'+ msg.type +'</span>');
                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});
                    }else{
                    M.toast({html: 'Carrito Vaciado Exitosamente', classes: 'rounded green', displayLength:2000, completeCallback: function(){window.location.href = "{{url('store')}}";}}); 
                    }
                },
                error: function(){
                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });   
        });

        $('body').on('click', '.deleteItem', function(){
            item_id = $(this).data('item_id');
            var jsonObject = {
                item_id :   item_id
            }
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('delete/cart/item')}}",
                // Whether this is a POST or GET request
                type: "POST",
                data:jsonObject,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function (response) {  
                if(response.status == 'error'){
                    var $toastContent = $('<span>'+ response.type +'</span>');
                    M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});
                }else if(response.status == 'success'){
                    M.toast({html: 'Producto Eliminado Exitosamente', classes: 'rounded green', displayLength:2000, completeCallback: function(){window.location.reload()}}); 
                }
                },
                error: function(){
                //window.location.reload();
                var $toastContent = $('<span>Hubo un error en el servidor</span>');
                M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
        });

        $('body').on('click', '.addItem', function(){
            item_id = $(this).data('item_id');
            var jsonObject = {
                item_id :   item_id
            }
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('add/cart/item')}}",
                // Whether this is a POST or GET request
                type: "POST",
                data:jsonObject,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function (response) {  
                if(response.status == 'error'){
                    var $toastContent = $('<span>'+ response.type +'</span>');
                    M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});
                }else if(response.status == 'success'){
                    M.toast({html: 'Producto Incrementado Exitosamente', classes: 'rounded green', displayLength:1000, completeCallback: function(){window.location.reload()}}); 
                }
                },
                error: function(){
                //window.location.reload();
                var $toastContent = $('<span>Hubo un error en el servidor</span>');
                M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
        });

        $('body').on('click', '.removeItem', function(){
            item_id = $(this).data('item_id');
            var jsonObject = {
                item_id :   item_id
            }
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('min/cart/item')}}",
                // Whether this is a POST or GET request
                type: "POST",
                data:jsonObject,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function (response) {  
                if(response.status == 'error'){
                    var $toastContent = $('<span>'+ response.type +'</span>');
                    M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});
                }else if(response.status == 'success'){
                    M.toast({html: 'Producto Decrementado Exitosamente', classes: 'rounded green', displayLength:1000, completeCallback: function(){window.location.reload()}}); 
                }
                },
                error: function(){
                //window.location.reload();
                var $toastContent = $('<span>Hubo un error en el servidor</span>');
                M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
        });
    });
</script>
<script>
    var loadFile = function(event) {
        var output = document.getElementById('output');
        output.src = URL.createObjectURL(event.target.files[0]);
    };

    $(document).ready(function(){
        $('.modal').modal({dismissible:false});        
        $('#users').DataTable();
        $('.tooltipped').tooltip();
        $('select').formSelect({hover:false});
        var item_id;

         $('body').on('click', '.deleteCategory', function(){
            category_id = $(this).data('category_id');
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('delete/category')}}/"+category_id,
                // Whether this is a POST or GET request
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function (response) {  
                if(response.status == 'error'){
                    var $toastContent = $('<span>'+ response.type +'</span>');
                    M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});
                }else if(response.status == 'success'){
                    $('.modal-content').html(response.data);
                    $('select').formSelect();
                    M.updateTextFields();
                    $('#modal2').modal('open');
                }
                },
                error: function(){
                //window.location.reload();
                var $toastContent = $('<span>Hubo un error en el servidor</span>');
                M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
        });

        $('body').on('click', '.editCategory', function(){
            category_id = $(this).data('category_id');
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('edit/category')}}/"+category_id,
                // Whether this is a POST or GET request
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function (response) {   
                    if(response.status == 'error'){
                        var $toastContent = $('<span>'+ response.type +'</span>');
                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});
                    }else if(response.status == 'success'){
                        $('.modal-content').html(response.data);
                        $('select').formSelect();
                        M.updateTextFields();
                        $('#modal1').modal('open');
                    }
                },
                error: function(){
                //window.location.reload();
                    var $toastContent = $('<span>Hubo un error en el servidor</span>');
                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
        });        

        $('#btnCancelarEditar').click(function(){
            M.toast({html: 'Edicion Cancelada!', classes: 'rounded', displayLength:1000, completeCallback: function(){window.location.reload()}});                    
        });

        $('#btnCancelarEliminar').click(function(){
            M.toast({html: 'Eliminacion Cancelada!', classes: 'rounded', displayLength:1000, completeCallback: function(){window.location.reload()}});                    
        });
        
        
        $('#btnEliminar').click(function(){
            var jsonObject = {
                session_id      : "{{$session_id}}",
                session_token   : "{{$session_token}}",
                category_id        : category_id
            }
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('delete/category')}}",
                // The data to send (will be converted to a query string). variable set above
                data: jsonObject,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // Whether this is a POST or GET request
                type: "POST",
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function( msg ) {
                    if(msg.status == 'error'){
                        var $toastContent = $('<span>'+ msg.type +'</span>');
                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:1000, completeCallback: function(){window.location.reload()}});
                    }else{
                        M.toast({html: 'Categoria Eliminada Exitosamente!', classes: 'rounded green', displayLength:2000, completeCallback: function(){window.location.reload()}}); 
                    }
                },
                error: function(){
                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
        })        

        $("#btnGuardarNuevo").click(function(){
             // Start $.ajax() method
             var jsonObject = {
                session_id      :   "{{$session_id}}",
                session_token   :   "{{$session_token}}",
                name            :   $("#new_name").val()
            };
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('new/category')}}",
                // The data to send (will be converted to a query string). variable set above
                data: jsonObject,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // Whether this is a POST or GET request
                type: "POST",
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function( msg ) {
                    if(msg.status == 'error'){
                        var $toastContent = $('<span>'+ msg.type +'</span>');
                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});
                    }else{
                        M.toast({html: 'Categoria Agregada Exitosamente!', classes: 'rounded green', displayLength:2000, completeCallback: function(){window.location.reload()}}); 
                    }
                },
                error: function(){
                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
        });

        $("#btnGuardar").click(function(){
            // Start $.ajax() method
            var jsonObject = {
                session_id      :   "{{$session_id}}",
                session_token   :   "{{$session_token}}",
                name            :   $("#name").val(),
                category_id        :   category_id
            }
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('save/category')}}",
                // The data to send (will be converted to a query string). variable set above
                data: jsonObject,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // Whether this is a POST or GET request
                type: "POST",
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function( msg ) {
                    if(msg.status == 'error'){
                        var $toastContent = $('<span>'+ msg.type +'</span>');
                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:1000, completeCallback: function(){window.location.reload()}});
                    }else{
                        M.toast({html: 'Categoria Editada Exitosamente!', classes: 'rounded green', displayLength:2000, completeCallback: function(){window.location.reload()}}); 
                    }
                },
                error: function(){
                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
        })
    });
</script>
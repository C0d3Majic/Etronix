<script>
    var loadFile = function(event) {
        var output = document.getElementById('output');
        output.src = URL.createObjectURL(event.target.files[0]);
    };

    $(document).ready(function(){
        document.addEventListener('DOMContentLoaded', function() {
            var elems = document.querySelectorAll('.materialboxed');
            var instances = M.Materialbox.init(elems, {});
        });
        $('.modal').modal({dismissible:false});        
        $('#users').DataTable();
        $('.tooltipped').tooltip();
        $('select').formSelect({hover:false});
        var item_id;

         $('body').on('click', '.deleteItem', function(){
            item_id = $(this).data('item_id');
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('delete/item')}}/"+item_id,
                // Whether this is a POST or GET request
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function (response) {  
                if(response.status == 'error'){
                    var $toastContent = $('<span>'+ response.type +'</span>');
                    M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});
                }else if(response.status == 'success'){
                    $('.modal-content').html(response.data);
                    $('select').formSelect();
                    M.updateTextFields();
                    $('#modal2').modal('open');
                }
                },
                error: function(){
                //window.location.reload();
                var $toastContent = $('<span>Hubo un error en el servidor</span>');
                M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
        });

        $('body').on('click', '.editItem', function(){
            item_id = $(this).data('item_id');
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('edit/item')}}/"+item_id,
                // Whether this is a POST or GET request
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function (response) {   
                    if(response.status == 'error'){
                        var $toastContent = $('<span>'+ response.type +'</span>');
                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});
                    }else if(response.status == 'success'){
                        $('.modal-content').html(response.data);
                        $('select').formSelect();
                        M.updateTextFields();
                        $('#modal1').modal('open');
                    }
                },
                error: function(){
                //window.location.reload();
                    var $toastContent = $('<span>Hubo un error en el servidor</span>');
                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
        });        

        $('body').on('click', '.imageItem', function(){
            item_id = $(this).data('item_id');
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('edit/item/image')}}/"+item_id,
                // Whether this is a POST or GET request
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function (response) {   
                    if(response.status == 'error'){
                        var $toastContent = $('<span>'+ response.type +'</span>');
                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});
                    }else if(response.status == 'success'){
                        $('.modal-content').html(response.data);
                        $('.modal-footer').html(response.footer);
                        $('select').formSelect();
                        M.updateTextFields();
                        $('#modal1').modal('open');

                        $('#btnCancelarCargar').click(function(){
                            M.toast({html: 'Actualizacion de Imagen Cancelada!', classes: 'rounded', displayLength:1000, completeCallback: function(){window.location.reload()}});                    
                        })

                        /*$('#btnCargar').click(function(){
                            var f = $("form_upload");
                            var formData = new FormData(document.getElementById("form_upload"));
                            console.log(formData)
                            formData.append(f.attr("name"), f[0].files[0]);
                           
                            $.ajax({
                                url: $("#form_upload").attr('action'),
                                // The data to send (will be converted to a query string). variable set above
                                data: formData,
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                contentType: false,
                                processData: false,
                                // Whether this is a POST or GET request
                                type: "POST",
                                // The type of data we expect back. can be json, html, text, etc...
                                dataType : "json",
                                // Code to run if the request succeeds;
                                // the response is passed to the function
                                success: function( msg ) {
                                    if(msg.status == 'error'){
                                        var $toastContent = $('<span>'+ msg.type +'</span>');
                                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:1000, completeCallback: function(){window.location.reload()}});
                                    }else{
                                        M.toast({html: 'Imagen Cargada Exitosamente!', classes: 'rounded green', displayLength:2000, completeCallback: function(){window.location.reload()}}); 
                                    }
                                },
                                error: function(){
                                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                                }
                            });
                        })*/
                        // Variable to store your files
                        var files;

                        // Add events
                        $('input[type=file]').on('change', prepareUpload);

                        // Grab the files and set them to our variable
                        function prepareUpload(event)
                        {
                            files = event.target.files;
                        }
                        //$('form').on('submit', uploadFiles);
                        $("#btnCargar").click(uploadFiles)
                        // Catch the form submit and upload the files
                        function uploadFiles(event)
                        {
                            event.stopPropagation(); // Stop stuff happening
                            event.preventDefault(); // Totally stop stuff happening

                            // START A LOADING SPINNER HERE

                            // Create a formdata object and add the files
                            var data = new FormData();
                            $.each(files, function(key, value)
                            {
                                data.append(key, value);
                            });
                            data.append('item_id',$('#item_id').val());
                            $.ajax({
                                url: '{{url("save/item/image")}}',
                                type: 'POST',
                                data: data,
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                cache: false,
                                dataType: 'json',
                                processData: false, // Don't process the files
                                contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                                success: function(data, textStatus, jqXHR)
                                {
                                    console.log(data)                                    
                                    if(data.status == 'success')
                                        M.toast({html: 'Imagen Cargada Exitosamente!', classes: 'rounded green', displayLength:2000, completeCallback: function(){window.location.reload()}});  
                                    else
                                    {
                                        var $toastContent = $('<span>'+ data.type +'</span>');
                                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:1000, completeCallback: function(){window.location.reload()}});
                                    }
                                },
                                error: function(jqXHR, textStatus, errorThrown)
                                {
                                    // Handle errors here
                                    console.log('ERRORS: ' + textStatus);
                                    // STOP LOADING SPINNER
                                }
                            });
                        }     
                    }
                },
                error: function(){
                //window.location.reload();
                    var $toastContent = $('<span>Hubo un error en el servidor</span>');
                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
        });

        $('#btnCancelarEditar').click(function(){
            M.toast({html: 'Edicion Cancelada!', classes: 'rounded', displayLength:1000, completeCallback: function(){window.location.reload()}});                    
        });

        $('#btnCancelarEliminar').click(function(){
            M.toast({html: 'Eliminacion Cancelada!', classes: 'rounded', displayLength:1000, completeCallback: function(){window.location.reload()}});                    
        });
        
        
        $('#btnEliminar').click(function(){
            var jsonObject = {
                session_id: "{{$session_id}}",
                session_token : "{{$session_token}}",
                item_id : item_id
            }
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('delete/item')}}",
                // The data to send (will be converted to a query string). variable set above
                data: jsonObject,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // Whether this is a POST or GET request
                type: "POST",
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function( msg ) {
                    if(msg.status == 'error'){
                        var $toastContent = $('<span>'+ msg.type +'</span>');
                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:1000, completeCallback: function(){window.location.reload()}});
                    }else{
                        M.toast({html: 'Usuario Eliminado Exitosamente!', classes: 'rounded green', displayLength:2000, completeCallback: function(){window.location.reload()}}); 
                    }
                },
                error: function(){
                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
        })        

        $("#btnGuardarNuevo").click(function(){
             // Start $.ajax() method
             var jsonObject = {
                session_id      :   "{{$session_id}}",
                session_token   :   "{{$session_token}}",
                name            :   $("#new_name").val(),
                part_number     :   $("#new_number").val(),
                provider        :   $("#new_provider").val(),
                brand           :   $("#new_brand").val(),
                category        :   $("#new_category").val(),
                price           :   $("#new_price").val(),
                stock           :   $("#new_stock").val(),
                active          :   $("#new_active").val(),
                description     :   $("#new_description").val()
            };
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('new/item')}}",
                // The data to send (will be converted to a query string). variable set above
                data: jsonObject,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // Whether this is a POST or GET request
                type: "POST",
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function( msg ) {
                    if(msg.status == 'error'){
                        var $toastContent = $('<span>'+ msg.type +'</span>');
                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});
                    }else{
                        M.toast({html: 'Articulo Agregado Exitosamente!', classes: 'rounded green', displayLength:2000, completeCallback: function(){window.location.reload()}}); 
                    }
                },
                error: function(){
                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
        });

        $("#btnGuardar").click(function(){
            // Start $.ajax() method
            var jsonObject = {
                session_id      :   "{{$session_id}}",
                session_token   :   "{{$session_token}}",
                name            :   $("#name").val(),
                part_number     :   $("#number").val(),
                provider        :   $("#provider").val(),
                brand           :   $("#brand").val(),
                category        :   $("#category").val(),
                price           :   $("#price").val(),
                stock           :   $("#stock").val(),
                active          :   $("#active").val(),
                description     :   $("#description").val(),
                item_id         :   item_id
            }
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('save/item')}}",
                // The data to send (will be converted to a query string). variable set above
                data: jsonObject,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // Whether this is a POST or GET request
                type: "POST",
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function( msg ) {
                    if(msg.status == 'error'){
                        var $toastContent = $('<span>'+ msg.type +'</span>');
                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:1000, completeCallback: function(){window.location.reload()}});
                    }else{
                        M.toast({html: 'Articulo Editado Exitosamente!', classes: 'rounded green', displayLength:2000, completeCallback: function(){window.location.reload()}}); 
                    }
                },
                error: function(){
                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
        })
    });
</script>
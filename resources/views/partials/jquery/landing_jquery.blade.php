<script>
$(document).ready(function(){
    $('.tooltipped').tooltip();
    $('.sidenav').sidenav();
    $('.materialboxed').materialbox();
    $('.modal').modal();
    $('.tabs').tabs();
    $("#btnIniciaSesion").click(function(){
        $('#body').removeClass('loaded');
        setTimeout(function(){
            $('#body').addClass('loaded');          
        },2000);
    });

    $("#btnIniciaSesion2").click(function(){
        $('#body').removeClass('loaded');
        setTimeout(function(){
            $('#body').addClass('loaded');          
        },2000);
    });
   

    $("#buttonLogin").click(function(){
            // Start $.ajax() method
            $.ajax({
            // The URL for the request. variable set above
            url: $("#login_form").attr('action'),
            // The data to send (will be converted to a query string). variable set above
            data: $("#login_form").serialize(),
            // Whether this is a POST or GET request
            type: "POST",
            // The type of data we expect back. can be json, html, text, etc...
            dataType : "json",
            // Code to run if the request succeeds;
            // the response is passed to the function
            success: function( msg ) {
                if(msg.status == 'error'){
                    var $toastContent = $('<span>'+ msg.type +'</span>');
                    M.toast({html:$toastContent, classes: 'rounded red', displayLength:2000});
                    Materialize.toast($toastContent, 2000, 'rounded red darken-3');
                }else if(msg.status == 'success'){
                    M.toast({html: 'Bienvenido!', classes: 'rounded', displayLength:2000, completeCallback: function(){window.location.reload()}});                    
                }
            },
            error: function(){
                M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
            }
        });
        return false;
    })
});
</script>
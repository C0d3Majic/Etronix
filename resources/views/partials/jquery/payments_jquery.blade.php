<script>
      $( document ).ready(function(){
        $('#payments').DataTable({
            responsive: true,
            "order": [[1, "desc" ]]
        }); //Datatable instantiation
        var table = $('#payments').DataTable();
        $('.modal').modal({dismissible:false});      
        $('select').formSelect({hover:false});
        
         $('#search').click(function(){
           var numero_transaccion = $("#numero_transaccion").val();
           //Change the date string to mysql date
           var jsonObject = {
               id : numero_transaccion,
               session_id   :   "{{$session_id}}",
               session_token:   "{{$session_token}}"
           }
           $.ajax({
             // The URL for the request. variable set above
             url: "{{url('payment/search')}}",
             // The data to send (will be converted to a query string). variable set above
             data: jsonObject,
             headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
             // Whether this is a POST or GET request
             type: "POST",
             // The type of data we expect back. can be json, html, text, etc...
             dataType : "json",
             // Code to run if the request succeeds;
             // the response is passed to the function
             success: function( msg ) {
               if(msg.status == 'error'){
                  var $toastContent = $('<span>'+ msg.type +'</span>');
                  M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});
               }else if(msg.status == 'success'){
                 var contObjetos = 0;
                 console.log(msg.payment);
                 //------------------------------------- CODE TO UPDATE THE DATATABLE ------------
                 table.rows().remove(); //We delete the all the rows to update the datatable
                 table.row.add([msg.payment.id,msg.payment.reason,msg.payment.created_at,msg.payment.service_key,msg.payment.amount,msg.payment.status == 1 ? '<span class="new badge blue" data-badge-caption="Verificado">' : '<span style="text-align:left"class="new badge brown" data-badge-caption="Pendiente"></span>' ]);

                 table.draw();
               }
             },
             error: function(){
               M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
             }
           });
         });

         $('#btnEnviarNuevo').click(function(){
           // Start $.ajax() method
           var jsonObject = {
               description  :  $("#description").val(),
               session_id   :   "{{$session_id}}",
               session_token:   "{{$session_token}}"
           }
           $.ajax({
             // The URL for the request. variable set above
             url: "{{url('new/ticket')}}",
             headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
             // The data to send (will be converted to a query string). variable set above
             data: jsonObject,
             // Whether this is a POST or GET request
             type: "POST",
             // The type of data we expect back. can be json, html, text, etc...
             dataType : "json",
             // Code to run if the request succeeds;
             // the response is passed to the function
             success: function( msg ) {
               if(msg.status == 'error'){
                  var $toastContent = $('<span>'+ msg.type +'</span>');
                  M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});
               }else if(msg.status == 'success'){
                M.toast({html: 'Ticket Creado Exitosamente!', classes: 'rounded green', displayLength:2000, completeCallback: function(){window.location.reload()}}); 
               }
             },
             error: function(){
               //window.location.reload();
               var $toastContent = $('<span>Hubo un error en el servidor</span>');
               M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
             }
           });
         });

         
      });
</script>

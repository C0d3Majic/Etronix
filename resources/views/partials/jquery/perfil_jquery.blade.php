<script type="text/javascript">
$(document).ready(function(){
    $('#saveBtn').click(function(){
        // Start $.ajax() method
        var name = $("#name").val();
        var email = $("#email").val();
        var password = $("#password").val();
        var jsonObject = {
            name : name,
            email : email,
            password : password,
            session_id: "{{$session_id}}",
            session_token : "{{$session_token}}"

        }
        $.ajax({
            // The URL for the request. variable set above
            url: "{{url('save/user/profile')}}",
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            // The data to send (will be converted to a query string). variable set above
            data: jsonObject,
            // Whether this is a POST or GET request
            type: "POST",
            // The type of data we expect back. can be json, html, text, etc...
            dataType : "json",
            // Code to run if the request succeeds;
            // the response is passed to the function
            success: function( msg ) {
            if(msg.status == 'error'){                
                var $toastContent = $('<span>'+ msg.type +'</span>');
                M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});
            }else if(msg.status == 'success'){
                M.toast({html: 'Perfil Actualizado Exitosamente!', classes: 'rounded green', displayLength:2000, completeCallback: function(){window.location.reload()}}); 
                }
            },
            error: function(){
            //window.location.reload();
                M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
            }
        });
    });
});
</script>
<script>
    $(document).ready(function(){
        $('.modal').modal({dismissible:false});        
        $('#reports').DataTable();
        $('.datepicker').datepicker();
        
        var ctx = document.getElementById("myChart").getContext('2d');
        // For a bubble chart
        var myBubbleChart = new Chart(ctx,{
            type: 'line',
            // The data for our dataset
            data: {
                labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio"],
                datasets: [{
                    label: "Ventas por Mes",
                    backgroundColor: 'rgba(255, 99, 132,0.5)',
                    borderColor: 'rgb(255, 99, 132)',
                    data: [0, 10, 5, 2, 20, 30, 45],
                }]
            }
        });
    });
</script>
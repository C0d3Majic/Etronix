<script>
      $( document ).ready(function(){
        //$('.modal').modal({dismissible:false});  
        var service_id;
        $('body').on('click', '.request3d', function(){
            service_id =   $(this).data('service_id');
            var string = 'Solicitar un servicio de <b> impresión 3D </b>';
            $("#title").empty();
            $("#title").append(string);
            $('#modal1').modal('open');            
        });    

        $('body').on('click', '.requestCircuit', function(){
            service_id =   $(this).data('service_id');
            var string = 'Solicitar un servicio de <b> creación de tablilla </b>';
            $("#title").empty();
            $("#title").append(string);
            $('#modal1').modal('open');            
        });        

        // Variable to store your files
        var files;

        // Add events
        $('input[type=file]').on('change', prepareUpload);

        // Grab the files and set them to our variable
        function prepareUpload(event)
        {
            files = event.target.files;
        }
        //$('form').on('submit', uploadFiles);
        $("#btnEnviarNuevo").click(uploadFiles)
        // Catch the form submit and upload the files
        function uploadFiles(event)
        {
            event.stopPropagation(); // Stop stuff happening
            event.preventDefault(); // Totally stop stuff happening

            // START A LOADING SPINNER HERE

            // Create a formdata object and add the files
            var data = new FormData();
            $.each(files, function(key, value)
            {
                data.append(key, value);
            });
            data.append('description',$('#description').val());
            data.append('quantity',$('#quantity').val());
            $.ajax({
                url: '{{url("request/new/service")}}',
                type: 'POST',
                data: data,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                cache: false,
                dataType: 'json',
                processData: false, // Don't process the files
                contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                success: function(data, textStatus, jqXHR)
                {
                    console.log(data)                                    
                    if(data.status == 'success')
                        M.toast({html: 'Solicitud Procesada Exitosamente!', classes: 'rounded green', displayLength:2000, completeCallback: function(){window.location.reload()}});  
                    else
                    {
                        var $toastContent = $('<span>'+ data.type +'</span>');
                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:1000, completeCallback: function(){}});
                    }
                },
                error: function(jqXHR, textStatus, errorThrown)
                {
                    // Handle errors here
                    console.log('ERRORS: ' + textStatus);
                    // STOP LOADING SPINNER
                }
            });
        }            
      });
</script>

<script>
    $(document).ready(function(){
        $('.modal').modal({dismissible:false});        
        $('#users').DataTable();
        $('.tooltipped').tooltip();
        var name = "";
        var service_id;

        $('body').on('click', '.answerService', function(){
            email       =   $(this).data('email');
            name        =   $(this).data('name');
            description =   $(this).data('description');
            service_id =   $(this).data('service_id');
            var string = 'Contestar correo de cotizacion al usuario ' + name;
            $('.modal-content').html('<h5 id="title" class="center">'+string+ '</h5>'+
                                        '<div class="row">'+
                                            '<h6 class="center">'+description+'</h6>'+
                                        '</div>'+
                                        '<div class="row">'+
                                            '<div class="input-field col s12 m6">'+
                                                '<input id="email" type="email" class="validate" readonly value="'+email+'">'+
                                                '<label for="email">Correo Electrónico</label>'+
                                            '</div>'+
                                            '<div class="input-field col s12 m6">'+
                                                '<textarea id="email_content" class="materialize-textarea"></textarea>'+
                                                '<label for="email_content">Contenido del Correo</label>'+
                                            '</div>'+
                                        '</div>');
            M.updateTextFields();
            $('#modal1').modal('open');            
        });        
        
        $("#btnEnviar").click(function(){
            jsonObject ={
                name        :   name,
                email       :   $("#email").val(),
                content     :   $("#email_content").val(),
                service_id  : service_id
            }
            console.log(jsonObject)
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('send/service/answer')}}",
                data: jsonObject,
                // Whether this is a POST or GET request
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function (response) {  
                    if(response.status == 'error'){
                        var $toastContent = $('<span>'+ response.type +'</span>');
                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});
                    }else if(response.status == 'success'){
                        M.toast({html: 'Respuesta Enviada Satisfactoriamente', classes: 'green rounded', displayLength:2000,completeCallback: function(){window.location.reload()}});
                    }
                },
                error: function(){
                    //window.location.reload();
                    var $toastContent = $('<span>Hubo un error en el servidor</span>');
                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
        });

        $('body').on('click', '.cancelService', function(){
            service_id =   $(this).data('service_id');
            description =   $(this).data('description');
            var string = 'Cancelar el servicio <b>' + description + '</b>';
            $("#title").empty();
            $("#title").append(string);
            $('#modal3').modal('open');            
        });


        $("#btnEnviarCancelacion").click(function(){
            jsonObject ={
                content     :   $("#cancel_content").val(),
                service_id  :   service_id
            }
            console.log(jsonObject)
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('send/service/cancel')}}",
                data: jsonObject,
                // Whether this is a POST or GET request
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function (response) {  
                    if(response.status == 'error'){
                        var $toastContent = $('<span>'+ response.type +'</span>');
                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});
                    }else if(response.status == 'success'){
                        M.toast({html: 'Cancelacion Enviada Satisfactoriamente', classes: 'green rounded', displayLength:2000,completeCallback: function(){window.location.reload()}});
                    }
                },
                error: function(){
                    //window.location.reload();
                    var $toastContent = $('<span>Hubo un error en el servidor</span>');
                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
        })
        
    });
</script>
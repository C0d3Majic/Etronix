<script>
    $(document).ready(function(){
        /*var array_of_ids = ;
        var arrayLength = array_of_ids.length;
        
        for(let i = 0; i < arrayLength; i++) {
            $('#btnAddToCart' + array_of_ids[i]).click( function(){
                
                $.ajax({
                        // The URL for the request. variable set above
                        url: "{{url('new/cart/item')}}",
                        // The data to send (will be converted to a query string). variable set above
                        data: jsonObject,
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        // Whether this is a POST or GET request
                        type: "POST",
                        // The type of data we expect back. can be json, html, text, etc...
                        dataType : "json",
                        // Code to run if the request succeeds;
                        // the response is passed to the function
                        success: function( msg ) {
                            if(msg.status == 'error'){
                                var $toastContent = $('<span>'+ msg.type +'</span>');
                                M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});
                            }else{
                            M.toast({html: 'Producto Agregado al Carrito'+array_of_ids[i]+ ' !', classes: 'rounded green', displayLength:2000, completeCallback: function(){}}); 
                            }
                        },
                        error: function(){
                            M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                        }
                });        
            });
        }*/
        $(".form-item").submit(function(e){ //user clicks form submit button
            var form_data = $(this).serialize(); //prepare form data for Ajax post
            $.ajax({ //make ajax request to cart_process.php
                url: "{{url('new/cart/item')}}",
                type: "POST",
                headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                dataType:"json", //expect json value from server
                data: form_data
            }).done(function(data){ //on Ajax success
                M.toast({html: 'Producto Agregado al Carrito!', classes: 'rounded green', displayLength:1000, completeCallback: function(){/*window.location.reload()*/}});                 
            })
            e.preventDefault();
        });
    });
</script>
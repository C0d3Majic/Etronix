<script>
$(document).ready(function(){
    var quill = new Quill('#editor', {
        placeholder: 'Compose an epic comment...',
        theme: 'snow'
    });
});  
</script>
<script>
    $(document).ready(function(){
        $('.modal').modal({dismissible:false});        
        $('#users').DataTable();
        $('.tooltipped').tooltip();
        $('select').formSelect({hover:false});
        var user_id;

         $('body').on('click', '.deleteUser', function(){
            user_id = $(this).data('user_id');
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('delete/user')}}/"+user_id,
                // Whether this is a POST or GET request
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function (response) {  
                if(response.status == 'error'){
                    var $toastContent = $('<span>'+ response.type +'</span>');
                    M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});
                }else if(response.status == 'success'){
                    $('.modal-content').html(response.data);
                    $('select').formSelect();
                    M.updateTextFields();
                    $('#modal2').modal('open');
                }
                },
                error: function(){
                //window.location.reload();
                var $toastContent = $('<span>Hubo un error en el servidor</span>');
                M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
        });

        $('body').on('click', '.editUser', function(){
            user_id = $(this).data('user_id');
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('edit/user')}}/"+user_id,
                // Whether this is a POST or GET request
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function (response) {   
                    if(response.status == 'error'){
                        var $toastContent = $('<span>'+ response.type +'</span>');
                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});
                    }else if(response.status == 'success'){
                        $('.modal-content').html(response.data);
                        $('select').formSelect();
                        M.updateTextFields();
                        $('#modal1').modal('open');
                    }
                },
                error: function(){
                //window.location.reload();
                    var $toastContent = $('<span>Hubo un error en el servidor</span>');
                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
        });

        $('#btnCancelarEditar').click(function(){
            M.toast({html: 'Edicion Cancelada!', classes: 'rounded', displayLength:1000, completeCallback: function(){window.location.reload()}});                    
        });

        $('#btnCancelarEliminar').click(function(){
            M.toast({html: 'Eliminacion Cancelada!', classes: 'rounded', displayLength:1000, completeCallback: function(){window.location.reload()}});                    
        });

        $('#btnEliminar').click(function(){
            var jsonObject = {
                session_id: "{{$session_id}}",
                session_token : "{{$session_token}}",
                user_id : user_id
            }
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('delete/user')}}",
                // The data to send (will be converted to a query string). variable set above
                data: jsonObject,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // Whether this is a POST or GET request
                type: "POST",
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function( msg ) {
                    if(msg.status == 'error'){
                        var $toastContent = $('<span>'+ msg.type +'</span>');
                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:1000, completeCallback: function(){window.location.reload()}});
                    }else{
                        M.toast({html: 'Usuario Eliminado Exitosamente!', classes: 'rounded green', displayLength:2000, completeCallback: function(){window.location.reload()}}); 
                    }
                },
                error: function(){
                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
        })        

        $("#btnGuardarNuevo").click(function(){
             // Start $.ajax() method
             var jsonObject = {
                session_id      :   "{{$session_id}}",
                session_token   :   "{{$session_token}}",
                name            :   $("#new_name").val(),
                email           :   $("#new_email").val(),
                password        :   $("#new_password").val(),
                type            :   $("#new_type").val()
            }
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('new/user')}}",
                // The data to send (will be converted to a query string). variable set above
                data: jsonObject,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // Whether this is a POST or GET request
                type: "POST",
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function( msg ) {
                    if(msg.status == 'error'){
                        var $toastContent = $('<span>'+ msg.type +'</span>');
                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:2000});
                    }else{
                        M.toast({html: 'Usuario Agregado Exitosamente!', classes: 'rounded green', displayLength:2000, completeCallback: function(){window.location.reload()}}); 
                    }
                },
                error: function(){
                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
        });

        $("#btnGuardar").click(function(){
            // Start $.ajax() method
            var jsonObject = {
                session_id      :   "{{$session_id}}",
                session_token   :   "{{$session_token}}",
                name            :   $("#name").val(),
                email           :   $("#email").val(),
                password        :   $("#password").val(),
                type            :   $("#type").val(),
                user_id         :   user_id
            }
            $.ajax({
                // The URL for the request. variable set above
                url: "{{url('save/user')}}",
                // The data to send (will be converted to a query string). variable set above
                data: jsonObject,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // Whether this is a POST or GET request
                type: "POST",
                // The type of data we expect back. can be json, html, text, etc...
                dataType : "json",
                // Code to run if the request succeeds;
                // the response is passed to the function
                success: function( msg ) {
                    if(msg.status == 'error'){
                        var $toastContent = $('<span>'+ msg.type +'</span>');
                        M.toast({html: $toastContent, classes: 'rounded red', displayLength:1000, completeCallback: function(){window.location.reload()}});
                    }else{
                        M.toast({html: 'Usuario Editado Exitosamente!', classes: 'rounded green', displayLength:2000, completeCallback: function(){window.location.reload()}}); 
                    }
                },
                error: function(){
                    M.toast({html: 'Oops algo a sucedido en el servidor!', classes: 'rounded red', displayLength:2000});
                }
            });
        })
    });
</script>
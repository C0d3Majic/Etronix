<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
      @include("partials.cart_headers")
      @include("partials.styles.general_styles")
    </head>
    <body>
      @include("partials.menu")
      @yield("content")

      @include("partials.footers")

      @include("partials.scripts.general_scripts")
      @include("partials.jquery.landing_jquery")
      @include("partials.jquery.cart_jquery")
    </body>
</html>
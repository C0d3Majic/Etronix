<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
      @include("partials.headers")
      @include("partials.styles.general_styles")
    </head>
    <body>
      @include("partials.profile_menu")
      @yield("content")

      @include("partials.footers")

      @include("partials.scripts.general_scripts")
      @include("partials.jquery.session_jquery")
      @include("partials.jquery.payments_jquery")
    </body>
</html>
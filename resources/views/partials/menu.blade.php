<div class="navbar-fixed">
    <nav style="background-color:#000">
        <div class="nav-wrapper">
            <a href="/" class="brand-logo"><img class="responsive-img" src="/img/logo.png" style="height:64px; width:auto"/></a>
            <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            <ul class="right hide-on-med-and-down">
                <li><a href="/" style="color:white" class="tooltipped" data-position="bottom" data-tooltip="Inicio">Inicio</a></li>
                <li><a href="{{url('requests')}}"style="color:white" class="tooltipped" data-position="bottom" data-tooltip="Ir a Servicios!">Servicios</a></li>
                <li><a href="{{url('store')}}"style="color:white" class="tooltipped" data-position="bottom" data-tooltip="Ir a Tienda!">Tienda</a></li>
                <li><a href="{{url('cart')}}" style="color:white" class="tooltipped" data-position="bottom" data-tooltip="Revisa tu carrito">Carrito</a></li>
                <li><a href="{{url('about')}}" style="color:white">Acerca de</a></li>
                <li><a href="{{url('contact')}}" style="color:white">Contacto</a></li>
                <li><a id="btnIniciaSesion" style="color:white" class="tooltipped modal-trigger"  href="#modal9" data-position="bottom" data-tooltip="Inicia Sesion!"><i class="material-icons">people</i></a></li>
                @if(Auth::check())<li><a href="{{url('profile')}}" style="color:white">Mi cuenta</a></li>@endif
            </ul>
        </div>
    </nav>
</div>

<ul class="sidenav" id="mobile-demo">
    <li><a href="/" style="color:black" class="tooltipped" data-position="bottom" data-tooltip="Inicio">Inicio</a></li>
    <li><a href="{{url('requests')}}"style="color:black" class="tooltipped" data-position="bottom" data-tooltip="Ir a Servicios!">Servicios</a></li>
    <li><a href="{{url('store')}}"style="color:black" class="tooltipped" data-position="bottom" data-tooltip="Ir a Tienda!">Tienda</a></li>
    <li><a href="{{url('cart')}}" style="color:black" class="tooltipped" data-position="bottom" data-tooltip="Revisa tu carrito">Carrito</a></li>
    <li><a href="{{url('about')}}" style="color:black">Acerca de</a></li>
    <li><a href="{{url('contact')}}" style="color:black">Contacto</a></li>
    <li><a id="btnIniciaSesion2" style="color:black" class="tooltipped modal-trigger"  href="#modal9" data-position="bottom" data-tooltip="Inicia Sesion!"><i class="material-icons" style="color:black">people</i>Inicia Sesión</a></li>
    @if(Auth::check())<li><a href="{{url('profile')}}" style="color:black">Mi cuenta</a></li>@endif
</ul>

<!-- Modal Structure -->
<div id="modal9" class="modal" style="background-color:#000">
    <div class="row">       
        <div class="col s12">
            <ul class="tabs" id="tabs">
                <li class="tab col s6" style="background-color:#000"><a href="#login" style="color:rgb(79, 163, 155);">Iniciar Sesión</a></li>
                <li class="tab col s6" style="background-color:#000"><a href="#signup" style="color:rgb(79, 163, 155);">Registrarme</a></li>
            </ul>
        </div>
        <div id="login" class="col s12" style="background-color:#000">
            <div class="modal-content center" id="body"style="background-color:#0D2948">
                <div id="loader-wrapper">
                    <div id="loader"></div>        
                    <div class="loader-section section-left"></div>
                    <div class="loader-section section-right"></div>
                </div>   
                <div id="login-page" class="z-depth-1 black row" style="display: inline-block; padding: 12px 24px 0px 24px; background-color:#0D2948">
                    <form class="col s12" class="login-form" id="login_form" method="post" action="{{url('user/login')}}">
                        {{ csrf_field() }}
                        <img class="responsive-img" style="width: 300px;" src="/img/logo.png" />
                        <div class='row'>
                            <div class="input-field col s12">
                                <input style="color:#fff" id="email" type="email" name="email" class="validate">
                                <label for="email" class="active">Correo electrónico</label>
                            </div>
                        </div>
                        <div class='row'>
                            <div class="input-field col s12">
                                <input style="color:#fff" id="password" type="password" name="password" class="validate">
                                <label for="password" class="active">Contraseña</label>
                            </div>
                        </div>
                        <center>
                        <div class='row'>
                            <a id="buttonLogin" style="background-color:white; color:black" class='col s12 btn btn-large waves-effect'>Iniciar Sesión</a>
                        </div>
                        </center>
                    </form>
                </div>           
            </div>
        </div>
        <div id="signup" class="col s12">
            <div class="modal-content center" style="background-color:#0D2948">
                <div id="login-page" class="z-depth-1 black row" style="display: inline-block; padding: 12px 24px 0px 24px; background-color:#0D2948">
                    <form class="col s12" class="login-form" id="register_form" method="post" action="{{url('user/register')}}">
                        {{ csrf_field() }}
                        <img class="responsive-img" style="width: 300px;" src="/img/logo.png" />
                        <div class='row'>
                            <div class="input-field col s12">
                                <input style="color:#fff" id="new_name" type="text" name="new_name" class="validate">
                                <label for="new_name" class="active">Nombre Completo</label>
                            </div>
                        </div>
                        <div class='row'>
                            <div class="input-field col s12">
                                <input style="color:#fff" id="new_email" type="email" name="new_email" class="validate">
                                <label for="new_email" class="active">Correo electrónico</label>
                            </div>
                        </div>
                        <div class='row'>
                            <div class="input-field col s12">
                                <input style="color:#fff" id="new_password" type="password" name="new_password" class="validate">
                                <label for="new_password" class="active">Contraseña</label>
                            </div>                           
                        </div>
                        <center>
                        <div class='row'>
                            <a id="buttonRegister" style="background-color:white; color:black" class='col s12 btn btn-large waves-effect'>Registrame</a>
                        </div>
                        
                        </center>
                    </form>
                </div>           
            </div>
        </div>
    </div>
    <!--div class="modal-content center" id="body"style="background-color:#0D2948"q>
        <div id="loader-wrapper">
            <div id="loader"></div>        
            <div class="loader-section section-left"></div>
            <div class="loader-section section-right"></div>
        </div>   
        <div id="login-page" class="z-depth-1 black row" style="display: inline-block; padding: 12px 24px 0px 24px; background-color:#0D2948">
            <form class="col s12" class="login-form" id="login_form" method="post" action="{{url('user/login')}}">
                {{ csrf_field() }}
                <img class="responsive-img" style="width: 300px;" src="/img/logo.png" />
                <div class='row'>
                    <div class="input-field col s12">
                        <input style="color:#fff" id="email" type="email" name="email" class="validate">
                        <label for="email" class="active">Correo electrónico</label>
                    </div>
                </div>
                <div class='row'>
                    <div class="input-field col s12">
                        <input style="color:#fff" id="password" type="password" name="password" class="validate">
                        <label for="password" class="active">Contraseña</label>
                    </div>
                </div>
                <center>
                <div class='row'>
                    <a id="buttonLogin" style="background-color:white; color:black" class='col s12 btn btn-large waves-effect'>Iniciar Sesión</a>
                </div>
                </center>
            </form>
        </div>           
    </div-->
</div>


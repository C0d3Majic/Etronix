<ul id="dropdown1" class="dropdown-content">
    <li><a id="btnLogout">Cerrar Sesion</a></li>
</ul>

<nav style="background-color:#000;" class="navbar-fixed">
    <div class="nav-wrapper">
        <a href="{{url('/')}}" class="brand-logo center"><img class="responsive-img" src="/img/logo.png" style="width:auto; height:64px;"></a>
        <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a style="color:#FFF" href="{{url('profile')}}">Mi Perfil</a></li>
            <li><a style="color:#FFF" href="{{url('payments')}}">Mis Compras</a></li>
            <li><a class="dropdown-trigger" href="#!" data-target="dropdown1" style="color:white;">{{$username}}<i class="material-icons right">arrow_drop_down</i></a></li>
        </ul>
    </div>
</nav>
<ul class="sidenav" id="mobile-demo">
    <li><a style="color:black" href="{{url('profile')}}">Mi Perfil</a></li>
    <li><a style="color:black" href="{{url('payments')}}">Mis Compras</a></li>
    <li><a style="color:black" id="btnLogout2">Cerrar Sesión</a></li>
</ul>

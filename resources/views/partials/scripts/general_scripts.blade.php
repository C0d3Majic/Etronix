<!-- ================================================
  Scripts
  ================================================ -->
<!-- jQuery Library-->
<!--Import jQuery before materialize.js-->
<script type="text/javascript" src="/js/jquery-3.1.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.15/datatables.min.js"></script>
<script type="text/javascript" src="/js/materialize.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
<!-- Include the Quill library -->
<script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
<!--script>
 window.fbAsyncInit = function() {
    FB.init({
      appId            : '1901868319835391',
      autoLogAppEvents : true,
      xfbml            : true,
      version          : 'v3.0'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script-->
    <div id="fb-root"></div>
      <!-- Your customer chat code -->
      <div class="fb-customerchat"
      attribution="setup_tool"
      page_id="1528718070756402"
      theme_color="#0084ff"
      logged_in_greeting="Hola, en que te podemos ayudar?"
      logged_out_greeting="Hola, en que te podemos ayudar?">
      </div>
<!-- Load Facebook SDK for JavaScript -->
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/es_LA/sdk/xfbml.customerchat.js#xfbml=1&version=v2.12&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>

<style>
div.item {
    vertical-align: top;
    display: inline-block;
    text-align: center;
    width: 120px;
}
body{
    background-color:#d3d3d3;
}
/* LIST #8 */
#list8 {  }
#list8 ul { list-style:none; }
#list8 ul li { font-size:18px; text-align:center}
#list8 ul li a { display:block; height:28px; background-color:#0D2948; border-left:5px solid #0D2948; border-right:5px solid #0D2948; border-radius:10px; margin-top:10px;
  text-decoration:none; color:#bfe1f1; }
#list8 ul li a:hover {  -moz-transform:rotate(-5deg); -moz-box-shadow:10px 10px 20px #000000;
  -webkit-transform:rotate(-5deg); -webkit-box-shadow:10px 10px 20px #000000;
  transform:rotate(-5deg); box-shadow:10px 10px 20px #000000; }

.styled li {
    background: #0D2948;
    position: relative;
    list-style-type: none; /* drop the bullets */
    color: white;
    font-family: Verdana, Arial, Helvetica;
    font-size: 15pt;
    padding: 5pt;
    padding-left: 10pt;
    line-height: 20pt;
}

.styled li:hover {
    background: rgb(0, 169, 148) linear-gradient(to right, rgba(0, 169, 148, 1) 1%, rgba(0, 84, 166, 1) 100%);
}

.styled li + li {
    margin-top: 6pt; /* This is the spacing between the arrows */
}
iframe {
    right:0pt !important;
}

.styled li:after {
    content: "";
    border-left: .5em solid rgb(84, 84, 84); /* This width determines the arrows length */
    border-top: 15pt solid transparent; /* This width has to be 50% of 'line-height + padding-top + padding-bottom' */
    border-bottom: 15pt solid transparent; /* Same for this width */
    width: 0;
    height: 0;
    position: absolute;
    right: -.5em; /* This has to be the same value as 'border-left's width */
    top: 0;
}

.styled li:hover:after {
    border-left: .5em solid rgb(0, 84, 166);
}

 li.active{
    background-color:#0D2948 !important;
}

.indicator{
    background-color:#FFF !important;
}

@media only screen and (max-width: 601px) {
    .brand-logo img{
        height:56px !important;
    }style="display:inline"
}
@media only screen and (max-width: 992px) {
    .collection-item{
        display:inline !important;
    }
}
.collection-item:hover {
        background-color:#fff !important;
    }
</style>
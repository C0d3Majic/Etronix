@extends("partials.layouts.payments_layout")

@section("content")
<!--
  Content Section Start
-->
<main>
  <div class="row">
    <div class="col s12">            
      <div class="card horizontal" style="background-color:white; color:black">
        <div class="card-stacked">
          <div class="card-content">                
            <div class="input-field col s9 m8 l10" style="margin-top: 0px;">
              <input id="numero_transaccion" type="number" class="validate" required>
              <label for="numero_transaccion">Numero de transaccion</label>
            </div>
            <div class="input-field col s2 m4 l2" style="text-align:center">
              <a id="search" class="btn btn-floating btn-large pulse"><i class="material-icons">search</i></a>
            </div>
            <div style="text-align:center" class="input-field col s12 m12 l12">
              <a href="#modal1" class="waves-effect waves-light btn modal-trigger" style="background-color:red"><i class="material-icons left">error</i>No aparece? Click aqui</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col s12">
      <div class="card horizontal" style="background-color:white; color:black">
        <div class="card-stacked">
          <div class="card-content">
            <h4 class="header" style="text-align:center; color:#003c4c">Estado de mis pagos</h4>
            <table id="payments" class="centered highlight responsive-table" style="background-color:white">
              <thead>
                <tr>
                    <th># Transaccion</th>
                    <th>Razon</th>
                    <th>Fecha</th>
                    <th>Monto</th>
                    <th style="text-align:center">Status</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>Compra Tienda</td>
                  <td>3/25/2018</td>
                  <td>$45.00</td>
                  <td style="text-align:center"><span style="text-align:left"class="new badge blue" data-badge-caption="Verificado"></span></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>

<!-- Modal Structure -->
<div id="modal1" class="modal">
  <div class="modal-content">
    <h4>Crear nuevo ticket</h4>
    <p>*Describe el problema que se te presenta...</p>
    <div class="row">
      <div class="input-field col s12 m12 l12">
        <i class="material-icons prefix">textsms</i>
        <textarea id="description" name="description" class="materialize-textarea"></textarea>
        <label for="description">Descripción</label>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <div class="row">
      <div class="col s6">
        <a href="#!" id="btnEnviarNuevo" class="modal-action modal-close waves-effect waves-light btn">Enviar</a>
      </div>
      <div class="col s6">
        <a href="#!" style="background:red" class="modal-action modal-close waves-effect waves-light btn">Cancelar</a>
      </div>
    </div>
  </div>
</div>
<!--
  Content Section End
-->
@endsection     

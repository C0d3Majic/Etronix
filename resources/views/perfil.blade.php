@extends("partials.layouts.perfil_layout")

@section("content")
<!--
  Content Section Start
-->
<main>
  <div class="row">
    <div class="col s12">
      <!-- welcome Card -->
      <div id="welcome" class="card white">
        <div class="card-content black-text">
          <span class="card-title"><center><h4><strong>Bienvenido a tu seccion personal </strong></h4>{{$username}}</center></span>
        </div>
        <div class="row">
          <div class="col s12 center">
            <b>Edita tu informacion personal</b>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12 m4 l4"><i class="material-icons prefix">person</i>
            <input id="name" name="name" type="text" class="validate" value="{{Auth::user()->name}}">
            <label class="active" for="name">Name</label>
          </div>
          <div class="input-field col s12 m4 l4"><i class="material-icons prefix">email</i>
            <input id="email" name="email" type="email" class="validate" value="{{Auth::user()->email}}" autocomplete="off">
            <label class="active"for="email">Email</label>
          </div>
          <div class="input-field col s12 m4 l4"><i class="material-icons prefix">lock</i>
            <input id="password" name="password" type="password" class="validate" autocomplete="off">
            <label for="password">Password</label>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col s12" style="text-align:center">
          <button class="btn waves-effect waves-light" type="submit" id="saveBtn" name="Register"> Guardar cambios </button>
        </div>
      </div>
    </div>
  </div>
</main>
<!--
  Content Section End
-->
@endsection
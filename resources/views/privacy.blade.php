@extends("partials.layouts.landing_layout")

@section("content")
<div class="container">
    <h4>TÉRMINOS Y CONDICIONES</h4>
    <h6><b>Por favor lea cuidadosamente los siguientes términos de servicio antes de usar este sitio web u 
        ordenar productos a etronixs. El uso del sitio por su parte, confirma su aceptación incondicional de los 
        siguientes términos de servicio. Si no acepta los términos no use el sitio web.
        </b>
    </h6>
    <p style="text-align:left">
        <b>1.- Devolución de artículos adquiridos</b></br> 
        eTronixs <b>No</b> acepta devoluciones de artículos. Sin embargo se analizarán las solicitudes de devolución de artículos 
        sin daños pero no se asegura su aceptación. "Sin daños" significa que el dispositivo nunca ha sido energizado, conectado, soldado, 
        programado o alterado. eTronixs <b>No</b> aceptará devoluciones de artículos que hayan sido energizados, conectados, soldados, 
        programados o alterados. Favor de revisar nuestras Políticas de Devolución.
        </br> 
        <b>2.- Limitaciones de envío. </b></br>  
        Cuando una orden sea colocada, será enviada a la dirección designada por el cliente siempre y cuando la dirección de envío 
        cumpla con  los requisitos de nuestro sitio web y de las leyes mexicanas. Todas las compras realizadas en este sitio web estás 
        hechas de acuerdo a un contrato de envío. Como resultado, el riesgo de pérdida y la titularidad de los artículos comprados pasan 
        a ser responsabilidad del comprador, a menos que se establezca por escrito lo contrario. Usted es responsable de contactarnos 
        para iniciar una reclamación de envíos dañados o perdidos. Así mismo usted es responsable de pagar cualquier impuesto o gasto 
        aduanal de su orden.</br> 
        <b>3.- Productos, contenidos y especificaciones. </b></br>  
        Todas las características, contenido, especificaciones, productos, precios de productos y servicios descritos o mostrados 
        en este Sitio Web, www.etronixs.com.mx (este "Sitio Web"), están sujetos a cambios en cualquier momento sin necesidad de aviso. 
        Algunos pesos, medidas y descripciones pueden ser aproximados y son proporcionados por conveniencia. 
        Daniel Arturo Jarquin Moreno (" etronixs.com.mx "), que opera este Sitio Web, realiza esfuerzos razonables por desplegar imágenes 
        exactas de los atributos de los productos así como de los colores disponibles; sin embargo el color que usted visualice dependerá 
        de su sistema de cómputo, por lo que no podemos garantizar que su equipo despliegue los colores reales. La inclusión de cualquier 
        producto o servicio en este Sitio Web en determinado momento no implica o garantiza que estos productos o servicios estén disponibles 
        en un tiempo en particular. Es su responsabilidad respetar las leyes locales, estatales, federales o internacionales 
        (incluyendo requerimientos de edad mínima) para la posesión, uso y venta de cualquier artículo adquirido en este Sitio Web. 
        Al colocar una orden, usted establece que los productos serán usados dentro de los términos de la ley.    </br> 
        <b>4.- Exactitud de la información. </b></br>  
        Es nuestra intención que la información contenida en este Sitio Web esté completa, exacta y actualizada. A pesar de nuestros esfuerzos, 
        la información de este Sitio Web en ocasiones puede ser inexacta, incompleta o desactualizada. No aceptamos responsabilidad por el hecho 
        de que no estén completas, exactas o actualizadas en este Sitio Web. Por ejemplo, productos incluidos en este Sitio Web pueden estar no 
        disponibles, o tener características diferentes a las listadas, incluso tener un precio diferente al listado en este Sitio Web. Así mismo, 
        podemos realizar cambios de información, disponibilidad y precios sin previo aviso. Es nuestra práctica común confirmar órdenes por email; 
        la recepción de un email de confirmación de orden no constituye nuestra aceptación de su orden o nuestra confirmación de oferta de venta de 
        un producto o servicio. Nos reservamos el derecho, sin previa notificación, de limitar la cantidad de artículos o servicios por orden o 
        cliente así como de no dar servicio a determinados clientes. En ocasiones requeriremos verificar la información del cliente antes de aceptar 
        y/o enviar alguna orden.
        </br> 
        <b>5.- Uso de este sitio web.</b></br>
        El diseño de este Sitio Web, todos sus textos, gráficas, información, contenido y cualquier material desplegado en él que puedan ser 
        descargados desde este Sitio Web se encuentran protegidos por leyes de propiedad intelectual y registros de marca entre otras leyes. 
        Los contenidos de este sitio web son Marcas Registradas (c) 2018 de Daniel Arturo Jarquin Moreno, o de sus afiliados o proveedores 
        correspondientes. Todos los derechos reservados. Cualquier uso no autorizado de cualquier información o material puede violar las leyes 
        de registro de marcas, leyes de privacidad u otras leyes y regulaciones.     
        </br> 
        <b>6.- Marcas registradas.  </b></br>  
        Determinados nombres, marcas, servicios o logos usados o desplegados en este Sitio Web están registrados por eTronixs y sus afiliados. 
        Otras marcas o nombres son propiedad de sus respectivos dueños. Ningún contenido en este Sitio Web debe ser usado sin consentimiento 
        escrito de eTronixs o de su dueño respectivo.
        </br> 
        <b>7.- Links a sitios de terceros. </b></br>  
        En ocasiones, este Sitio Web puede contener links a sitios webs que no son propiedad, operados o manejados por eTronixs o sus afiliados. 
        Todos estos links son proporcionados de manera informativa solamente. Si usted emplea estos links, saldrá de nuestro Sitio Web. 
        Ni nosotros ni ninguno de nuestros respectivos afiliados serán responsables por los contenidos,  materiales u otra información ubicada o accesibles desde cualquier otro sitio Web. Ni nosotros ni ninguno de nuestros respectivos afiliados aprueba, garantiza o hace ninguna representación o garantía con respecto de cualquier otro sitio web o cualquier contenido, materiales u otra información ubicada o accesible desde cualquier otro sitio web, o de los resultados que pueda obtener del uso de cualquier otro sitio web. Si usted decide acceder a otros sitios web vinculados hacia o desde este sitio Web, usted lo hace bajo su propio riesgo.
        </br> 
        <b>8.- Material inapropiado. </b></br> 
        Se le prohíbe publicar o transmitir cualquier material ilegal, amenazante, difamatorio, calumnioso, obsceno, pornográfico o profano, 
        o cualquier material que pudiera constituir o alentar una conducta que pueda ser considerada una ofensa criminal o dar lugar a responsabilidad 
        ivil o violar cualquier ley. Así mismo si determinamos, a nuestra entera discreción, que usted ha violado o intenta violar las prohibiciones 
        anteriores, podemos tomar cualquier acción que consideremos necesaria para curar o prevenir la violación, incluyendo, sin limitación, 
        la eliminación inmediata de los materiales relacionados en este Sitio Web. 
        </br> 
        <b>9.- Información del usuario. </b></br> 
        Adicionalmente a la información de identificación personal, que está sujeta al Aviso de Privacidad de este Sitio Web; cualquier material, 
        información, sugerencia, idea, concepto, know -how , técnica , pregunta, comentarios u otra comunicación que usted transmita o publique 
        en este Sitio Web en cualquier forma ("Comunicaciones de Usuario") es y será considerado como no confidencial y no propietaria . 
        Nosotros y nuestros respectivos afiliados o sus representantes podrán utilizar cualquiera o todas las comunicaciones del usuario para 
        cualquier propósito, incluyendo, sin limitación, reproducción, transmisión, divulgación, publicación, difusión, desarrollo, fabricación 
        y / o comercialización de cualquier manera por cualquier o todos los fines comerciales o no comerciales. Podemos, aunque no estamos 
        obligados a, controlar o revisar cualquier comunicación del usuario. No tendremos ninguna obligación de utilizar, devolver, revisar 
        o responder a cualquier comunicación del usuario. No tendremos ninguna responsabilidad relacionada con el contenido de cualquier 
        comunicación del usuario, que surjan o no bajo las leyes de derecho de autor, difamación, privacidad, obscenidad u otras. 
        Nos reservamos el derecho a eliminar cualquier o todas las comunicaciones del usuario que incluye cualquier material que consideremos 
        inapropiado o inaceptable. 
        </br> 
        <b>10.- Clausulas. </b></br>  
        El uso de este Sitio Web es bajo su propio riesgo. La información, los materiales y los servicios prestados en o a través de este 
        sitio web se proporcionan "tal cual" sin ninguna garantía de ningún tipo incluyendo garantías de comerciabilidad, adecuación a un 
        propósito particular, o no infracción de la propiedad intelectual. Ni eTronixs ni ninguna de sus filiales garantizan la exactitud 
        o integridad de la información, los materiales o servicios proporcionados a través de este sitio web. La información, los materiales 
        y los servicios prestados en o a través de este sitio web pueden no estar actualizados, y tampoco eTronixs, ni ninguna de sus 
        respectivas afiliadas hace ningún compromiso ni asume ninguna obligación de actualizar dicha información, materiales o servicios. 
        Las anteriores exclusiones de las garantías implícitas no se aplican en la medida prohibida por la ley. Consulte las leyes locales 
        para las tales prohibiciones.
        Todos los productos y servicios adquiridos a través de este sitio web están sujetos sólo a las garantías aplicables de sus 
        respectivos fabricantes, distribuidores y proveedores, si los hubiere. En la máxima medida permitida por la ley vigente, por la 
        presente negamos todas las garantías de cualquier tipo, ya sea expresa o implícita, incluyendo cualquier garantía implícita, con 
        respecto a los productos y servicios enumerados o comprados a través de este sitio web. Sin limitar la generalidad de lo anterior, 
        por la presente negamos expresamente toda responsabilidad por producto defectuoso o avería, afirmaciones que son debido al desgaste 
        normal, mal uso del producto, abuso, modificación del producto, selección de productos indebida, incumplimiento de los códigos, 
        o apropiación indebida.</br>
        eTronixs no aprueba ni recomienda el uso de nuestros productos o recursos en productos críticos de soporte vital o de dispositivos 
        médicos, o para cualquier uso o aplicación en la que el fallo de un solo componente podría crear una situación en la que los daños 
        materiales, lesiones corporales o fallecimiento puedan ocurrir. Si usted viola el punto anterior,  absuelve a eTronixs y/o al fabricante 
        del producto de todos los daños, costos y gastos en que pueda incurrir, incluyendo, sin limitación, honorarios de abogados y costos 
        relacionados con cualquier demanda o pleito amenazado que surja de la el uso de nuestros productos en aplicaciones no autorizadas.</br> 
        <b>11.- Limitaciones de responsabilidad. </b></br>  
        eTronixs se deslinda de cualquier responsabilidad por cualquier daño, virus, malware que infecte su computadora, 
        equipo de telecomunicaciones o cualquier otra propiedad por accesar nuestro Sitio Web o por descargar cualquier información o 
        material desde el mismo. En ningún caso eTronixs o cualquiera de sus empleados, directores, afiliados, agentes, sucesores o asignados, 
        o cualquier persona involucrada en la creación, producción o transmisión de este Sitio Web, será responsable de daños directos o indirectos, 
        incidentales o accidentales y sus daños consecuentes (incluyendo sin limitación, aquellos que originen pérdidas de ganancias, 
        pérdida de datos o información o interrupción de operaciones mercantiles) como resultado del empleo de este Sitio Web o de otros 
        sitios ligados al mismo.</br>
        En el caso de cualquier problema con este Sitio Web o con su contenido, usted acepta dejar de usar nuestro Sitio Web. 
        En el caso de problemas con el funcionamiento de los productos adquiridos con nosotros, se resolverán de acuerdo a nuestra 
        Política de Garantías.</br>
        <b>12.- Revisiones a estos términos de servicio.  </b></br>
        Estos términos de servicio pueden ser revisados en cualquier momento y ser actualizados. Usted debe visitar esta página con 
        regularidad para revisar los términos de servicio actualizados. Algunos términos de servicio pueden ser sobreseídos por avisos 
        o términos contenidos en este Sitio Web.  </br>
        <b>13.- Jurisdicción. </b></br>
        Estos términos de servicio se sobreponen a cualquier acuerdo entre usted y eTronixs para resolver cualquier inconsistencia o ambigüedad. 
        Estos términos de servicio se encuentran regidos por las leyes mexicanas, sin dar ningún efecto o conflicto con dichas leyes Cualquier 
        acción legal o de conciliación relacionada con este Sitio Web será bajo la jurisdicción de Chihuahua, Chih., México. Una versión impresa 
        de estos términos será admisible como argumento judicial o administrativo.</br>
        <b>14.- Terminación. </b></br>
        Usted o nosotros podremos suspender o dar de baja su cuenta o su uso en nuestro Sitio Web en cualquier momento, por cualquier razón o 
        sin razón., Usted es responsable por cualquier orden colocada y los cargos que incurra antes de su terminación. 
        Nos reservamos el derecho de cambiar, suspender o modificar cualquier aspecto de este Sitio Web in previo aviso.</br>
        <b>15.- Orientación adicional. </b></br>
        Si usted tiene dudas sobre los términos de este contrato o si tiene alguna pregunta o comentario, puede contactar a nuestro 
        departamento de Servicio al cliente por email clientes@etronixs.com.mx o por teléfono al +52 (656)6087352.
    </p>
</div>
<!--
  Content Section End
-->
@endsection
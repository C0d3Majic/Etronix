@extends("partials.layouts.reports_layout")

@section("content")
<!--
  Content Section Start
-->
<main>
  <div class="row">
    <div class="col s12">
      <div class="card blue-white darken-1">
        <div class="card-content black-text">
          <span style="text-align:center" class="card-title"><b>Tabla de Pagos</b></span>          
          <div class="row">
            <div class="input-field col s12 m6 l2">
              <input id="date" type="text" class="datepicker">
              <label>Filtrado por Fecha</label>
            </div>
            <div class="input-field col s12 m6 l2">
              <select id="user">
                <option value="">Selecciona un usuario</option>        
                @foreach($users as $user)
                  <option value="{{$user->id}}">{{$user->name}}</option>
                @endforeach              
              </select>
              <label>Usuario</label>
            </div>                    
            <div style="margin-top:25px;"class="col s12 col m3 l3">
              <a  style="background-color:#0D2948" class="waves-effect waves-light btn">Aplicar Filtros</a>
            </div>
          <div style="margin-top:25px" class="col s12 col m3 l3">
            <a  style="background-color:#0D2948" class="waves-effect waves-light btn">Imprimir Reporte</a>
          </div>
        </div>
        <div class="row">
        <canvas id="myChart"></canvas>
          <div class="col s12">
            <table id="reports" class="highlight centered responsive-table" style="background-color:white">
              <thead>
                <tr>
                    <th>Usuario </th>
                    <th># Transaccion</th>
                    <th>Razon</th>
                    <th>Fecha</th>
                    <th>Monto</th>
                    <th style="text-align:center">Status</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Daniel Jarquin</td>
                  <td>1</td>
                  <td>Arduino UNO</td>
                  <td>3/25/2018</td>
                  <td>$45.00</td>
                  <td style="text-align:center"><span class="new badge blue" data-badge-caption="Verificado"></span> </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
<!--
  Content Section End
-->
@endsection
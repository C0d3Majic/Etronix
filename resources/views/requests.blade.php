@extends("partials.layouts.requests_layout")

@section("content")
<!--
  Content Section Start
-->
<main>
  <div class="container"><br>
    <div class="row">
     <div class="col s12 center">
        <h4 style="color:#0D2948">Todos los Servicios</h4>
        <hr>
        <div class="row">
          <div class="col s12">
            <h5 class="header">Impresión 3D</h5>
            <div class="card horizontal">
              <div class="card-image hide-on-small-only">
                <img src="/img/printing.jpg">
              </div>
              <div class="card-stacked">
                <div class="card-content">
                  <p>Nos especializamos en desarrollar partes a medida de nuestros clientes.</p>
                </div>
                <div class="card-action">
                    <a class="request3d waves-effect waves-light btn" data-service_id="1"><i class="material-icons right">work</i>Solicitar Servicio?</a>
                </div>
              </div>
            </div>
          </div>   
        </div>
        <div class="row">
          <div class="col s12">
            <h5 class="header">Creacion de tablillas digitales</h5>
            <div class="card horizontal">
              <div class="card-image hide-on-small-only">
                <img src="/img/electronics.jpg">
              </div>
              <div class="card-stacked">
                <div class="card-content">
                  <p>Nos especializamos en desarrollar circuitos embebidos para nuestros clientes.</p>
                </div>
                <div class="card-action">
                  <a class="requestCircuit waves-effect waves-light btn modal-trigger" data-service_id="2"><i class="material-icons right">work</i>Solicitar Servicio?</a>
                </div>
              </div>
            </div>
          </div>   
        </div>
      </div>
    </div>
  </div>
</main>

<!-- Modal Structure -->
<div id="modal1" class="modal">
  @if(Auth::check())
  <div class="modal-content">   
    <h4 id="title">Solicitar un servicio de </h4> 
    <div class="row">
      <form id="form_upload" action="{{url('request/new/service')}}" method="POST" enctype="multipart/form-data">        
        <div class="row">
          <div class="input-field col s12 l6">
            <i class="material-icons prefix">textsms</i>
            <textarea id="description" name="description" class="materialize-textarea"></textarea>
            <label for="description">Describe del servicio que estas buscando</label>
          </div>
          <div class="input-field col l6 s12">
            <i class="material-icons prefix">people</i>
            <input id="quantity" name="quantity" type="number" class="validate" min="0">
            <label for="quantity">Cantidad a generar</label>
          </div>       
        </div> 
        <div class="row">
            <div class="input-field file-field col s12" style="margin-top:10%">
                <div class="btn">
                    <span>Archivo</span>
                    <input name="picture" id="picture" accept="file/*" onchange="loadFile(event)" type="file">
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path validate" type="text">
                </div>
            </div> 
        </div>                                   
      </form>
    </div>
  </div>
  <div class="modal-footer">
    <div class="row">
      <div class="col s6">
        <a href="#!" id="btnEnviarNuevo" class="modal-action modal-close waves-effect waves-light btn">Enviar Solicitud</a>
      </div>
      <div class="col s6">
        <a href="#!" style="background:red" class="modal-action modal-close waves-effect waves-light btn">Cancelar</a>
      </div>
    </div>
  </div>
  @else
  <div class="modal-content">   
    <h4 class="center">Inicia Sesión para solicitar un servicio</h4>    
  </div>
  <div class="modal-footer">
    <div class="row center">
        <a class="modal-action modal-close waves-effect waves-light btn">Ok</a>
    </div>
  </div>
  @endif
</div>
<!--
  Content Section End
-->
@endsection     

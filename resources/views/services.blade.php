@extends("partials.layouts.services_layout")

@section("content")
<!--
  Content Section Start
-->
<main>
  <div class="row">
    <div class="col s12">
      <div class="card blue-white darken-1">
        <div class="card-content black-text">
          <span style="text-align:center" class="card-title"><b>Tabla Servicios</b></span>
          <div class="row">
            <div class="col s12">
                <table id="users" class="highlight responsive-table">
                  <thead>
                    <tr>
                        <th>Usuario que lo solicita</th>
                        <th>Correo Electrónico</th>
                        <th>Descripcion del Servicio</th>
                        <th>Cantidad solicitada</th>
                        <th style="text-align:right">Acciones</th>
                    </tr>
                  </thead>
                  <tbody>                 
                    @foreach($services as $service)
                      <tr>
                        <td>{{$service->owner->name}}</td>
                        <td>{{$service->owner->email}}</td>
                        <td>{{$service->description}}</td>
                        <td>{{$service->quantity}}</td>
                        <td style="text-align:right; cursor:pointer">
                            <a class="downloadService small material-icons tooltipped" href="{{$service->attached_file}}" download data-user_id="{{$service->id}}" data-position="bottom" data-delay="50" data-tooltip="Descargar adjunto"><i class="small material-icons">file_download</i></a>
                            <a class="answerService small material-icons tooltipped" data-service_id="{{$service->id}}" data-email="{{$service->owner->email}}" data-name="{{$service->owner->name}}" data-description="{{$service->description}}" data-position="bottom" data-delay="50" data-tooltip="Contestar Cotización"><i class="small material-icons">mail</i></a>
                            <a class="cancelService small material-icons tooltipped" data-service_id="{{$service->id}}" data-description="{{$service->description}}" data-position="bottom" data-delay="50" data-tooltip="Cancelar Servicio"><i class="small material-icons">cancel</i></a>
                        </td>  
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>

<!-- Modal Structure -->
<div id="modal1" class="modal">
  <div class="modal-content">
  </div>
  <div class="modal-footer">
    <div class="row">
      <div class="col s6">
        <a id="btnEnviar" class="modal-action modal-close waves-effect waves-light btn">Enviar Mail</a>
      </div>
      <div class="col s6">
        <a id="btnCancelarEnviar" style="background:red" class="modal-action modal-close waves-effect waves-light btn">Cancelar</a>
      </div>
    </div>
  </div>
</div>

<!-- Modal Structure -->
<div id="modal3" class="modal">
  <div class="modal-content">
    <h5 id="title" class="center"></h5>
        <div class="row">                                            
            <div class="input-field col s12">
                <textarea id="cancel_content" class="materialize-textarea"></textarea>
                <label for="cancel_content">Razón de la Cancelación</label>
            </div>
        </div>
  </div>
  <div class="modal-footer">
    <div class="row">
      <div class="col s6">
        <a id="btnEnviarCancelacion" class="modal-action modal-close waves-effect waves-light btn">Enviar Mail</a>
      </div>
      <div class="col s6">
        <a id="btnCancelarCancelacion" style="background:red" class="modal-action modal-close waves-effect waves-light btn">Cancelar</a>
      </div>
    </div>
  </div>
</div>


<!--
  Content Section End
-->
@endsection
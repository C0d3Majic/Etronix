@extends("partials.layouts.store_layout")

@section("content")
<!--
  Content Section Start
-->

<div class="container"><br>
    <div class="row">
     <div class="col s12 m4 l3 center">
            <h5 style="color:#0D2948">Categorias</h5>
            <hr>
            <ul class="styled">
                @foreach($categories as $category)
                <li><a href="#!" style="color:white">{{$category->name}}</a></li>
                @endforeach
            </ul><br><br>
            <h5 style="color:#0D2948" class="center">Lo más buscado</h5>
            <ul class="center">
                <li class="collection-item avatar center">
                    <div class="item">
                        <img style="width:50%" src="https://www.prometec.net/wp-content/uploads/2016/01/Leonardo-1-300x300.png" alt="" class="circle">
                        <span style="color:#0D2948; display:block">Arduino </span>
                    <div>
                </li>
                <li class="collection-item avatar center">
                    <div class="item">
                        <img style="width:50%" src="https://www.prometec.net/wp-content/uploads/2015/08/IMG_1220-300x300.jpg" alt="" class="circle">
                        <span style="color:#0D2948; display:block">Kit de inicio prometec</span>
                    <div>
                </li>
                <li class="collection-item avatar center">
                    <div class="item">
                        <img style="width:50%" src="https://www.prometec.net/wp-content/uploads/2018/02/HTB1.cQvQXXXXXcoXVXXq6xXFXXXB-520x535.jpg" alt="" class="circle">
                        <span style="color:#0D2948; display:block">Módulo de 16 relés</span>
                    <div>
                </li>
                <li class="collection-item avatar center">
                    <div class="item">
                        <img style="width:50%" src="https://www.prometec.net/wp-content/uploads/2017/02/HTB1AVrnHXXXXXajXFXXq6xXFXXXs-300x300.jpg" alt="" class="circle">
                        <span style="color:#0D2948; display:block">Attiny85</span>
                    <div>
                </li>
                <li class="collection-item avatar center">
                    <div class="item">
                        <img style="width:50%" src="https://www.prometec.net/wp-content/uploads/2015/04/2222n.jpg" alt="" class="circle">
                        <span style="color:#0D2948; display:block">Transistor NPN 2222N</span>
                    <div>
                </li>
                <li class="collection-item avatar center">
                    <div class="item">
                        <img style="width:50%" src="https://www.prometec.net/wp-content/uploads/2018/01/HTB1k4_KRVXXXXb6apXXq6xXFXXX3-520x535.jpg" alt="" class="circle">
                        <span style="color:#0D2948; display:block">USB Logic Analyze 24M 8CH</span>
                    <div>
                </li>
            </ul>
        </div>
        <div class="col s12 m8 l9 center">
            <h5 style="color:#0D2948">Todos los productos</h5>
            <hr>
            <div class="row">
                @foreach($items as $item)
                <div class="col s12 m4 l4 xl3">
                    <form class="form-item">
                        <div class="card">
                            <div class="card-image center">
                                <img class="materialboxed responsive-img" data-caption="{{$item->description}}" src="{{$item->picture}}">
                                <!--img src="{{$item->picture}}" class="activator responsive-img"-->
                            </div>
                            <div class="card-content activator">
                                <p class="activator" style="font-size:14px!important">{{$item->name}} <br>${{$item->price}} + IVA</p>
                            </div>
                            <div class="card-action">
                                <input name="item_id" type="hidden" value="{{$item->id}}">                   
                                <input name="amount" style="text-align:center; font-size:14px" type="number" min="1" size="2" max="{{$item->stock}}"name="quantity" value="1"> 
                                <button style="background-color:#0D2948" data-position="bottom" data-delay="50" data-tooltip="Agregar al carrito" class="tooltipped btn-floating btn-small waves-effect waves-light" type="submit" name="action">
                                    <i class="material-icons right">add</i>
                                </button>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4">Descripción<i class="material-icons right">close</i></span>
                                <p>{{$item->description}}</p>
                            </div>
                        </div>
                    </form>
                </div>
                @endforeach                
            </div>
             
            @if($active === "")
                <ul class="pagination">
                @if($page == 1)
                    <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                    <li class="active"><a href="{{url('store/all-products/1')}}">1</a></li>
                    <li class="waves-effect"><a href="{{url('store/all-products/2')}}">2</a></li>
                    <li class="waves-effect"><a href="{{url('store/all-products/3')}}">3</a></li>
                    <li class="waves-effect"><a href="{{url('store/all-products/4')}}">4</a></li>
                    
                    <li class="waves-effect"><a href="{{url('store/all-products')}}/{{$page}}"><i class="material-icons">chevron_right</i></a></li>
                @elseif($page == 2)
                    <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                    <li class="waves-effect"><a href="{{url('store/all-products/1')}}">1</a></li>
                    <li class="active"><a href="{{url('store/all-products/2')}}">2</a></li>
                    <li class="waves-effect"><a href="{{url('store/all-products/3')}}">3</a></li>
                    <li class="waves-effect"><a href="{{url('store/all-products/4')}}">4</a></li>
                    
                    <li class="waves-effect"><a href="{{url('store/all-products')}}/{{$page}}"><i class="material-icons">chevron_right</i></a></li>
                @elseif($page == 3)
                    <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                    <li class="waves-effect"><a href="{{url('store/all-products/1')}}">1</a></li>
                    <li class="waves-effect"><a href="{{url('store/all-products/2')}}">2</a></li>
                    <li class="active"><a href="{{url('store/all-products/3')}}">3</a></li>
                    <li class="waves-effect"><a href="{{url('store/all-products/4')}}">4</a></li>
                    
                    <li class="waves-effect"><a href="{{url('store/all-products')}}/{{$page}}"><i class="material-icons">chevron_right</i></a></li>
                @elseif($page == 4)
                    <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                    <li class="waves-effect"><a href="{{url('store/all-products/1')}}">1</a></li>
                    <li class="waves-effect"><a href="{{url('store/all-products/2')}}">2</a></li>
                    <li class="waves-effect"><a href="{{url('store/all-products/3')}}">3</a></li>
                    <li class="active"><a href="{{url('store/all-products/4')}}">4</a></li>
                    
                    <li class="waves-effect"><a href="{{url('store/all-products')}}/{{$page}}"><i class="material-icons">chevron_right</i></a></li>
                @endif
                </ul>      
            @endif           
        </div>
    </div>
</div>
  <!--
  Content Section End
-->
@endsection
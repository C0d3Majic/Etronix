@extends("partials.layouts.users_layout")

@section("content")
<!--
  Content Section Start
-->
<main>
  <div class="row">
    <div class="col s12">
      <div class="card blue-white darken-1">
        <div class="card-content black-text">
          <span style="text-align:center" class="card-title"><b>Tabla Usuarios</b></span>
          <div class="row">
            <div class="col s12">
                <table id="users" class="highlight responsive-table">
                  <thead>
                    <tr>
                        <th>Usuario</th>
                        <th>Correo Electrónico</th>
                        <th>Tipo de Usuario</th>
                        <th style="text-align:right">Acciones</th>
                    </tr>
                  </thead>
                  <tbody>                 
                    @foreach($users as $user)
                      <tr>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->usertype->name}}</td>
                        <td style="text-align:right; cursor:pointer">
                            <a class="editUser small material-icons tooltipped" data-user_id="{{$user->id}}" data-position="bottom" data-delay="50" data-tooltip="Editar"><i class="small material-icons">mode_edit</i></a>
                            <a class="deleteUser small material-icons tooltipped" data-user_id="{{$user->id}}" data-position="bottom" data-delay="50" data-tooltip="Eliminar"><i class="small material-icons">delete</i></a>
                        </td>  
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col s4 push-s5">
      <a id="btnAgregar" style="background-color:#0D2948" data-target="modal3" class="waves-effect waves-light btn modal-trigger">Agregar Usuario</a>
    </div>
  </div>
</main>

<!-- Modal Structure -->
<div id="modal1" class="modal">
  <div class="modal-content">
  </div>
  <div class="modal-footer">
    <div class="row">
      <div class="col s12">
        <h6 style="color:red">**Nota: Dejar vacio password si no se desea editar **</h6>
      </div>
    </div>
    <div class="row">
      <div class="col s6">
        <a href="#!" id="btnGuardar" class="modal-action modal-close waves-effect waves-light btn">Guardar</a>
      </div>
      <div class="col s6">
        <a href="#!" id="btnCancelarEditar" style="background:red" class="modal-action modal-close waves-effect waves-light btn">Cancelar</a>
      </div>
    </div>
  </div>
</div>

<!-- Modal Structure -->
<div id="modal2" class="modal">
  <div class="modal-content">
    <h4>Eliminar usuario</h4>
    <p>Estas seguro de querer eliminar a este usuario?</p>
  </div>
  <div class="modal-footer">
    <div class="row">
      <div class="col s6">
        <a href="#!" id="btnEliminar" class="modal-action modal-close waves-effect waves-light btn">Aceptar</a>
      </div>
      <div class="col s6">
        <a href="#!" id="btnCancelarEliminar" style="background:red" class="modal-action modal-close waves-effect waves-light btn">Cancelar</a>
      </div>
    </div>
  </div>
</div>

<!-- Modal Structure --> 
<div id="modal3" class="modal">
  <div class="modal-content">
    <h4 class="center">Llena los datos para el nuevo usuario</h4>
    <div class="row">
      <div class="input-field col m6 l6 s12">
        <i class="material-icons prefix">people</i>
        <input id="new_name" name="new_name" type="text" class="validate">
        <label for="new_name">Nombre del usuario</label>
      </div>  
      <div class="input-field col m6 l6 s12">
        <i class="material-icons prefix">mail</i>
        <input id="new_email" name="new_email" type="email" class="validate">
        <label for="new_email">Correo electrónico</label>
      </div>   
    </div>  
    <div class="row">
        <div class="input-field col m6 l6 s12">
            <i class="material-icons prefix">people</i>
            <input id="new_password" name="new_password" type="password" class="validate">
            <label for="new_password">Contraseña</label>
        </div>  
        <div class="input-field col m6 l6 s12">
            <i class="material-icons prefix">accessibility</i>
            <select name="new_type" id="new_type">
            <option value="" disabled selected>Tipo de usuario?</option>
            @foreach($user_types as $user_type)
                <option value="{{$user_type->id}}">{{$user_type->name}}</option>
            @endforeach
            </select>
            <label for="new_type">Ubicación</label>
      </div>   
    </div>  
  </div>
  <div class="modal-footer">
    <div class="row">
      <div class="col s6 center">
        <a id="btnGuardarNuevo" class="modal-action modal-close waves-effect waves-light btn">Guardar</a>
      </div>
      <div class="col s6 center">
        <a id="btnCancelar" style="background:red" class="modal-action modal-close waves-effect waves-light btn">Cancelar</a>
      </div>
    </div>
  </div>
</div>
<!--
  Content Section End
-->
@endsection
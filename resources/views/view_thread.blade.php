@extends("partials.layouts.thread_layout")

@section("content")
<!--
  Content Section Start
-->

<div class="container"><br><br>
    <div class="row">
        <div class="col s12 l10 center">
            <div class="row">
                <div class="col s12 l10">
                    <h5 style="color:#0D2948">{{$thread->title}} {{$thread->date}}</h5>
                    <img class="responsive-img" src="{{$thread->splash_image}}">
                    {!!$thread->content!!}
                    <div style="float:right;font-weight:bold">Autor: {{$thread->owner->name}}</div>
                </div>
            </div>
        </div>
        <div class="col s12 l2">
            <div class="row">
                <div class="col s12 l12">
                    <h5 style="color:#0D2948;" class="center">Items used</h5>
                    <ul class="center">
                        <li class="collection-item avatar center">
                            <div class="item">
                                <img style="width:50%" src="https://www.prometec.net/wp-content/uploads/2016/01/Leonardo-1-300x300.png" alt="" class="circle">
                                <span style="color:#0D2948; display:block">Arduino</span>
                            <div>
                        </li>
                        <li class="collection-item avatar center">
                            <div class="item">
                                <img style="width:50%" src="https://www.prometec.net/wp-content/uploads/2015/08/IMG_1220-300x300.jpg" alt="" class="circle">
                                <span style="color:#0D2948; display:block">Kit de inicio prometec</span>
                            <div>
                        </li>
                        <li class="collection-item avatar center">
                            <div class="item">
                                <img style="width:50%" src="https://www.prometec.net/wp-content/uploads/2018/02/HTB1.cQvQXXXXXcoXVXXq6xXFXXXB-520x535.jpg" alt="" class="circle">
                                <span style="color:#0D2948; display:block">Módulo de 16 relés</span>
                            <div>
                        </li>
                        <li class="collection-item avatar center">
                            <div class="item">
                                <img style="width:50%" src="https://www.prometec.net/wp-content/uploads/2017/02/HTB1AVrnHXXXXXajXFXXq6xXFXXXs-300x300.jpg" alt="" class="circle">
                                <span style="color:#0D2948; display:block">Attiny85</span>
                            <div>
                        </li>
                        <li class="collection-item avatar center">
                            <div class="item">
                                <img style="width:50%" src="https://www.prometec.net/wp-content/uploads/2015/04/2222n.jpg" alt="" class="circle">
                                <span style="color:#0D2948; display:block">Transistor NPN 2222N</span>
                            <div>
                        </li>
                        <li class="collection-item avatar center">
                            <div class="item">
                                <img style="width:50%" src="https://www.prometec.net/wp-content/uploads/2018/01/HTB1k4_KRVXXXXb6apXXq6xXFXXX3-520x535.jpg" alt="" class="circle">
                                <span style="color:#0D2948; display:block">USB Logic Analyze 24M 8CH</span>
                            <div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- SECCION DE COMENTARIOS -->
    <div class="row">
        <div class="col s12 l10 offset-l1">
            <h5>Comments Section </h5>
            <!-- Create the editor container -->
            <div id="editor">
                <p placeholder="Add a comment"></p>
            </div> 
            <button id="submit" style="float:right;background-color:#0D2948" class="btn waves-effect waves-light" type="submit" name="action">Submit
                <i class="material-icons right">send</i>
            </button>
        </div>
    </div>
    @foreach($comments as $comment)
    <div class="row">
        <div class="col s3 m2 l1 offset-l1">
            <img class="responsive-img circle" src="{{$comment->owner->avatar}}"/>  
        </div>
        <div class="col s9 m10 l10">
            <span style="text-align:left;font-size:18px;font-weight:bold;">{{$comment->owner->name}}</span><span style="padding-left:5px;font-size:14px;padding-top:4px;font-size: 0.8rem;">   23 Mar, 2018 </span>
            <p>{{$comment->content}}</p>
            <span style="color:grey;cursor:pointer;">Reply</span> <span style="color:grey;cursor: pointer;">Share</span>
        </div>
    </div>
    @endforeach
    <div class="row">
        <div class="col s3 m2 l1 offset-l1">
            <img class="responsive-img circle" src="/img/female.png"/>  
        </div>
        <div class="col s9 m10 l10">
            <span style="text-align:left;font-size:18px;font-weight:bold;">Maria Ruiz</span><span style="padding-left:5px;font-size:14px;padding-top:4px;font-size: 0.8rem;">   25 Mar, 2018 </span>
            <p>Jarquin explica esto de forma tan sencilla que cualquiera puede hacerlo</p>
            <span style="color:grey;cursor:pointer;">Reply</span> <span style="color:grey;cursor: pointer;">Share</span>
        </div>
    </div>
    <div class="row">
        <div class="col s3 m2 l1 offset-l1">
            <img class="responsive-img circle" src="/img/teenager.png"/>  
        </div>
        <div class="col s9 m10 l10">
            <span style="text-align:left;font-size:18px;font-weight:bold;">Ramiro Cortez</span><span style="padding-left:5px;font-size:14px;padding-top:4px;font-size: 0.8rem;">   28 Mar, 2018 </span>
            <p>Si hubiera encontrado este blog antes ya dominaria la electronica, gracias totales</p>
            <span style="color:grey;cursor:pointer;">Reply</span> <span style="color:grey;cursor: pointer;">Share</span>
        </div>
    </div>
</div>
<!--
  Content Section End
-->
@endsection

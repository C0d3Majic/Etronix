<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Etronixs</title>
  <!--Import Google Icon Font-->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!-- CORE CSS-->
  <link href="/css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="/css/style.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="/css/page-center.css" type="text/css" rel="stylesheet" media="screen,projection">
</head>

<body style="background-color:#0D2948">
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
      <div id="loader"></div>        
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
  </div>
  <!-- End Page Loading -->

  <div id="login-page" class="row">
    <div class="col s12 z-depth-4 card-panel" style="background-color:black">
      <form id="login_form" method="post" action="http://localhost:8080/namaste_backend/user/login" class="login-form">
        <div class="row">
          <div class="input-field col s12 center">
            <img src="/img/logo.jpg" alt="" class="responsive-img">
            <p class="center" style="color:white">Bienvenido a Etronix</p>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
            <i class="material-icons prefix" style="color:white">person_outline</i>
            <input id="icon_prefix" type="text" class="validate" name="account" style="color:white" autocomplete="off">
            <label for="icon_prefix">Usuario</label>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
            <i class="material-icons prefix" style="color:white">lock_outline</i>
            <input id="password" type="password" name="password" autocomplete="off">
            <label for="password">Password</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <button class="btn waves-effect waves-light col s12" style="background-color:white;color:black">Iniciar Sesión</button>
          </div>
        </div>

      </form>
    </div>
  </div>



  <!-- ================================================
    Scripts
    ================================================ -->
  <script type="text/javascript" src="/js/jquery-3.1.1.js"></script>
  <script type="text/javascript" src="/js/materialize.min.js"></script>
  <script>
    $( document ).ready(function() {    
      
        setTimeout(function(){
          $('body').addClass('loaded');          
        },1000);

        $("#login_form").submit(function(){
        // Start $.ajax() method
        $.ajax({
          // The URL for the request. variable set above
          url: $(this).attr('action'),
          // The data to send (will be converted to a query string). variable set above
          data: $(this).serialize(),
          // Whether this is a POST or GET request
          type: "POST",
          // The type of data we expect back. can be json, html, text, etc...
          dataType : "json",
          // Code to run if the request succeeds;
          // the response is passed to the function
          success: function( msg ) {
            if(msg.status == 'error'){
              var $toastContent = $('<span>'+ msg.type +'</span>');
              Materialize.toast($toastContent, 2000, 'rounded red darken-3');
            }else if(msg.status == 'success'){
                          
            }
          },
          error: function(){
            var $toastContent = $('<span>There was an error on the server</span>');
            Materialize.toast($toastContent, 2000, 'rounded red darken-3');
          }
        });
        return false;
      });
    });
  </script>
</body>
</html>
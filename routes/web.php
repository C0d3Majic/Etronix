<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SiteController@show_view_landing');;
Route::get('thread/{id}', 'ThreadsController@thread');

Route::get('darkness',         'SiteController@show_view_login');
Route::get('users',         'SiteController@show_view_users');
Route::get('error',         'SiteController@show_view_error');
Route::get('dashboard',     'SiteController@show_view_dashboard');
Route::get('reports',       'SiteController@show_view_reports');
Route::get('profile',       'SiteController@show_view_perfil');
Route::get('payments',      'SiteController@show_view_payments');
Route::get('items',         'SiteController@show_view_items');
Route::get('store',         'SiteController@show_view_store');
Route::get('cart',          'SiteController@show_view_cart');
Route::get('brands',        'SiteController@show_view_brands');
Route::get('providers',     'SiteController@show_view_providers');
Route::get('categories',    'SiteController@show_view_categories');
Route::get('services',      'SiteController@show_view_services');
Route::get('requests',      'SiteController@show_view_requests');
Route::get('welcome',       'SiteController@show_view_welcome');
Route::get('privacy',       'SiteController@show_view_privacy');
Route::get('conditions',    'SiteController@show_view_conditions');

Route::prefix('user')->group(function () {
    Route::post('login',    'UsersController@login');
    Route::post('logout',   'UsersController@logout');
});

Route::post('new/user',             'UsersController@new');
Route::get('delete/user/{id}',      'UsersController@delete');
Route::post('delete/user',          'UsersController@delete_user');
Route::get('edit/user/{id}',        'UsersController@edit');
Route::post('save/user',            'UsersController@save');
Route::post('save/user/profile',    'UsersController@save_profile');

Route::post('payment/search',       'PaymentsController@search');
Route::post('new/ticket',           'TicketsController@new');

Route::post('new/item',             'ItemsController@new');
Route::post('delete/item',          'ItemsController@delete_item');
Route::get('delete/item/{id}',      'ItemsController@delete');
Route::get('edit/item/{id}',        'ItemsController@edit');
Route::post('save/item',            'ItemsController@save');
Route::get('edit/item/image/{id}',  'ItemsController@edit_image');
Route::post('save/item/image',      'ItemsController@save_image');

Route::post('new/cart/item',        'CartsController@add_item');
Route::post('add/cart/item',        'CartsController@add_to_item');
Route::post('min/cart/item',        'CartsController@substract_to_item');
Route::post('delete/cart',          'CartsController@delete');
Route::post('delete/cart/item',     'CartsController@remove_item');

Route::post('new/brand',            'BrandsController@new');
Route::get('edit/brand/{id}',       'BrandsController@edit');
Route::post('save/brand',           'BrandsController@save');
Route::post('delete/brand',         'BrandsController@delete_brand');
Route::get('delete/brand/{id}',     'BrandsController@delete');

Route::post('new/provider',         'ProvidersController@new');
Route::get('edit/provider/{id}',    'ProvidersController@edit');
Route::post('save/provider',        'ProvidersController@save');
Route::post('delete/provider',      'ProvidersController@delete_provider');
Route::get('delete/provider/{id}',  'ProvidersController@delete');

Route::post('new/category',         'CategoriesController@new');
Route::get('edit/category/{id}',    'CategoriesController@edit');
Route::post('save/category',        'CategoriesController@save');
Route::post('delete/category',      'CategoriesController@delete_category');
Route::get('delete/category/{id}',  'CategoriesController@delete');

Route::post('send/service/answer',  'ServicesController@send_answer');
Route::post('send/service/cancel',  'ServicesController@cancel');
Route::post('request/new/service',  'ServicesController@new');





Route::get('contact',function(){
    return view('contact')->with('username','Daniel Jarquin')->with('today_sales',3)->with('pending_sales',2)->with('session_id',1)->with('session_token',3);
});

Route::get('about',function(){
    return view('about')->with('username','Daniel Jarquin')->with('today_sales',3)->with('pending_sales',2)->with('session_id',1)->with('session_token',3);
});
